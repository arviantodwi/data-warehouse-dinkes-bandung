/*!
 *      Untuk demo saja
 */

(function ($) {

  "use strict";

  /* Morris.js Charts */
  // 1 Bulan chart
  var area = new Morris.Bar({
    element: '1m-chart',
    resize: true,
    data: [
      {y: '5-11', item1: 52, item2: 78},
      {y: '12-19', item1: 57, item2: 53},
      {y: '20-34', item1: 12, item2: 6},
      {y: '35-44', item1: 21, item2: 18},
      {y: '45-54', item1: 35, item2: 34},
      {y: '55-64', item1: 53, item2: 52},
      {y: '65-74', item1: 63, item2: 68},
      {y: '75-84', item1: 64, item2: 77},
      {y: '85+', item1: 56, item2: 51},
    ],
    xkey: 'y',
    ykeys: ['item1', 'item2'],
    labels: ['Wanita', 'Pria'],
    barColors: ['#e9422e', '#009cd6'], //item 1, Wanita, #e9422e  | item 2, Pria, #009cd6 
    hideHover: 'auto',
  });
  
  // 6 Bulan chart
  var area = new Morris.Bar({
    element: '6m-chart',
    resize: true,
    data: [
      {y: '5-11', item1: 42, item2: 68},
      {y: '12-19', item1: 67, item2: 63},
      {y: '20-34', item1: 22, item2: 16},
      {y: '35-44', item1: 41, item2: 38},
      {y: '45-54', item1: 15, item2: 14},
      {y: '55-64', item1: 43, item2: 42},
      {y: '65-74', item1: 83, item2: 88},
      {y: '75-84', item1: 74, item2: 67},
      {y: '85+', item1: 66, item2: 61},
    ],
    xkey: 'y',
    ykeys: ['item1', 'item2'],
    labels: ['Wanita', 'Pria'],
    barColors: ['#e9422e', '#009cd6'], //item 1, Wanita, #e9422e  | item 2, Pria, #009cd6 
    hideHover: 'auto'
  });
  
  // 1 Tahun chart
  var area = new Morris.Bar({
    element: '1y-chart',
    resize: true,
    data: [
      {y: '5-11', item1: 32, item2: 48},
      {y: '12-19', item1: 27, item2: 23},
      {y: '20-34', item1: 32, item2: 36},
      {y: '35-44', item1: 51, item2: 48},
      {y: '45-54', item1:65, item2: 64},
      {y: '55-64', item1: 43, item2: 42},
      {y: '65-74', item1: 83, item2: 88},
      {y: '75-84', item1: 44, item2: 57},
      {y: '85+', item1: 36, item2: 31},
    ],
    xkey: 'y',
    ykeys: ['item1', 'item2'],
    labels: ['Wanita', 'Pria'],
    barColors: ['#e9422e', '#009cd6'], //item 1, Wanita, #e9422e  | item 2, Pria, #009cd6 
    hideHover: 'auto'
  });

  // //Fix untuk charts dibawah tabs
  // $('.box ul.nav a').on('shown.bs.tab', function () {
  //   bar.redraw();
  // });

})(jQuery);
