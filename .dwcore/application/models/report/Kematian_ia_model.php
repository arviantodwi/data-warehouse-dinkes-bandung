<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

class Kematian_ia_model extends CI_Model {
  public function __construct() {
    parent::__construct();
  }

  public function fetch_kematian_diagnosis($formValue, $periodSpan, $puskesmasId) {
    $this->db->distinct();
    $this->db->select('pu.nama_puskesmas, k.*');
    $this->db->from('periode_dim pr, kematian_ib_fact k, puskesmas_dim pu');
    $this->db->where('pr.id = k.ID_PERIODE and pu.id = k.ID_PUSKESMAS');
    $this->db->where('k.ID_PERIODE >=', $periodSpan['start']);
    $this->db->where('k.ID_PERIODE <=', $periodSpan['end']);

    if( is_array($puskesmasId) )
      $this->db->where_in('k.ID_PUSKESMAS', $puskesmasId);
    else
      $this->db->where('k.ID_PUSKESMAS', $puskesmasId);

    $this->db->order_by('pu.nama_puskesmas', 'asc');

    $query = $this->db->get('kb');
    $result = $query->result_array();

    return $result;
  }
}
