<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

class Spreadsheet_model extends CI_Model {
  public function __construct() {
    parent::__construct();
  }
  
  public function fetch_puskesmas($id) {
    if($id == 0) {
      $command = 'select k.nama_kecamatan, p.id, p.nama_puskesmas from kewilayahan_dim k, puskesmas_dim p, puskesmas_kewilayahan_rel pk where pk.id_puskesmas = p.id and pk.id_kewilayahan = k.id group by p.nama_puskesmas order by k.nama_kecamatan asc, p.nama_puskesmas asc';
      $query = $this->db->query($command);
    }
    else {
      $command = 'select k.nama_kecamatan, p.id, p.nama_puskesmas from kewilayahan_dim k, puskesmas_dim p, puskesmas_kewilayahan_rel pk where p.id = ? and pk.id_puskesmas = p.id and pk.id_kewilayahan = k.id group by p.nama_puskesmas order by k.nama_kecamatan asc, p.nama_puskesmas asc';
      $query = $this->db->query($command, array($id));
    }
    
    $result = $query->result();
    $puskesmas = array();
    
    for($i=0; $i<count($result); $i++) {
      $kecamatan = $result[$i]->nama_kecamatan;
      $idPuskesmas = $result[$i]->id;
      $_puskesmas = $result[$i]->nama_puskesmas;
      
      if( !array_key_exists($kecamatan, $puskesmas) )
        $puskesmas[$kecamatan] = array($idPuskesmas => $_puskesmas);
      else
        $puskesmas[$kecamatan][$idPuskesmas] = $_puskesmas;
    }
    
    return $puskesmas;
  }
  
  public function insert_temp_xls() {
    
  }
}