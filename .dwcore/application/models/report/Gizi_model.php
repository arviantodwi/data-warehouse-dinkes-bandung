<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

class Gizi_model extends CI_Model {
  public function __construct() {
    parent::__construct();
  }

  public function fetch_gizi_diagnosis($periodSpan, $puskesmasId) {
    $this->db->distinct();
    $this->db->select('pu.nama_puskesmas, g.*');
    $this->db->from('periode_dim pr, gizi g, puskesmas_dim pu');
    $this->db->where('pr.id = g.ID_PERIODE and pu.id = g.ID_PUSKESMAS');
    $this->db->where('g.ID_PERIODE >=', $periodSpan['start']);
    $this->db->where('g.ID_PERIODE <=', $periodSpan['end']);

    if( is_array($puskesmasId) )
      $this->db->where_in('g.ID_PUSKESMAS', $puskesmasId);
    else
      $this->db->where('g.ID_PUSKESMAS', $puskesmasId);

    $this->db->order_by('pu.nama_puskesmas', 'asc');

    $query = $this->db->get('gizi');
    $result = $query->result_array();

    return $result;
  }
}
