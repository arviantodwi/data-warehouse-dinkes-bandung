<?php
defined('BASEPATH') or exit('Akses langsung tidak diperkenankan');

/**
 * Created by PhpStorm.
 * User: Arvianto
 * Date: 2/9/2016
 * Time: 11:45 PM
 */
class Log_model extends CI_Model {
  public function fetch_log() {
    $this->db->select('id_puskesmas, log_message, report_type, log_time');
    $this->db->order_by('log_time', 'desc');

    $query = $this->db->get('sys_change_log');
    $result = $query->result_array();

    for($i=0; $i<count($result); $i++) {
      $this->fetch_puskesmas_details( $result[$i] );
    }

    return $result;
  }

  private function fetch_puskesmas_details(&$data) {
    $this->db->select('kode_puskesmas, nama_puskesmas');
    $this->db->where('id', $data['id_puskesmas']);

    $query = $this->db->get('puskesmas_dim');
    $result = $query->row();

    if($result) {
      $data['kode_puskesmas'] = $result->kode_puskesmas;
      $data['nama_puskesmas'] = $result->nama_puskesmas;
    }
  }
}