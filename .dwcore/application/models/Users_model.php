<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

class Users_model extends CI_Model {
  private $userData = array();

  public function __construct() {
    parent::__construct();
  }

  public function get_user_data($data = 'all') {
    switch($data) {
      case 'all':
        $data = $this->userData;
      break;

      case 'user_id':
      case 'user_nip':
      case 'username':
      case 'password':
      case 'hash_map':
      case 'real_name':
      case 'gender':
      case 'account_type':
      case 'puskesmas_id':
      case 'registration_time':
        $data = $this->userData[$data];
      break;

      default:
        $data = null;
      break;
    }

    return $data;
  }

  public function fetch_global_account_type() {
    $query = $this->db->get('sys_account_type');
    $result = $query->result_array();

    return $result;
  }

  public function fetch_user_id($uname) {
    $uname = htmlspecialchars($uname, ENT_QUOTES);

    $this->db->select('user_id');
    $this->db->where('username', $uname);
    $query = $this->db->get('sys_users');

    if( $query->num_rows() == 1 ) {
      $this->userData = $query->row_array();
      $userId = $this->userData['user_id'];
    }
    else
      $userId = null;

    return $userId;
  }

  public function fetch_all_user_data($userId) {
    $this->db->select('user_nip, username, password, hash_map, real_name, gender, account_type, puskesmas_id, registration_time');
    $this->db->where('user_id', $this->userData['user_id']);
    $query = $this->db->get('sys_users');

    foreach($query->row_array() as $key => $value) {
      $this->userData[$key] = $value;
    }

    return;
  }

  public function fetch_authority($accountTypeId=null) {
    $this->db->select('show_parent_dashboard as "dashboard", show_parent_import as "import", show_parent_report as "report", show_parent_config as "config", show_parent_log as "log", show_child_import_sikda as "import_sikda", show_child_import_excel as "import_excel"');

    if($accountTypeId !== null)
      $this->db->where('account_type_id', $accountTypeId);
    else
      $this->db->select('account_type_id');

    $query = $this->db->get('sys_authority');
    $result = $query->result_array();

    if($accountTypeId !== null)
      $result = (object)$result[0];
    else {
      $tempResult = array();

      foreach($result as $value) {
        $tempResult[ $value['account_type_id'] ] = $value;
        array_pop( $tempResult[ $value['account_type_id'] ] );
      }

      $result = $tempResult;
    }

    return $result;
  }

  public function insert_user($data) {
    $this->load->library('crypto');

    $hash = $this->crypto->make_password_hash($data['password'], 'sha256');
    $combination = $this->crypto->get_combination();

    $finalData = array(
      'user_nip'          => $data['nip'],
      'username'          => $data['username'],
      'password'          => $hash,
      'hash_map'          => $combination,
      'real_name'         => $data['real_name'],
      'gender'            => $data['gender'],
      'account_type'      => $data['account_type'],
      'puskesmas_id'      => $data['puskesmas_id'],
      'registration_time' => date('Y-m-d H:i:s')
    );

    $this->db->set($finalData);
    $this->db->insert('sys_users');
  }

  public function update_authority($typeId, $options) {
    $data = array();

    foreach($options as $key => $value) {
      if( count(explode('_', $key)) == 1 )
        $data['show_parent_'.$key] = (empty($value)) ? 0 : 1;
      elseif( count(explode('_', $key)) > 1 )
        $data['show_child_'.$key] = (empty($value)) ? 0 : 1;
    }

    $this->db->where('account_type_id', $typeId);
    $this->db->update('sys_authority', $data);
  }
}
