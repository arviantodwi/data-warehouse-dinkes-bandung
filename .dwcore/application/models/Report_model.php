<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

class Report_model extends CI_Model {
  public function __construct() {
    parent::__construct();
  }

  /* public function random_update() {
     $rows = $this->db->query('select * from diagnosis_fact where id_puskesmas = 1 and id_periode = 12')->num_rows();

     $this->db->where('id_puskesmas', 1);
     $this->db->where('id_periode', 12);
     for($i=1; $i<=28; $i++) {
       $this->db->where('id_penyakit', $i);
       $this->db->update('diagnosis_fact', $this->random_data());
     }
   }*/

  /* private function random_data() {
     $cols = array('0-7hr', '8-28hr', '29hr-1th', '1-4th', '5-9th', '10-14th', '15-19th', '20-44th', '45-54th', '55-59th', '60-69th', '>-70th', 'kasus_baru', 'kasus_lama');
     $abbr = array('l', 'p');
     $randomData = array();

     foreach($abbr as $key) {
       foreach($cols as $value) {
         if($value == 'kasus_baru' || $value == 'kasus_lama')
           $randomData[$value.'_'.$key] = null;
         else
           $randomData[$value.'_'.$key] = rand(0,27);
       }
     }

     foreach($randomData as $key=>$value) {
       if($key == 'kasus_baru_l')
         $randomData[$key] = $randomData['0-7hr_l'] + $randomData['8-28hr_l'] + $randomData['29hr-1th_l'] + $randomData['1-4th_l'] + $randomData['5-9th_l'] + $randomData['10-14th_l'];
       elseif($key == 'kasus_baru_p')
         $randomData[$key] = $randomData['0-7hr_p'] + $randomData['8-28hr_p'] + $randomData['29hr-1th_p'] + $randomData['1-4th_p'] + $randomData['5-9th_p'] + $randomData['10-14th_p'];
       elseif($key == 'kasus_lama_l')
         $randomData[$key] = $randomData['15-19th_l'] + $randomData['20-44th_l'] + $randomData['45-54th_l'] + $randomData['55-59th_l'] + $randomData['60-69th_l'] + $randomData['>-70th_l'];
       elseif($key == 'kasus_lama_p')
         $randomData[$key] = $randomData['15-19th_p'] + $randomData['20-44th_p'] + $randomData['45-54th_p'] + $randomData['55-59th_p'] + $randomData['60-69th_p'] + $randomData['>-70th_p'];
     }

     $randomData['kasus_baru_jml'] = $randomData['kasus_baru_l'] + $randomData['kasus_baru_p'];
     $randomData['kasus_lama_jml'] = $randomData['kasus_lama_l'] + $randomData['kasus_lama_p'];
     $randomData['total_kasus'] = $randomData['kasus_baru_jml'] + $randomData['kasus_lama_jml'];
     $randomData['peserta_gakin'] = rand($randomData['total_kasus']/3, $randomData['total_kasus']);

     return $randomData;
   }*/

  public function get_period_id($year, $monthNum=null) {
    $this->db->select('id');
    $this->db->where('tahun', $year);

    if( !is_null($monthNum) && !empty($monthNum) ) {
      if( $monthNum !== 'all' ) {
        $this->db->where('nomor_bulan', $monthNum);
        $query = $this->db->get('periode_dim');
        $result = $query->row();

        if( isset($result) )
          $result = $result->id;
      }
      else {
        $this->db->order_by('nomor_bulan', 'asc');
        $query = $this->db->get('periode_dim');
        $result = $query->result_array();
      }
    }
    else {
      $query = $this->db->get('periode_dim');
      $result = $query->result_array();
    }

    return $result;
  }

  public function get_kecamatan_name($kecamatanCode) {
    $this->db->select('nama_kecamatan');
    $this->db->where('kode_kecamatan', $kecamatanCode);
    $query = $this->db->get('kewilayahan_dim');
    $result = $query->row();

    if( isset($result) )
      $result = $result->nama_kecamatan;

    return $result;
  }

  public function get_puskesmas_name($puskesmasId) {
    $this->db->select('nama_puskesmas');

    if( !is_array($puskesmasId) ) {
      $this->db->where('id', $puskesmasId);
      $query = $this->db->get('puskesmas_dim');
      $result = $query->row();

      if( isset($result) )
        $result = $result->nama_puskesmas;
    }
    else {
      $this->db->where_in('id', $puskesmasId);
      $this->db->order_by('nama_puskesmas', 'asc');

      $query = $this->db->get('puskesmas_dim');
      $result = $query->result_array();
    }

    return $result;
  }

  public function get_upt_from_kecamatan($kecamatanCode) {
    $this->db->select('p.nama_puskesmas');
    $this->db->from('puskesmas_dim p, puskesmas_kewilayahan_rel pkr, kewilayahan_dim k');
    $this->db->where('k.id = pkr.ID_KEWILAYAHAN and p.id = pkr.ID_PUSKESMAS');
    $this->db->where('k.kode_kecamatan', $kecamatanCode);
    $this->db->where('p.nama_puskesmas like \'UPT%\'');
    $query = $this->db->get('puskesmas_dim');
    $result = $query->row();

    if( isset($result) )
      $result = $result->nama_puskesmas;

    return $result;
  }

  public function get_puskesmas_id($code) {
    $this->db->select('id');
    $this->db->where('kode_puskesmas', $code);

    $query = $this->db->get('puskesmas_dim');
    $result = $query->row();

    if( isset($result) )
      return $result->id;
  }

  public function get_icd_code($diseaseCode, $category) {
    if($category == 'main') {
      $this->db->select('kode_icd9');
      $this->db->where('kode_penyakit', $diseaseCode);

      $query = $this->db->get('penyakit_dim');
      $result = $query->row();

      if( isset($result) )
        $result = $result->kode_icd9;
    }
    elseif($category == 'sub') {
      $this->db->select('kode_icd9, kategori2');
      $this->db->where('kode_penyakit', $diseaseCode);

      $query = $this->db->get('penyakit_dim');
      $result = $query->result_array();
    }

    return $result;
  }

  public function fetch_related_puskesmas_on_kecamatan($code) {
    $puskesmasList = $this->fetch_puskesmas($code);
    $puskesmasId = array();

    foreach($puskesmasList as $value) {
      $puskesmasId[] = $this->get_puskesmas_id( $value['kode_puskesmas'] );
    }

    return $puskesmasId;
  }

  public function get_all_puskesmas_id() {
    $this->db->select('id');
    $query = $this->db->get('puskesmas_dim');
    $tempPuskesmasId = $query->result_array();

    $result = array();

    foreach($tempPuskesmasId as $key=>$value) { $result[$key] = $value['id']; }

    return $result;
  }

  // DEPRECATED
  public function fetch_area($area, $code = null) {
    if($area == 'kecamatan') {
      $command = 'CALL kecamatan()';
      $query = $this->db->query($command);
      $result = $query->result_array();

      $query->free_result();
      $query->next_result();
    }
    elseif($area == 'puskesmas') {
      if($code !== null) {
        $command = "CALL puskesmas(?)";
        $query = $this->db->query($command, array($code));
        $result = $query->result_array();

        $query->free_result();
        $query->next_result();
      }
      else {
        $result = $this->db->get('puskesmas_dim')->result_array();
      }
    }

    return $result;
  }

  public function fetch_year($minYear=null) {
    if( is_null($minYear) || empty($minYear) ) {
      $command = 'CALL tahun()';
      $query = $this->db->query($command);
      $result = array();

      foreach( $query->result() as $row ) {
        $result[] = $row->tahun;
      }

      $query->free_result();
      $query->next_result();
    }
    else {
      $this->db->select('distinct(tahun)');
      $this->db->where('tahun >=', $minYear);
      $query = $this->db->get('periode_dim');
      $result = array();

      foreach( $query->result() as $row ) {
        $result[] = array(0 => $row->tahun, 1 => $row->tahun);
      }
    }

    return $result;
  }

  public function fetch_month($year) {
    $this->db->select('nomor_bulan, nama_bulan');
    $this->db->where('tahun', $year);
    $this->db->order_by('nomor_bulan asc');
    $query = $this->db->get('periode_dim');
    $result = $query->result_array();

    return $result;
  }

  public function fetch_kecamatan() {
    $command = 'CALL kecamatan()';
    $query = $this->db->query($command);
    $result = $query->result_array();

    $query->free_result();
    $query->next_result();

    return $result;
  }

  public function fetch_puskesmas($kecamatanCode=null) {
    if( !is_null($kecamatanCode) && !empty($kecamatanCode) ) {
      $command = "CALL puskesmas(?)";
      $query = $this->db->query($command, array($kecamatanCode));
      $result = $query->result_array();

      $query->free_result();
      $query->next_result();
    }
    else {
      $query = $this->db->get('puskesmas_dim');
      $result = $query->result_array();
    }

    return $result;
  }

  public function fetch_diseases($code=null) {
    if( !is_null($code) && !empty($code) ) {
      $this->db->where('kode_penyakit', $code);
    }

    $query = $this->db->get('penyakit_dim');
    $result = $query->result_array();

    return $result;
  }

  public function fetch_diseases_code($param, $whereTarget) {
    $this->db->select('kode_penyakit');
    $this->db->where($whereTarget, $param);

    $query = $this->db->get('penyakit_dim');
    $result = $query->row();

    if( isset($result) )
      return $result->kode_penyakit;
  }

  public function fetch_diagnosis($formValue, $periodSpan, $puskesmasId) {
    $command = 'select distinct pr.nama_bulan, pr.tahun, d.*, pu.nama_puskesmas, p.kode_penyakit, p.kategori2, p.kode_icd9, p.nama_penyakit from diagnosis_fact d, penyakit_dim p, periode_dim pr, puskesmas_dim pu where p.id = d.id_penyakit and pr.id = d.id_periode and pu.id = d.id_puskesmas';

    if( !is_null($formValue['disease_id']) && is_array($formValue['disease_id']) ) {
      $command .= ' and p.id in (';

      for($i=0; $i<count($formValue['disease_id']); $i++) {
        $command .= ( $i != count($formValue['disease_id'])-1 ) ? $formValue['disease_id'][$i].',' : $formValue['disease_id'][$i];
      }

      $command .= ')';
    }

    $command .= ' and d.id_periode between '.$periodSpan['start'].' and '.$periodSpan['end'];

    if( is_array($puskesmasId) ) {
      $command .= ' and d.id_puskesmas in (';

      for($i=0; $i<count($puskesmasId); $i++) {
        $command .= ( $i != count($puskesmasId)-1 ) ? $puskesmasId[$i].',' : $puskesmasId[$i];
      }

      $command .= ')';
    }
    else
      $command .= ' and d.id_puskesmas = '.$puskesmasId;

    $command .= ' order by p.kode_penyakit asc';
    //$command .= ' order by pr.nomor_bulan asc, pr.tahun asc, p.kode_penyakit asc, p.kategori1 asc';

    $query = $this->db->query($command);
    $result = $query->result_array();

    return $result;
  }
}
