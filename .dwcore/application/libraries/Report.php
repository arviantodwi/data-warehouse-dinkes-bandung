<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

class Report  {
  private $reportData = array(
    'markup' => array(),
    'model'  => array()
  );

  public function __construct() {
    $this->reportData['markup'] = array(
      'active_view' => 'cpanel_view',
      'body_class'  => 'hold-transition skin-green-light sidebar-mini'
    );
  }

  public function set_report_markup_data() {
    $argsNum = func_num_args();

    if( $argsNum == 1 && is_array( func_get_arg(0) ) ) {
      $argsList = func_get_arg(0);

      foreach($argsList as $key=>$value) {
        $this->reportData['model'][$key] = $value;
      }
    }
    elseif($argsNum == 2) {
      $key = func_get_arg(0);
      $value = func_get_arg(1);
      $this->reportData['markup'][$key] = $value;
    }
  }

  public function set_report_model_data() {
    $argsNum = func_num_args();

    if( $argsNum == 1 && is_array( func_get_arg(0) ) ) {
      $argsList = func_get_arg(0);

      foreach($argsList as $key=>$value) {
        $this->reportData['model'][$key] = $value;
      }
    }
    elseif($argsNum == 2) {
      $key = func_get_arg(0);
      $value = func_get_arg(1);
      $this->reportData['model'][$key] = $value;
    }
  }

  public function get_report_data() {
    return $this->reportData;
  }
  
  public function check_nulled_value_posted() {
    $nulledFound = FALSE;
    
    foreach($valuePosted as $key=>$value) {
      if( is_array($valuePosted[$key]) ) {
        foreach($value as $subkey=>$subvalue) {
          if($key == 'period_end' && is_null($subvalue) && $valuePosted['period_start']['month'] == 'all' )
            continue;

          if( is_null($subvalue) ) {
            $nulledFound = TRUE;
            break;
          }
        }
      }
      else {
        if( is_null($value) ) {
          if( $key == 'puskesmas_code' && $valuePosted['kecamatan_code'] == 'all' )
            continue;

          $nulledFound = TRUE;
          break;
        }
      }

      if( $nulledFound ) break;
    }
  }
  
  public function generate_period_string() {
    $monthString = array(1=>'Januari', 2=>'Februari', 3=>'Maret', 4=>'April', 5=>'Mei', 6=>'Juni', 7=>'Juli', 8=>'Agustus', 9=>'September', 10=>'Oktober', 11=>'November', 12=>'Desember');
    
    if( $valuePosted['period_start']['month'] !== 'all' ) {
      $valuePosted['period_start'] = $monthString[ $valuePosted['period_start']['month'] ].' '.$valuePosted['period_start']['year'];
      $valuePosted['period_end'] = $monthString[ $valuePosted['period_end']['month'] ].' '.$valuePosted['period_end']['year'];
    }
    else {
      $year = $valuePosted['period_start']['year'];
      $valuePosted['period_start'] = $monthString[1].' '.$year;
      $valuePosted['period_end'] = $monthString[12].' '.$year;
    }
  }
  
  public function generate_kecamatan_puskesmas_name() {
    if( $valuePosted['kecamatan_code'] != 'all' ) {
      $valuePosted['kecamatan_name'] = $this->report_model->get_kecamatan_name( $valuePosted['kecamatan_code'] );
      $valuePosted['puskesmas_name'] = $this->report_model->get_puskesmas_name( $puskesmasId );

      if( is_array($valuePosted['puskesmas_name']) ) {
        $tempPuskesmasName = '';

        for($i=0; $i<count( $valuePosted['puskesmas_name'] ); $i++) {
          $tempPuskesmasName .= $valuePosted['puskesmas_name'][$i]['nama_puskesmas'];

          if($i !== count( $valuePosted['puskesmas_name'] )-1)
            $tempPuskesmasName .= ', ';
        }

        $valuePosted['puskesmas_name'] = $tempPuskesmasName;
      }

      $valuePosted['upt_name'] = ( substr($valuePosted['puskesmas_name'], 0, 3) == 'UPT' ) ? $valuePosted['puskesmas_name'] : $this->report_model->get_upt_from_kecamatan( $valuePosted['kecamatan_code'] );
    }
    else {
      $valuePosted['kecamatan_name'] = 'Semua Kecamatan';
      $valuePosted['puskesmas_name'] = '-';
      $valuePosted['upt_name'] = '-';
    }
  }
  
  public function recurse_array_search($needle, $haystack, $strict=FALSE) {
    $foundInArray = FALSE;
    
    foreach ($haystack as $item) {
      if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->recurse_array_search($needle, $item, $strict))) {
        $foundInArray = TRUE;
      }
    }

    return $foundInArray;
  }

  public function lb1_diseases_data($diseases) {
    foreach($diseases as $key=>$value) {
      $codeChunks = explode( '.', $value['kode_penyakit'] );

      if( count($codeChunks) > 1 ) {
        if( $value['kode_penyakit'] != $value['kategori2'] ) {
          $this->reportData['model']['diseases'][] = array(
            'id'   => $value['id'],
            'name' => $value['nama_penyakit']
          );
        }
      }
    }
  }
}