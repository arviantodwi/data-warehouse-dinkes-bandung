<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

/**
 * Class Crypto
 */
class Crypto {
  /**
   * @var string
   */
  private $salt = '';
  /**
   * @var int
   */
  protected $saltLength = 10;
  /**
   * @var string
   */
  private $combination = '';
  /**
   * @var array
   */
  private $dictionary = array(
    // Alphabet chars
    0 => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
    // Numeric chars
    1 => '0123456789',
    // Symbol chars
    2 => '`~!@#$%^&*()-_=+|]}[{;:/?.>,<'
  );

  /**
   * Crypto constructor.
   */
  public function __construct() {}

  /**
   * @return string
   */
  public function get_salt() {
    return $this->salt;
  }

  /**
   *
   */
  public function set_salt() {}

  /**
   * @return string
   */
  public function get_combination() {
    return $this->combination;
  }

  /**
   * @param $text
   */
  public function set_combination($text) {
    $this->combination = $text;
  }

  /**
   * @param $number
   */
  public function set_salt_length($number) {
    $this->saltLength = $number;
  }

  /**
   * @param      $hashMap
   * @param bool $returnToCaller
   *
   * @return string
   */
  public function retrieve_salt($hashMap, $returnToCaller = FALSE) {
    $chunks = str_split($hashMap, 3);
    $retrieved = '';
    
    foreach($chunks as $key=>$value) {
      $retrieved .= $this->dictionary[ (int)$value[0] ][ (int)substr($value, 1, 2) ];
    }
    
    $this->salt = $retrieved;
    
    unset($chunks);
    
    if($returnToCaller) {
      return $retrieved;
    }
  }

  /**
   * @param        $text
   * @param string $algo
   *
   * @return bool|mixed|string
   */
  public function make_password_hash($text, $algo = 'bcrypt') {
    $algo = strtoupper($algo);
    
    if($algo == 'BCRYPT') {
      $this->saltLength = 22;
      $this->generate_salt();
      $configs = array(
        'salt' => $this->salt,
        'cost' => 12
      );
      $hash = password_hash($text, PASSWORD_BCRYPT, $configs);
    }
    else {
      if($this->salt == '')
        $this->generate_salt();
        
      $text = $text.$this->salt;
      
      switch($algo) {
        // supported SHA algorithm - strong
        case 'SHA224':
        case 'SHA256':
        case 'SHA384':
        case 'SHA512':
          $hash = hash($algo, $text, FALSE);
        break;
        // default algorithm - weak
        case 'SHA1':
        default:
          $hash = hash('SHA1', $text, FALSE);
        break;
      }
    }
    
    return $hash;
  }

  /**
   *
   */
  public function make_nonce_hash() {}

  /**
   *
   */
  private function generate_salt() {
    $saltTemp = '';
    $combinationTemp = '';
    $length = ($this->saltLength < 10) ? 10 : $this->saltLength;
    
    for($i=0; $i<$length; $i++) {
      // Randomize the dictionary type for N-char of salt
      $randomDictionary = mt_rand( 0, count($this->dictionary)-1 );

      // Get related char from the random dictionary type
      $randomChar = mt_rand( 0, strlen($this->dictionary[$randomDictionary])-1 );
      // Deprecated since v1.0.1
      /*switch($randomDictionary) {
        case 0: $randomChar = mt_rand( 0, 51 ); break;
        case 1: $randomChar = mt_rand( 0, 9 ); break;
        case 2: $randomChar = mt_rand( 0, 28 ); break;
      }*/

      $saltTemp .= $this->dictionary[$randomDictionary][$randomChar];
      $combinationTemp .= ($randomChar < 10) ? "{$randomDictionary}0{$randomChar}" : "{$randomDictionary}{$randomChar}";
    }
    
    $this->salt = $saltTemp;
    $this->combination = $combinationTemp;
  }
}