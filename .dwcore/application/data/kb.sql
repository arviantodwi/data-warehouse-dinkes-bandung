# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.46-0ubuntu0.14.04.2)
# Database: datakesb_dw
# Generation Time: 2015-12-05 04:12:03 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table kb
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kb`;

CREATE TABLE `kb` (
  `ID_PUSKESMAS` int(11) DEFAULT NULL,
  `ID_PERIODE` int(11) DEFAULT NULL,
  `AKS_POST_PARTUM_42_H` int(11) DEFAULT NULL,
  `AKS_BARU_PIL` int(11) DEFAULT NULL,
  `AKS_BARU_SUNTIK` int(11) DEFAULT NULL,
  `AKS_BARU_AKDR` int(11) DEFAULT NULL,
  `AKS_BARU_IMPLANT` int(11) DEFAULT NULL,
  `AKS_BARU_KONDOM` int(11) DEFAULT NULL,
  `AKS_BARU_MOW` int(11) DEFAULT NULL,
  `AKS_BARU_MOP` int(11) DEFAULT NULL,
  `AKS_BARU_LAIN` int(11) DEFAULT NULL,
  `AKS_LAMA` int(11) DEFAULT NULL,
  `AKS_AKTIF_PIL` int(11) DEFAULT NULL,
  `AKS_AKTIF_SUNTIK` int(11) DEFAULT NULL,
  `AKS_AKTIF_AKDR` int(11) DEFAULT NULL,
  `AKS_AKTIF_IMPLANT` int(11) DEFAULT NULL,
  `AKS_AKTIF_KONDOM` int(11) DEFAULT NULL,
  `AKS_AKTIF_MOW` int(11) DEFAULT NULL,
  `AKS_AKTIF_MOP` int(11) DEFAULT NULL,
  `AKS_AKTIF_LAIN` int(11) DEFAULT NULL,
  `EFEK_SAMPING_PIL` int(11) DEFAULT NULL,
  `EFEK_SAMPING_SUNTIK` int(11) DEFAULT NULL,
  `EFEK_SAMPING_AKDR` int(11) DEFAULT NULL,
  `EFEK_SAMPING_IMPLANT` int(11) DEFAULT NULL,
  `EFEK_SAMPING_KONDOM` int(11) DEFAULT NULL,
  `EFEK_SAMPING_MOW` int(11) DEFAULT NULL,
  `EFEK_SAMPING_MOP` int(11) DEFAULT NULL,
  `EFEK_SAMPING_LAIN` int(11) DEFAULT NULL,
  `KOMPLIKASI_PIL` int(11) DEFAULT NULL,
  `KOMPLIKASI_SUNTIK` int(11) DEFAULT NULL,
  `KOMPLIKASI_AKDR` int(11) DEFAULT NULL,
  `KOMPLIKASI_IMPLANT` int(11) DEFAULT NULL,
  `KOMPLIKASI_KONDOM` int(11) DEFAULT NULL,
  `KOMPLIKASI_MOW` int(11) DEFAULT NULL,
  `KOMPLIKASI_MOP` int(11) DEFAULT NULL,
  `KOMPLIKASI_LAIN` int(11) DEFAULT NULL,
  `GAGAL_PIL` int(11) DEFAULT NULL,
  `GAGAL_SUNTIK` int(11) DEFAULT NULL,
  `GAGAL_AKDR` int(11) DEFAULT NULL,
  `GAGAL_IMPLANT` int(11) DEFAULT NULL,
  `GAGAL_KONDOM` int(11) DEFAULT NULL,
  `GAGAL_MOW` int(11) DEFAULT NULL,
  `GAGAL_MOP` int(11) DEFAULT NULL,
  `GAGAL_LAIN` int(11) DEFAULT NULL,
  `DO_PIL` int(11) DEFAULT NULL,
  `DO_SUNTIK` int(11) DEFAULT NULL,
  `DO_AKDR` int(11) DEFAULT NULL,
  `DO_IMPLANT` int(11) DEFAULT NULL,
  `DO_KONDOM` int(11) DEFAULT NULL,
  `DO_MOW` int(11) DEFAULT NULL,
  `DO_MOP` int(11) DEFAULT NULL,
  `DO_LAIN` int(11) DEFAULT NULL,
  `PUS_KIN_PIL` int(11) DEFAULT NULL,
  `PUS_KIN_SUNTIK` int(11) DEFAULT NULL,
  `PUS_KIN_AKDR` int(11) DEFAULT NULL,
  `PUS_KIN_IMPLANT` int(11) DEFAULT NULL,
  `PUS_KIN_KONDOM` int(11) DEFAULT NULL,
  `PUS_KIN_MOW_MOP` int(11) DEFAULT NULL,
  `PUS_4T_PIL` int(11) DEFAULT NULL,
  `PUS_4T_SUNTIK` int(11) DEFAULT NULL,
  `PUS_4T_AKDR` int(11) DEFAULT NULL,
  `PUS_4T_IMPLANT` int(11) DEFAULT NULL,
  `PUS_4T_KONDOM` int(11) DEFAULT NULL,
  `PUS_4T_MOW_MOP` int(11) DEFAULT NULL,
  `STOK_PIL` int(11) DEFAULT NULL,
  `STOK_SUNTIK` int(11) DEFAULT NULL,
  `STOK_AKDR` int(11) DEFAULT NULL,
  `STOK_IMPLANT` int(11) DEFAULT NULL,
  `STOK_KONDOM` int(11) DEFAULT NULL,
  `TERIMA_PIL` int(11) DEFAULT NULL,
  `TERIMA_SUNTIK` int(11) DEFAULT NULL,
  `TERIMA_AKDR` int(11) DEFAULT NULL,
  `TERIMA_IMPLANT` int(11) DEFAULT NULL,
  `TERIMA_KONDOM` int(11) DEFAULT NULL,
  `PEMAKAIAN_PIL` int(11) DEFAULT NULL,
  `PEMAKAIAN_SUNTIK` int(11) DEFAULT NULL,
  `PEMAKAIAN_AKDR` int(11) DEFAULT NULL,
  `PEMAKAIAN_IMPLANT` int(11) DEFAULT NULL,
  `PEMAKAIAN_KONDOM` int(11) DEFAULT NULL,
  `SISA_PIL` int(11) DEFAULT NULL,
  `SISA_SUNTIK` int(11) DEFAULT NULL,
  `SISA_AKDR` int(11) DEFAULT NULL,
  `SISA_IMPLANT` int(11) DEFAULT NULL,
  `SISA_KONDOM` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `kb` WRITE;
/*!40000 ALTER TABLE `kb` DISABLE KEYS */;

INSERT INTO `kb` (`ID_PUSKESMAS`, `ID_PERIODE`, `AKS_POST_PARTUM_42_H`, `AKS_BARU_PIL`, `AKS_BARU_SUNTIK`, `AKS_BARU_AKDR`, `AKS_BARU_IMPLANT`, `AKS_BARU_KONDOM`, `AKS_BARU_MOW`, `AKS_BARU_MOP`, `AKS_BARU_LAIN`, `AKS_LAMA`, `AKS_AKTIF_PIL`, `AKS_AKTIF_SUNTIK`, `AKS_AKTIF_AKDR`, `AKS_AKTIF_IMPLANT`, `AKS_AKTIF_KONDOM`, `AKS_AKTIF_MOW`, `AKS_AKTIF_MOP`, `AKS_AKTIF_LAIN`, `EFEK_SAMPING_PIL`, `EFEK_SAMPING_SUNTIK`, `EFEK_SAMPING_AKDR`, `EFEK_SAMPING_IMPLANT`, `EFEK_SAMPING_KONDOM`, `EFEK_SAMPING_MOW`, `EFEK_SAMPING_MOP`, `EFEK_SAMPING_LAIN`, `KOMPLIKASI_PIL`, `KOMPLIKASI_SUNTIK`, `KOMPLIKASI_AKDR`, `KOMPLIKASI_IMPLANT`, `KOMPLIKASI_KONDOM`, `KOMPLIKASI_MOW`, `KOMPLIKASI_MOP`, `KOMPLIKASI_LAIN`, `GAGAL_PIL`, `GAGAL_SUNTIK`, `GAGAL_AKDR`, `GAGAL_IMPLANT`, `GAGAL_KONDOM`, `GAGAL_MOW`, `GAGAL_MOP`, `GAGAL_LAIN`, `DO_PIL`, `DO_SUNTIK`, `DO_AKDR`, `DO_IMPLANT`, `DO_KONDOM`, `DO_MOW`, `DO_MOP`, `DO_LAIN`, `PUS_KIN_PIL`, `PUS_KIN_SUNTIK`, `PUS_KIN_AKDR`, `PUS_KIN_IMPLANT`, `PUS_KIN_KONDOM`, `PUS_KIN_MOW_MOP`, `PUS_4T_PIL`, `PUS_4T_SUNTIK`, `PUS_4T_AKDR`, `PUS_4T_IMPLANT`, `PUS_4T_KONDOM`, `PUS_4T_MOW_MOP`, `STOK_PIL`, `STOK_SUNTIK`, `STOK_AKDR`, `STOK_IMPLANT`, `STOK_KONDOM`, `TERIMA_PIL`, `TERIMA_SUNTIK`, `TERIMA_AKDR`, `TERIMA_IMPLANT`, `TERIMA_KONDOM`, `PEMAKAIAN_PIL`, `PEMAKAIAN_SUNTIK`, `PEMAKAIAN_AKDR`, `PEMAKAIAN_IMPLANT`, `PEMAKAIAN_KONDOM`, `SISA_PIL`, `SISA_SUNTIK`, `SISA_AKDR`, `SISA_IMPLANT`, `SISA_KONDOM`)
VALUES
	(1,25,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5);

/*!40000 ALTER TABLE `kb` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
