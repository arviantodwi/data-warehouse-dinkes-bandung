# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.46-0ubuntu0.14.04.2)
# Database: datakesb_dw
# Generation Time: 2015-12-09 16:10:32 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table sys_authority
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_authority`;

CREATE TABLE `sys_authority` (
  `auth_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `account_type_id` tinyint(3) unsigned NOT NULL,
  `show_parent_dashboard` tinyint(1) NOT NULL DEFAULT '1',
  `show_parent_import` tinyint(1) NOT NULL DEFAULT '1',
  `show_parent_report` tinyint(1) NOT NULL DEFAULT '1',
  `show_parent_config` tinyint(1) NOT NULL DEFAULT '1',
  `show_parent_log` tinyint(1) NOT NULL DEFAULT '1',
  `show_child_import_sikda` tinyint(1) NOT NULL DEFAULT '1',
  `show_child_import_excel` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`auth_id`),
  KEY `fk_accountType_id` (`account_type_id`),
  CONSTRAINT `fk_accountType_id` FOREIGN KEY (`account_type_id`) REFERENCES `sys_account_type` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sys_authority` WRITE;
/*!40000 ALTER TABLE `sys_authority` DISABLE KEYS */;

INSERT INTO `sys_authority` (`auth_id`, `account_type_id`, `show_parent_dashboard`, `show_parent_import`, `show_parent_report`, `show_parent_config`, `show_parent_log`, `show_child_import_sikda`, `show_child_import_excel`)
VALUES
	(1,1,1,1,1,1,1,1,1),
	(2,2,1,1,1,0,1,1,1),
	(3,4,0,1,0,0,1,0,1),
	(4,3,1,0,1,0,1,0,0);

/*!40000 ALTER TABLE `sys_authority` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sys_change_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_change_log`;

CREATE TABLE `sys_change_log` (
  `id_log` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_puskesmas` int(10) unsigned NOT NULL,
  `log_message` text NOT NULL,
  `report_type` varchar(5) NOT NULL,
  `log_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
