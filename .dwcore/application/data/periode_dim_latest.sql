# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.46-0ubuntu0.14.04.2)
# Database: datakesb_dw
# Generation Time: 2015-12-08 15:50:22 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table periode_dim
# ------------------------------------------------------------

LOCK TABLES `periode_dim` WRITE;
/*!40000 ALTER TABLE `periode_dim` DISABLE KEYS */;

INSERT INTO `periode_dim` (`id`, `nama_bulan`, `nomor_bulan`, `tahun`)
VALUES
	(12,'Desember',12,2013),
	(11,'November',11,2013),
	(10,'Oktober',10,2013),
	(9,'Septemer',9,2013),
	(8,'Agustus',8,2013),
	(7,'Juli',7,2013),
	(6,'Juni',6,2013),
	(5,'Mei',5,2013),
	(4,'April',4,2013),
	(3,'Maret',3,2013),
	(2,'Februari',2,2013),
	(1,'Januari',1,2013),
	(13,'Januari',1,2014),
	(14,'Februari',2,2014),
	(15,'Maret',3,2014),
	(16,'April',4,2014),
	(17,'Mei',5,2014),
	(18,'Juni',6,2014),
	(19,'Juli',7,2014),
	(20,'Agustus',8,2014),
	(21,'Septemer',9,2014),
	(22,'Oktober',10,2014),
	(23,'November',11,2014),
	(24,'Desember',12,2014),
	(25,'Januari',1,2015),
	(26,'Februari',2,2015),
	(27,'Maret',3,2015),
	(28,'April',4,2015),
	(29,'Mei',5,2015),
	(30,'Juni',6,2015),
	(31,'Juli',7,2015),
	(32,'Agustus',8,2015),
	(33,'Septemer',9,2015),
	(34,'Oktober',10,2015),
	(35,'November',11,2015),
	(36,'Desember',12,2015),
	(37,'Januari',1,2016),
	(38,'Februari',2,2016),
	(39,'Maret',3,2016),
	(40,'April',4,2016),
	(41,'Mei',5,2016),
	(42,'Juni',6,2016),
	(43,'Juli',7,2016),
	(44,'Agustus',8,2016),
	(45,'September',9,2016),
	(46,'Oktober',10,2016),
	(47,'November',11,2016),
	(48,'Desember',12,2016);

/*!40000 ALTER TABLE `periode_dim` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
