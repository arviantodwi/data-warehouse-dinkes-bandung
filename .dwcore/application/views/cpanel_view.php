<?php
$this->load->view('layouts/header');
$this->load->view('layouts/sidebar');
?>
<div class="content-wrapper">
  <?php
    $this->load->view('layouts/topbar');
    
    switch( count($markup['uri']) ) {
      case 1:
        $this->load->view('contents/'.$markup['uri'][0]);
      break;
      
      case 2:
      default:
        $this->load->view('contents/'.$markup['uri'][0].'/'.$markup['uri'][1]);
      break;
    }

    $this->load->view('layouts/footer');
  ?>
</div>