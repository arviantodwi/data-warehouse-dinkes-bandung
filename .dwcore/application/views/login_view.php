<div class="login-box">
  <div class="login-logo clearfix">
    <img src="<?php echo base_url('images/icon-bandung-big.png'); ?>" />
    <div class="title">
      <span><strong>Data Warehouse</strong></span>
      <span>Dinas Kesehatan Kota Bandung</span>
    </div>
  </div><!-- /.login-logo -->
  <div class="login-box-body">
    <div class="ruler"></div>
    
    <?php
      $className = 'feedback-notification';
      $message = '';

      if( isset($_SESSION['validationError']) || isset($_SESSION['credentialExpired']) ) {
        $className .= ' has-error';

        if( isset($_SESSION['validationError']) && $_SESSION['validationError'] === TRUE ) {
          $message = "<strong>Terdapat kesalahan informasi <em>log in</em>.</strong> Silakan cek dan masukkan kembali ID Pengguna dan Kata Sandi Anda.";
        }
        elseif( isset($_SESSION['credentialExpired']) && $_SESSION['credentialExpired'] === TRUE ) {
          $message = "<strong>Sesi <em>log in</em> Anda telah habis.</strong> Akun Anda tidak aktif selama 30 menit dan otomatis di-<em>log out</em> oleh sistem.";
        }
      }

      echo "<div id=\"login-response\" class=\"{$className}\">{$message}</div>";
    ?>

    <?php echo form_open('auth/login/verify'); ?>
      <div class="form-group fg-login has-feedback">
        <?php
          $attr = array('class' => 'form-control', 'name'  => 'login_uname', 'placeholder' => 'ID Pengguna');
          echo form_input($attr);
        ?>
        <!--<span class="fa fa-user form-control-feedback"></span>-->
      </div>
      <div class="form-group fg-login has-feedback">
        <?php
          $attr = array('class' => 'form-control', 'name' => 'login_upass', 'placeholder' => 'Kata Sandi');
          echo form_password($attr);
        ?>
        <!--<span class="fa fa-lock form-control-feedback"></span>-->
      </div>
      <div class="form-group fg-login has-feedback">
          <?php
            $attr = array('class' => 'btn btn-success btn-block', 'type' => 'submit', 'content' => 'Masuk');
            echo form_button($attr);
          ?>
      </div>
    <?php echo form_close(); ?>

    <div class="ruler"></div>
  </div><!-- /.login-box-body -->
  <div class="login-foot">
    <div class="credit" style="width: 100%;">
      <span class="text-center">
        <strong>&copy; 2015 Dinas Kesehatan Kota Bandung.</strong>
      </span>
      <span class="text-center">
        <strong>Semua hak dilindungi.</strong>
      </span>
    </div>
    <!-- <div class="logo-bandung">
      <img src="<?php echo base_url('images/icon-bandung.png'); ?>"></img>
    </div> -->
  </div><!-- /.login-foot -->
</div><!-- /.login-box -->
