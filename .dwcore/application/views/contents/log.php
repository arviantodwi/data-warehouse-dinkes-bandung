<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <div class="box-header">
          <h3 class="box-title">Tabel Riwayat Perubahan</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <table id="table-logs" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>Tanggal</th>
                <th>Jenis Laporan</th>
                <th>Kode & Nama Pusk.</th>
                <th>Keterangan Riwayat</th>
              </tr>
            </thead>
            <tbody>
              <?php echo $model['logs_table']; ?>
            </tbody>
            <!--<tfoot>
              <tr>
                <th>Tanggal</th>
                <th>Nama Puskesmas</th>
                <th>Keterangan</th>
              </tr>
            </tfoot>-->
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
<script>
  $(function() {
    $("#table-logs").DataTable({
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "paging": true,
      "autoWidth": false
    });
  });
</script>