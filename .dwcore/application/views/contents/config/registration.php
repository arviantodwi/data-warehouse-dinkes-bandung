<section class="content">
  <?php if( isset($_SESSION['register_error']) && $_SESSION['register_error'] === TRUE): ?>
    <div class="error">Registration Error!</div>
  <?php endif; ?>
  <?php echo form_open('config/registration/verify'); ?>
    <!--<div class="box box-danger box-solid">-->
    <div class="box box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Tambah Pengguna</h3>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-12 col-sm-12" style="position:relative;">
            <div class="col-md-6 form-group">
              <label>Nama Pegawai</label>
              <input type="text" class="form-control" placeholder="Nama Pegawai" name="reg_real_name" />
            </div><!-- /.form-group -->
          </div>
          <div class="col-md-12 col-sm-12" style="position:relative;">
            <!--<div class="col-md-6 col-sm-12 form-group has-error">-->
            <div class="col-md-6 col-sm-12 form-group">
              <label>Nomor Induk Pegawai</label>
              <input type="text" class="form-control" id="inputWarning" placeholder="0000 000 0000" name="reg_nip" />
            </div>
            <!--<div class="col-md-6 col-sm-6" style="position:absolute; bottom:15px; right:15px;">
              <div class="alert alert-danger alert-dismissable" style="margin:0; height:34px; padding:6px 35px 6px 15px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="icon fa fa-ban"></i>
                Nomor Induk Pegawai telah terdaftar
              </div>
            </div>-->
          </div>
          <div class="col-md-12 col-sm-12" style="position:relative;">
            <!--<div class="col-md-6 col-sm-12 form-group has-warning">-->
            <div class="col-md-6 col-sm-12 form-group">
              <label>ID Pengguna</label>
              <input type="text" class="form-control" placeholder="id_pengguna" name="reg_username" />
            </div>
            <!--<div class="col-md-6 col-sm-6" style="position:absolute; bottom:15px; right:15px;">
              <div class="alert alert-warning alert-dismissable" style="margin:0; height:34px; padding:6px 35px 6px 15px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="icon fa fa-warning"></i>
                Hanya boleh mengandung karakter huruf, angka, tanda garis bawah ( _ ) dan titik ( . )
              </div>
            </div>-->
          </div><!-- /.form-group -->
          <div class="col-md-12 col-sm-12" style="position:relative;">
            <div class="col-md-6 col-sm-12 form-group">
              <label>Kata Sandi</label>
              <input type="password" class="form-control" id="inputPassword3" placeholder="Masukan Kata Sandi"  name="reg_userpass[]" />
            </div>
          </div>
          <div class="col-md-12 col-sm-12" style="position:relative;">
            <!--<div class="col-md-6 col-sm-12 form-group has-success">-->
            <div class="col-md-6 col-sm-12 form-group">
              <label>Ulang Kata Sandi</label>
              <input type="password" class="form-control" id="inputPassword3" placeholder="Ulang Kata Sandi"  name="reg_userpass[]" />
            </div>
            <!--<div class="col-md-6 col-sm-6" style="position:absolute; bottom:15px; right:15px;">
              <div class="alert alert-success alert-dismissable" style="margin:0; height:34px; padding:6px 35px 6px 15px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="icon fa fa-check"></i>
                Kata Sandi sesuai.
              </div>
            </div>-->
          </div><!-- /.form-group -->
          <div class="col-md-12 col-sm-12" style="position:relative;">
            <div class="col-md-6 col-sm-6 form-group">
              <label>Jenis Akun</label>
              <select class="form-control" name="reg_account_type" style="width: 100%;">
                <option selected="selected" disabled="disabled" value="none">Pilih Jenis Akun</option> 
                <?php foreach($model['account_type'] as $value): ?>
                <option value="<?php echo $value['type_id']; ?>"><?php echo ucwords($value['type_name']); ?></option> 
                <?php endforeach; ?>
              </select>
            </div><!-- /.form-group -->
          </div>
          <div class="col-md-12 col-sm-12 puskesmas-container" style="position:relative; display: none;">
            <div class="col-md-6 col-sm-6 form-group">
              <label>Nama Puskesmas</label>
              <select class="form-control" name="reg_puskesmas" disabled="disabled" style="width: 100%;">
                <option selected="selected" disabled="disabled" value="none">Pilih Puskesmas</option>
                <?php
                  $divider = '';
                  
                  for($i=0; $i<50; $i++):
                    $divider .= '-';
                  endfor;
                  
                  foreach($model['puskesmas'] as $key=>$value):
                    echo '<option disabled="disabled" value="none">'.$divider.'</option>';
                    echo '<option disabled="disabled" value="none">KECAMATAN '.$key.'</option>';
                    echo '<option disabled="disabled" value="none">'.$divider.'</option>';
                    foreach($model['puskesmas'][$key] as $index=>$data):
                      echo '<option style="padding-left:17px;" value="'.$index.'">'.$data.'</option>';
                    endforeach;
                  endforeach;
                ?>
              </select>
            </div><!-- /.form-group -->
          </div>
<!--          <div class="col-md-12 col-sm-12" style="position:relative;">-->
<!--            <div class="col-md-6 col-sm-6 form-group">-->
<!--              <label>Jenis Kelamin</label>-->
<!--              <div class="checkbox">-->
<!--                <input type="radio" name="reg_gender" id="optionsRadios2" value="P" style="vertical-align: middle; margin: 0px;">-->
<!--                <label for="optionsRadios2" style="vertical-align: middle; padding-left: 4px; margin-right: 17px;">Pria</label>-->
<!--                <input type="radio" name="reg_gender" id="optionsRadios3" value="W" style="vertical-align: middle; margin: 0px;">-->
<!--                <label for="optionsRadios3" style="vertical-align: middle; padding-left: 4px; margin-right: 17px;">Wanita</label>-->
<!--              </div>-->
<!--            </div>-->
            <!-- /.form-group -->
<!--          </div>-->
          <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.box-body -->
      <div class="box-footer">
        <button type="submit" class="btn btn-success" style="margin-left:15px; margin-bottom:5px;">Daftarkan Pengguna</button>
      </div>
    </div><!-- /.box -->
  <?php echo form_close(); ?>
</section><!-- /.content -->
<script type="text/javascript">
(function(jq) {
  jq('select[name=reg_account_type]').on('change', function(ev) {
    if( $(this)[0].value === '4' ) {
      $('select[name=reg_puskesmas]').prop('disabled', false).parents('.puskesmas-container').show();
    }
    else {
      $('select[name=reg_puskesmas]').prop('disabled', true).parents('.puskesmas-container').hide();
    }
  });
})(jQuery);
</script>