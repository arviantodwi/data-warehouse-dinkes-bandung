<section class="content">
  <?php echo form_open('config/authority/verify'); ?>
  <div class="box box-solid">
    <div class="box-header with-border">
      <h3 class="box-title">Pengaturan Hak Akses</h3>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-3">
				  <div class="box-body">
            <div class="form-group">
              <label>Jenis Akun</label>
              <select class="form-control" name="priv_account_type" style="width: 100%;">
                <option value="none" selected="selected" disabled="disabled">Pilih Jenis Akun</option>
                <?php foreach($model['account_type'] as $key => $value): ?>
                <option value="<?php echo $value['type_id']; ?>" data-privileges='<?php echo json_encode($model['privileges'][$value['type_id']]); ?>'><?php echo ucwords($value['type_name']); ?></option>
                <?php endforeach; ?>
              </select>
            </div><!-- /.form-group -->
          </div><!-- /.box-body -->
				</div><!-- /.col -->
			</div><!-- /.row -->
			<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12 privileges-container" style="opacity: 0.45;">
					<div class="box-header with-border">
						<h3 class="box-title">Hak Akses Halaman</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
            <div class="checkbox">
							<input id="priv-01" type="checkbox" name="priv_opt_dashboard" disabled="disabled" data-privilege-index="dashboard" style="vertical-align: middle; margin: 0px; position: relative;">
              <label for="priv-01" style="vertical-align: middle; padding-left: 4px; margin-right: 17px;">Beranda</label>
            </div>
            <div class="checkbox">
							<input id="priv-02" type="checkbox" name="priv_opt_import" disabled="disabled" data-privilege-index="import" style="vertical-align: middle; margin: 0px; position: relative;">
              <label for="priv-02" style="vertical-align: middle; padding-left: 4px; margin-right: 17px;">Unggah & Impor</label>
            </div>
            <div class="checkbox">
							<input id="priv-03" type="checkbox" name="priv_opt_report" disabled="disabled" data-privilege-index="report" style="vertical-align: middle; margin: 0px; position: relative;">
              <label for="priv-03" style="vertical-align: middle; padding-left: 4px; margin-right: 17px;">Laporan</label>
            </div>
            <div class="checkbox">
							<input id="priv-04" type="checkbox" name="priv_opt_config" disabled="disabled" data-privilege-index="config" style="vertical-align: middle; margin: 0px; position: relative;">
              <label for="priv-04" style="vertical-align: middle; padding-left: 4px; margin-right: 17px;">Konfigurasi</label>
            </div>
					</div><!-- /.box-body -->
				</div><!-- ./col -->
				<div class="col-md-3 col-sm-6 col-xs-12 privileges-container" style="opacity: 0.45;">
					<div class="box-header with-border">
						<h3 class="box-title">Unggah & Impor</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
            <div class="checkbox">
							<input id="priv-05" type="checkbox" name="priv_opt_sikda" disabled="disabled" data-privilege-parent="import" data-privilege-index="import_sikda" style="vertical-align: middle; margin: 0px; position: relative;">
              <label for="priv-05" style="vertical-align: middle; padding-left: 4px; margin-right: 17px;">Data Sikda</label>
            </div>
            <div class="checkbox">
							<input id="priv-06" type="checkbox" name="priv_opt_excel" disabled="disabled" data-privilege-parent="import" data-privilege-index="import_excel" style="vertical-align: middle; margin: 0px; position: relative;">
              <label for="priv-06" style="vertical-align: middle; padding-left: 4px; margin-right: 17px;">Berkas Excel</label>
            </div>
					</div><!-- /.box-body -->
				</div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.box-body -->
    <div class="box-footer">
      <button type="submit" class="btn btn-danger">Simpan Hak Akses</button>
    </div>
  </div><!-- /.box -->
  <div class="row"></div><!-- /.row -->
  <?php echo form_close(); ?>
</section><!-- /.content -->
<script type="text/javascript">
(function(jq) {
  var privilegeData;
  var checkboxes = jq('input[type=checkbox]');
  var container = jq('.privileges-container');
  
  jq('select[name=priv_account_type]').on('change', function(ev) {
    if( jq(this)[0].value !== 'none' || jq(this)[0].value != '' ) {
      privilegeData = JSON.parse( $(this).find('option:selected')[0].dataset.privileges );
      
      checkboxes.prop('disabled', false);
      container.css('opacity', 1);
      
      jq.each(privilegeData, function(index, value) {
        if(value == 1)
          jq('input[type=checkbox][data-privilege-index='+index+']').prop('checked', true);
        else
          jq('input[type=checkbox][data-privilege-index='+index+']').prop('checked', false);
      });
      
      checkboxes.trigger('change');
    }
    else {
      checkboxes.prop('disabled', true);
      container.css('opacity', 0.45);
    }
  });
  
  jq('input[type=checkbox]').on('change', function() {
    if( jq(this).data('privilege-index') === 'import' ) {
      if( !jq(this).is(':checked') ) {
        jq('input[type=checkbox][data-privilege-parent=import]').prop({
          disabled: true,
          checked : false
        });
        container.eq(1).css('opacity', 0.45);
      }
      else {
        jq('input[type=checkbox][data-privilege-parent=import]').prop('disabled', false);
        container.eq(1).css('opacity', 1);
      }
    }
    else if( jq(this).data('privilege-parent') === 'import' ) {
      if( jq('input[type=checkbox][data-privilege-parent=import]:checked').length == 0 ) {
        jq('input[type=checkbox][data-privilege-index=import]').prop('checked', false);
        jq('input[type=checkbox][data-privilege-parent=import]').prop('disabled', true);
        container.eq(1).css('opacity', 0.45);
      }
    }
  });
})(jQuery);
</script>