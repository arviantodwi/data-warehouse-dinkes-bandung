<section class="content">
  <?php if( isset($_SESSION['postErrorNoData']) && $_SESSION['postErrorNoData'] === TRUE ): ?>
  <div id="notification" class="modal fade modal-warning">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="icon fa fa-ban"></i> UNGGAH GAGAL</h4>
        </div>
        <div class="modal-body">
          <h3 class="box-title">Status Unggah</h3>
          <p>Ada kesalahan pada pengisian formulir Detil Laporan. Silakan unggah kembali berkas Anda.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline" data-dismiss="modal">Ok</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <script>
    $(window).load(function(){
      $('#notification').modal('show');
    });
  </script>
  <?php elseif( isset($_SESSION['wsResponse']) ): ?>
  <?php
    if( strtolower($_SESSION['wsResponse']['status']) == 'berhasil' ) {
      $modalClass = 'modal-success';
      $iconClass = 'fa-check';
      $headingText = 'UNGGAH BERHASIL';
    }
    elseif( strtolower($_SESSION['wsResponse']['status']) == 'duplikasi' ) {
      $modalClass = 'modal-warning';
      $iconClass = 'fa-warning';
      $headingText = 'UNGGAH MEMBUTUHKAN KONFIRMASI';
    }
    else {
      $modalClass = 'modal-danger';
      $iconClass = 'fa-ban';
      $headingText = 'UNGGAH GAGAL';
    }
  ?>
  <div id="notification" class="modal fade <?php echo $modalClass; ?>">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="icon fa <?php echo $iconClass; ?>"></i> <?php echo $headingText; ?></h4>
        </div>
        <div class="modal-body">
          <h3 class="box-title">Status Unggah</h3>
          <p><?php echo $_SESSION['wsResponse']['message']; ?></p>
        </div>
        <div class="modal-footer">
          <?php if(isset($_SESSION['wsResponseAct']) && $_SESSION['wsResponseAct'] == TRUE): ?>
          <?php echo form_open('import/spreadsheet/do_reupload', array( 'class' => 'post-upload', 'data-credential' => 4 )); ?>
            <input type="hidden" name="puskesmas_name" value="<?php echo $_SESSION['oldUploadData']['puskesmas']; ?>" />
            <input type="hidden" name="report_type" value="<?php echo $_SESSION['oldUploadData']['report']; ?>" />
            <input type="hidden" name="report_raw_name" value="<?php echo $_SESSION['oldUploadData']['raw_type']; ?>" />
            <input type="hidden" name="month_num" value="<?php echo $_SESSION['oldUploadData']['month']; ?>" />
            <input type="hidden" name="years" value="<?php echo $_SESSION['oldUploadData']['year']; ?>" />
            <input type="hidden" name="response_filename" value="<?php echo $_SESSION['oldUploadData']['file_name']; ?>" />
            <input type="hidden" name="exist_filename" value="<?php echo $_SESSION['wsResponse']['details']['existedFile']; ?>" />
            <button type="submit" class="btn btn-outline">Ya</button>
            <button type="button" class="btn btn-outline" data-dismiss="modal">Tidak</button>
          <?php echo form_close(); ?>
          <?php else: ?>
          <button type="button" class="btn btn-outline" data-dismiss="modal">Tutup</button>
          <?php endif; ?>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <script>
    $(window).load(function(){
      $('#notification').modal('show');
    });
  </script>
  <?php endif; ?>
  <div class="row" style="position:relative;">
    <div class="col-md-4 col-sm-12 col-xs-12" style="position:absolute; top:0; right:0; height:100%; padding-left:0;  border-top-left-radius:0 !important; border-bottom-left-radius:0 !important; opacity:0.6;">
      <?php echo form_open('import/spreadsheet/post_upload', array( 'class' => 'post-upload', 'data-credential' => 4 )); ?>
        <div class="box box-primary" style="border-top-left-radius:0 !important; border-bottom-left-radius:0 !important;">
          <div class="box-header with-border">
            <h3 class="box-title">Detil Laporan</h3>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Nama Puskesmas</label>
                  <select class="form-control" disabled="disabled" name="<?php if( (int)$_SESSION['credential']['account_type'] !== 4 ) echo 'puskesmas_name'; else echo 'puskesmas_readonly'; ?>" style="width: 100%;">
                    <?php if( (int)$_SESSION['credential']['account_type'] !== 4 ): ?>
                    <option selected="selected" disabled="disabled" value="none">Pilih Puskesmas</option>
                    <?php endif; ?>
                    <?php
                      $hiddenValue = '';

                      foreach($model['puskesmas'] as $key=>$value) {
                        echo '<optgroup label="KECAMATAN '.$key.'">';

                        foreach($model['puskesmas'][$key] as $data) {
                          if((int)$_SESSION['credential']['account_type'] !== 4)
                            echo '<option value="' . $data . '" style="padding-left:17px;">' . $data . '</option>';
                          else {
                            $hiddenValue = $data;
                            echo '<option value="' . $data . '" selected="selected" style="padding-left:17px;">' . $data . '</option>';
                          }
                        }

                        echo '</optgroup>';
                      }
                    ?>
                  </select>
                </div><!-- /.form-group -->
              </div><!-- /.col -->
              <div class="col-md-12">
                <div class="form-group">
                  <label>Jenis Laporan</label>
                  <select class="form-control" disabled="disabled" name="report_type" style="width: 100%;">
                    <option selected="selected" disabled="disabled" value="none">Pilih Jenis Laporan</option>
                    <option value="LB1">Laporan Bulanan (LB1)</option>
                    <option value="KI1">Laporan Kesehatan Ibu</option>
                    <option value="KA1">Laporan Kesehatan Anak</option>
                    <option value="KI2">Laporan Kematian Ibu</option>
                    <option value="KA2">Laporan Kematian Anak</option>
                    <option value="KB">Laporan KB</option>
                    <option value="GIZI">Laporan Gizi</option>
                  </select>
                </div><!-- /.form-group -->
              </div><!-- /.col -->
              <div class="col-md-12">
                <div class="form-group">
                  <label>Bulan</label>
                  <select class="form-control" disabled="disabled" name="month_num" style="width: 100%;">
                    <option selected="selected" disabled="disabled" value="none">Pilih Bulan</option>
                    <?php foreach($model['periode']['bulan'] as $key=>$value): ?>
                    <option value="<?php echo $key+1; ?>"><?php echo $value; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div><!-- /.form-group -->
              </div><!-- /.col -->
              <div class="col-md-12">
                <div class="form-group">
                  <label>Tahun</label>
                  <select class="form-control" disabled="disabled" name="years" style="width: 100%;">
                    <option selected="selected" disabled="disabled" value="none">Pilih Tahun</option>
                    <?php foreach($model['periode']['tahun'] as $value): ?>
                    <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                    <?php if( $value == date('Y') ): ?>
                    <option value="<?php echo $value+1; ?>"><?php echo $value+1; ?></option>
                    <?php endif; ?>
                    <?php endforeach; ?>
                  </select>
                </div><!-- /.form-group -->
              </div><!-- /.col -->
              <div class="col-md-12">
                <div class="form-group has-feedback">
                  <input type="hidden" name="response_filename" value="" />
                  <input type="hidden" name="report_raw_name" value="" />
                  <?php if( (int)$_SESSION['credential']['account_type'] === 4 ): ?>
                  <input type="hidden" name="puskesmas_name" value="<?php echo $hiddenValue; ?>" />
                  <?php endif; ?>
                  <?php
                    $attr = array('class' => 'btn btn-success btn-block', 'disabled' => 'disabled', 'type' => 'submit', 'content' => 'Unggah Berkas');
                    echo form_button($attr);
                  ?>
                </div>
              </div>
            </div><!-- /.row -->
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      <?php echo form_close() ?>
    </div><!-- /.col-md-4 -->
    <div class="col-md-8 col-sm-12 col-xs-12" style="padding-right:0;">
      <form id="upload-tray" class="dropzone upload-drop" method="post" enctype="multipart/form-data" action="/api/json/xls_upload" style="border-right:none !important; border-top-right-radius:0 !important; border-bottom-right-radius:0 !important; height:411px; position:relative;">
        <div class="dz-message" style="margin-top:33px;">
				  <i class="fa fa-cloud-upload fa-5x"></i>
				  <h1>Tarik dan taruh berkas di sini<br/>untuk mulai mengimpor data.</h1><br/>
				  <h4><b>atau</b></h4><br/>
				  <button id="clickable" class="btn btn-info" type="button">Telusuri Berkas Unggahan</button>
			  </div>
			</form>
    </div><!-- /.col-md-8 -->
  </div><!-- /.row -->
</section><!-- /.content -->

<script type="text/javascript">
(function(jq) {
  var credential = parseInt( $('form').data('credential') );

  Dropzone.options.uploadTray = {
    paramName: "xls_file",
    maxFiles: 1,
    maxFilesize: 10, // MB, 1000 bytes based
    uploadMultiple: false,
    acceptedFiles: ".xls,.xlsx",
    init: function() {
      this.on('error', function(file, error, xhr) {
        this.removeFile(file);
      });

      this.on('success', function(file, response) {
        if(credential !== 4) {
          jq('.post-upload').parent().css({'opacity': 1}).end().find('select, button').prop('disabled', false);
        }
        else {
          jq('.post-upload').parent().css({'opacity': 1}).end().find('select[name!=puskesmas_readonly], button').prop('disabled', false);
        }
        jq('input[type=hidden][name=response_filename]')[0].value = response;
        this.disable();
      });
    },
    accept: function(file, done) {
      done();
    }
  };

  jq('select[name=report_type]').on('change', function() {
    $('input[name="report_raw_name"]').val( $(this).find('option:selected').text() );
  });
})(jQuery);
</script>
