<section class="content">
  <div class="row">
    <div class="col-md-4">
      <div class="box box-default">
        <!-- form start -->
        <form class="form-horizontal">
          <div class="box-body">
            <div class="form-group">
              <label class="col-md-12">Nama Host atau Alamat IP</label>
              <div class="col-md-12">
                <input type="text" class="form-control" placeholder="Nama Host / IP">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-12">Nama Akun DB</label>
              <div class="col-md-12">
                <input type="text" class="form-control" placeholder="Nama Host / IP">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-12">Kata Sandi DB</label>
              <div class="col-md-12">
                <input type="password" class="form-control" placeholder="Kata Sandi">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-12">Port</label>
              <div class="col-md-12">
                <input type="text" class="form-control" placeholder="3306">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-12">Database</label>
              <div class="col-md-12">
                <select class="form-control" style="width: 100%;">
                  <option selected="selected">SIKDA_DB_1</option>
                  <option>SIKDA_DB_2</option>
                  <option>SIKDA_DB_3</option>
                </select>
              </div>
            </div>
            <div class="form-group" style="margin-bottom:0; margin-top:13px;">
              <div class="col-lg-4 col-md-12 col-sm-12">
                <button type="submit" class="btn btn-default col-lg-12 col-md-12 col-sm-12">Uji Koneksi</button>
              </div>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <!--<button type="submit" class="btn btn-default">Uji Koneksi</button>&nbsp;&nbsp;&nbsp;<i class="fa fa-spinner fa-lg fa-spin"></i><br/><br/>-->
            <!--<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <h4>	<i class="icon fa fa-check"></i> Koneksi sukses</h4>
              Status server, terhubung
            </div>-->
            <div class="col-md-12" style="padding: 0;">
              <button type="submit" class="btn btn-info col-md-12">Mulai Proses Impor SIKDA</button>
            </div>
          </div><!-- /.box-footer -->
        </form>
      </div><!-- /.box -->
    </div><!-- /.col -->

    <div class="col-md-8">
      <div class="box">
        <div class="box-body no-padding">
          <table class="table table-bordered">
            <tr>
              <th>No.#</th>
              <th>Database</th>
              <th>Akun Pelaku</th>
              <th>Waktu Akses</th>
            </tr>
            <tr>
              <td>1.</td>
              <td>SIKDA_DB_2</td>
              <td>warehouse_admin</td>
              <td>Senin, 19 Oktober 2015</td>
            </tr>
            <tr>
              <td>2.</td>
              <td>SIKDA_DB_1</td>
              <td>operator1</td>
              <td>Jumat, 16 Oktober 2015</td>
            </tr>
            <tr>
              <td>3.</td>
              <td>SIKDA_DB_1</td>
              <td>operator1</td>
              <td>Kamis, 15 Oktober 2015</td>
            </tr>
            <tr>
              <td>4.</td>
              <td>SIKDA_DB_2</td>
              <td>warehouse_admin</td>
              <td>Jumat, 9 Oktober 2015</td>
            </tr>
            <tr>
              <td>5.</td>
              <td>SIKDA_DB_1</td>
              <td>operator2</td>
              <td>Senin, 5 Oktober 2015</td>
            </tr>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>