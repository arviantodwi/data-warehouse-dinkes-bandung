<section class="content">
  <!-- Main row -->
  <div class="row">
    <!-- center col -->
    <!-- <div class="col-xs-2 pull-right">
      <button class="btn btn-block btn-default cetak"><i class="fa fa-print"></i> Cetak</button><br/>
    </div> -->
    <section class="col-xs-12">
      <!-- Custom tabs (Charts with tabs)-->
      <div class="nav-tabs-custom">
        <!-- Tabs within a box -->
        <ul class="nav nav-tabs pull-right">
          <li class="pull-left header"><i class="fa fa-pie-chart"></i> Tingkat Pertumbuhan Gizi</li>
          <li class="active"><a href="#1m-chart" data-toggle="tab">1 Bulan Terakhir</a></li>
          <!--<li><a href="#6m-chart" data-toggle="tab">6 Bulan Trakhir</a></li>-->
          <!--<li><a href="#1y-chart" data-toggle="tab">1 Tahun Terakhir</a></li>-->
        </ul>
        <div class="tab-content no-padding">
          <!-- Morris chart -->
          <div class="chart tab-pane active" id="1m-chart" style="position: relative; height: 350px;"></div>
          <div class="chart tab-pane" id="6m-chart" style="position: relative; height: 350px;"></div>
          <div class="chart tab-pane" id="1y-chart" style="position: relative; height: 350px;"></div>
        </div>
      </div><!-- /.nav-tabs-custom -->
    </section><!-- /.center col -->
  </div><!-- /.row (main row) -->
</section>
