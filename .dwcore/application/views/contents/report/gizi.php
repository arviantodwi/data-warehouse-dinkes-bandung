<section class="content">
  <?php if( is_null( $this->uri->segment(3) ) ): ?>
  <div class="row">
    <?php echo form_open('report/gizi/filter'); ?>
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Filter Laporan Gizi</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="col-md-12">
            <div class="form-group col-md-6" style="padding-left: 0;">
              <label>Kota</label>
              <input class="form-control" type="text" value="Bandung" disabled="disabled" style="width: 100%;" />
            </div><!-- /.form-group -->
          </div>
        </div>

        <div class="box-header with-border">
          <h3 class="box-title">Periode</h3>
        </div><!-- /.box-header -->
        <div id="periode-chain" class="box-body">
          <div class="col-md-12">
            <div class="form-group col-md-6" style="padding-left: 0;">
              <label>Rentang Awal - Tahun</label>
              <select class="form-control" name="f_period_start[]" data-is-chain="true" data-chain-control="tahun-awal" style="width: 100%;">
                <option selected="selected" disabled="disabled" value="none">Pilih Tahun</option>
                <?php foreach($model['years'] as $value): ?>
                <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                <?php endforeach; ?>
              </select>
            </div><!-- /.form-group -->
            <div class="form-group col-md-6" style="position:relative; padding-right: 0;">
              <label>Rentang Awal - Bulan</label>
              <div class="preloader" style="width: 100%; height: 34px; background: white; line-height: 34px;">Pilih rentang awal Tahun terlebih dahulu.</div>
              <select class="form-control" name="f_period_start[]" data-is-chain="true" data-chain-control="bulan-awal" disabled="disabled" style="width: 100%; display:none;">
                <option selected="selected" disabled="disabled" value="none">Pilih Bulan</option>
                <option value="all">Tampilkan Semua Bulan</option>
              </select>
            </div><!-- /.form-group -->
          </div><!-- /.col -->
          <div class="col-md-12">
            <div class="form-group col-md-6" style="position: relative; padding-left: 0;">
              <label>Rentang Akhir - Tahun</label>
              <div class="preloader" style="width: 100%; height: 34px; background: white; line-height: 34px;">Pilih rentang awal terlebih dahulu.</div>
              <select class="form-control" name="f_period_end[]" data-is-chain="true" data-chain-control="tahun-akhir" style="width: 100%; display:none;">
                <option selected="selected" disabled="disabled" value="none">Pilih Tahun</option>
              </select>
            </div><!-- /.form-group -->
            <div class="form-group col-md-6" style="position:relative; padding-right: 0;">
              <label>Rentang Akhir - Bulan</label>
              <div class="preloader" style="width: 100%; height: 34px; background: white; line-height: 34px;">Pilih rentang awal terlebih dahulu.</div>
              <select class="form-control" name="f_period_end[]" data-is-chain="true" data-chain-control="bulan-akhir" disabled="disabled" style="width: 100%; display:none;">
                <option selected="selected" disabled="disabled" value="none">Pilih Bulan</option>
              </select>
            </div><!-- /.form-group -->
          </div><!-- /.col -->
        </div><!-- /.box-body -->

        <div class="box-header with-border">
          <h3 class="box-title">Area</h3>
        </div><!-- /.box-header -->
        <div id="area-chain" class="box-body">
          <div class="col-md-12">
            <div class="form-group col-md-6" style="position:relative; padding-left: 0;">
              <label>Kecamatan</label>
              <select class="form-control" name="f_kecamatan" data-is-chain="true" data-chain-control="kecamatan" style="width: 100%;">
                <option selected="selected" disabled="disabled" value="none">Pilih Kecamatan</option>
                <option value="all">Tampilkan Semua Kecamatan</option>
                <?php foreach( $model['kecamatan'] as $value): ?>
                <option value="<?php echo $value['kode_kecamatan']; ?>"><?php echo $value['nama_kecamatan']; ?></option>
                <?php endforeach; ?>
              </select>
            </div><!-- /.form-group -->
            <div class="form-group col-md-6" style="position:relative; padding-right: 0;">
              <label>Puskesmas</label>
              <div class="preloader" style="width: 100%; height: 34px; background: white; line-height: 34px;">Pilih Kecamatan terlebih dahulu.</div>
              <select class="form-control" name="f_puskesmas" data-is-chain="true" data-chain-control="puskesmas" disabled="disabled" style="width: 100%; display:none;">
                <option selected="selected" disabled="disabled" value="none">Pilih Puskesmas</option>
                <option value="all">Tampilkan Semua Puskesmas</option>
              </select>
            </div><!-- /.form-group -->
          </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
          <?php
            $attr = array('class' => 'btn btn-block btn-info', 'type' => 'submit', 'style' => 'float:left; width:72%;', 'content' => 'Tampilkan Laporan');
            echo form_button($attr);
          ?>
          <a href="#" style="float:right; width:27%; line-height:34px; text-align:center;">Ubah ke awal</a>
        </div>
      </div><!-- /.box -->
    </div><!-- /.col -->
    <?php echo form_close(); ?>
  </div> <!-- /.row -->
  <script type="text/javascript">
    (function(jq) {
      var Dinkes = function(element, options) {
        var obj  = this;
        var conf = jq.extend({}, options || {});
        var elem = jq(element);

        this.constructor = function() {

        }

        this.constructor();
      }

      var FormChain = function(element, options) {
        var defaults = {
          /** NAMESPACES **/
          controlClass      : '.form-control',
          controlGroupClass : '.form-group',
          dynamicDataClass  : '.dyndata',
          /** PARAMETERS **/
          chainControlsName : [],
          preloaderText     : {
            starter    : [],
            onAjaxSend : 'Memuat...',
            onSkip     : 'Masukan data ini akan diabaikan'
          },
          /** CALLBACKS **/
          onValueChange     : null,
          onInit            : null,
          onAjaxSuccess     : null,
          onAjaxFailed      : null
        };
        var proto    = jq.extend(true, defaults, options);
        var obj      = this;
        var root     = jq(element);
        var inputs   = [];
        var scope    = [];
        var code     = [];

        obj.makeInputAlias = null;

        var _constructor = function() {
          var controls = root.find(proto.controlClass);
          var counter = 0;

          // index, control
          jq.each(controls, function(i, c) {
            var chain = jq(c).data('is-chain');

            if(typeof chain !== 'undefined' && chain) {
              inputs[counter] = c;
              scope[counter] = c.dataset.chainControl;
              c.dataset.chainOrder = counter;
              counter++;
            }
            else { return true };
          });
        }

        // order
        var _do_onValueChange = function(o) {
          if( typeof proto.onValueChange !== 'function' ) { return; }
          proto.onValueChange.call(this, obj, scope[o]);
        }

        // order, value, event
        var _get_current_value = function(o, v, e) {
          _do_onValueChange(o);

          if(typeof v === 'undefined') {
            console.log('Value was undefined!');
          }
          else if( v.toLowerCase() === 'none' || v === '' || v === null ) {
            console.log('Value is not permitted for POST data');
          }
          else if( o !== inputs.length - 1 ) {
            if(obj.makeInputAlias !== null && obj.makeInputAlias !== '') {
              code[o] = root.find(proto.controlClass + '[data-chain-control=' + obj.makeInputAlias + ']')[0].value;
              obj.makeInputAlias = null;
            }
            else { code[o] = v; }

            if(v == 'all') { _reset_further_input(o); }
            else { _fetch_options_data(o); }
          }
        }

        // order, refetch
        var _reset_further_input = function(o, r) {
          r = typeof r != 'undefined' ? r : false;

          if(r == true || r == 1) {
            var nextInput = jq(inputs[o+1]);

            nextInput.prev('.preloader').html(proto.preloaderText.onAjaxSend);

            if( nextInput.is(':visible') ) {
              nextInput.hide().prev('.preloader').show();
            }

            o += 1;
          }

          var nextAllInputs = [];
          var textString = (r == true || r == 1) ? proto.preloaderText.starter[o] : proto.preloaderText.onSkip;

          for(var next=o+1; next<inputs.length; next++) {
            nextAllInputs.push(inputs[next]);
            inputs[next].selectedIndex = 0;

            if( jq(inputs[next]).prev('.preloader').is(':visible') ) {
              jq(inputs[next]).prev('.preloader').html(textString);
            }
            else {
              jq(inputs[next]).prev('.preloader').html(textString).show();
            }
          }

          jq(nextAllInputs).prop('disabled', true).hide();
        }

        // order
        var _fetch_options_data = function(o) {
          var previousScope = scope[o];
          // var responseCondition = false;
          var nextInput = jq(inputs[o+1]);

          jq.ajax({
            method   : 'POST',
            dataType : 'json',
            data     : { prev_scope: previousScope, code: code[o] },
            timeout  : 1000,
            url      : '/api/json/get_chain_options',
            //
            beforeSend: function() {
              _reset_further_input(o, true);
            },
            // response
            success: function(r) {
              var optionMarkup = '';
              var fetchedData = [];

              if( nextInput.children(proto.dynamicDataClass).length > 0 ) {
                nextInput.children(proto.dynamicDataClass).remove();
              }

              nextInput[0].selectedIndex = 0;

              if( nextInput.prop('disabled', true) ) {
                nextInput.prop('disabled', false);
              }

              for(var i=0; i<r.length; i++) {
                fetchedData[i] = [];

                // key, value
                jq.each(r[i], function(k, v) { fetchedData[i].push(v); });

                optionMarkup += '<option class="'+proto.dynamicDataClass.substr(1)+'" value="'+fetchedData[i][0]+'">'+fetchedData[i][1]+'</option>';
              }

              nextInput.append(optionMarkup);
            },
            // response, status, message
            error: function(r, s, m) {
              console.log(s);
              console.log(m);
            },
            // response
            complete: function(r) {
              // console.log(previousScope);
              var delay = 1000;
              setTimeout(function() {
                jq(inputs[o+1]).show().prev('.preloader').hide();
              }, delay);
            }
          });
        }

        // Call the constructor to begin the
        // initialization of plugin
        _constructor();

        // event
        jq(inputs).on('change', function(e) {
          var order = parseInt( this.dataset.chainOrder );
          var value = this.selectedOptions[0].value;

          _get_current_value(order, value, e);
        });

        // event
        jq(inputs).on('keyup', function(e) {
          if(e.which == 38 || e.which == 40) {
            jq(this).trigger('change');
            return false;
          }
        });
      }

      jq.fn.extend({
        dinkes: function(options) {
          return this.each(function() {
            if( jq(this).data('dw-extension') ) { return; }

            var dinkes = new Dinkes(this, options);
            jq(this).data('dw-extension', dinkes);
          });
        },
        formchain: function(options) {
          if( !jq(this).data('dw-extension') ) { return; }

          return this.each(function() {
            if( jq(this).data('ext-fc') ) { return; }

            var formchain = new FormChain(this, options);
            jq(this).data('ext-fc', formchain);
          });
        }
      });

      jq('#periode-chain').dinkes().formchain({
        preloaderText: {
          starter: [
            'Pilih rentang awal Tahun terlebih dahulu.',
            'Pilih rentang awal terlebih dahulu.',
            'Pilih rentang awal terlebih dahulu.'
          ]
        },
        onValueChange: function(MyChain, currentInput) {
          if (currentInput === 'bulan-awal') {
            MyChain.makeInputAlias = 'tahun-awal';
          }
        }
      });

      jq('#area-chain').dinkes().formchain();
    })(jQuery);
  </script>
  <?php elseif( $this->uri->segment(3) == 'filter' ): ?>
  <div class="row">
    <!--<section class="col-xs-12 pull-right">
      <div class="btn-group pull-left">
        <h3>Laporan Gizi</h3>
      </div>
      <div class="btn-group pull-right">
        <button type="button" class="btn btn-default cetak" id="btnExport"><i class="fa fa-file-excel-o"></i> Simpan Sebagai File Excel</button>
        <button type="button" class="btn btn-default cetak" onclick="window.open('cetak-bulanan.html')"><i class="fa fa-print"></i>  Cetak / PDF</button>
      </div>
    </section>-->
    <section class="col-xs-12 pull-right">
      <div class="btn-group pull-left">
        <h3>Laporan Gizi</h3>
      </div>
    </section>

    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-body no-padding">
          <div class="box-body col-md-6">
            <div class="box-header with-border" style="margin-bottom: 12px;">
              <h3 class="box-title">Data Laporan</h3>
            </div><!-- /.box-header -->
            <dl class="dl-horizontal">
              <dt>Periode Laporan</dt>
              <dd><?php echo $model['report_spec']['period_start']; ?> s.d. <?php echo $model['report_spec']['period_end']; ?></dd>
            </dl>
            <dl class="dl-horizontal">
              <dt>Kecamatan</dt>
              <dd><?php echo $model['report_spec']['kecamatan_name']; ?></dd>
            </dl>
            <dl class="dl-horizontal">
              <dt>Puskesmas</dt>
              <dd><?php echo $model['report_spec']['puskesmas_name']; ?></dd>
            </dl>
            <dl class="dl-horizontal">
              <dt>UPT Puskesmas</dt>
              <dd><?php echo $model['report_spec']['upt_name']; ?></dd>
            </dl>
          </div><!-- /.box-body -->
          <div class="box-body col-md-6">
            <div class="box-header with-border" style="margin-bottom: 12px;">
              <h3 class="box-title">Pengaturan Laporan</h3>
            </div><!-- /.box-header -->
            <div class="form-group" style="padding: 0 15px;">
              <label>Nama Kegiatan</label>
              <select class="form-control" style="width: 100%;">
                <option value="" selected="selected">Tampilkan Semua</option>
                <option value="">Bayi</option>
                <option value="">Balita</option>
                <option value="">Ibu Hamil</option>
                <option value="">Ibu Nifas</option>
                <option value="">Gizi Buruk</option>
                <option value="">Masalah Gizi Lain</option>
              </select>
            </div><!-- /.form-group -->

            <div class="form-group" style="padding: 0 15px;">
              <label>Rincian Kegiatan</label>
              <select class="form-control" style="width: 100%;">
                <option selected="selected">Tampilkan Semua</option>
                <optgroup label="Bayi">
                  <option value="">Tampilkan Semua Kegiatan Bayi</option>
                  <option value="">Jumlah Bayi (0-6 bl) atau (0-5 bl / 5 bl 29 hr) Seluruhnya(S)</option>
                  <option value="">Bayi (0-6 bl) atau (0-5 bl / 5 bl 29 hr) dengan ASI EkskLusif</option>
                  <option value="">Bayi (6 bl) Seluruhnya (S)</option>
                  <option value="">Bayi (6 bl) yang Mendapatkan ASI Saja &rarr; LULUS ASI EKSKLUSIF</option>
                  <option value="">Jumlah Bayi Kasus Gizi Kurang (KURUS: -3 BB/PB sampai &lt; -2 BB/PB) Umur 0-6 bl Pada Bulan Ini</option>
                  <option value="">Bayi (0-11 bl) Seluruhnya (S)</option>
                  <option value="">Bayi (0-11 bl) dengan KMS (K)</option>
                  <option value="">Bayi (0-11 bl) yang Ditimbang (D)</option>
                  <option value="">Bayi (0-11 bl) Naik Berat Badan (N)</option>
                  <option value="">Bayi (0-11 bl) Tidak Naik Berat Badan (T)</option>
                  <option value="">Bayi (0-11 bl) 2T</option>
                  <option value="">Bayi (0-11 bl) yang Tidak Ditimbang Bulan Lalu (O)</option>
                  <option value="">Bayi (0-11 bl) Baru Pertama Lali Ditimbang (B)</option>
                  <option value="">Bayi (0-11 bl) B G M</option>
                  <option value="">Bayi (0-11 bl) B G M Baru</option>
                  <option value="">Bayi (6-11 bl) yang Mendapatkan Vitamin A Dosis Tinggi</option>
                  <option value="">Bayi (6-11 bl) yang Mendapat MP-ASI / PMT-P</option>
                  <option value="">Jumlah Bayi Umur 6-11 bl Kasus Gizi Buruk (&lt; -3 BB/U)</option>
                  <option value="">Jumlah Bayi Umur 6-11 bl Kasus Gizi Kurang (KURUS: -3 BB/PB sampai &lt; -2 BB/PB) Pada Bulan Ini</option>
                  <option value="">Jumlah Bayi Umur 6-11 bl Kasus Gizi Kurang (KURUS: -3 BB/PB sampai &lt; -2 BB/PB) Mendapat PMT-P Pada Bulan Ini</option>
                  <option value="">Jumlah Anak Balita 6-23 bl yang Mendapat Taburia</option>
                </optgroup>
                <optgroup label="Balita">
                  <option value="">Tampilkan Semua Kegiatan Balita</option>
                  <option value="">Jumlah Anak Umur 12-23 Seluruhnya (S)</option>
                  <option value="">Jumlah Anak Umur 12-23 bl dengan KMS (K)</option>
                  <option value="">Jumlah Anak Umur 12-23 bl yang Ditimbang (D)</option>
                  <option value="">Jumlah Anak Umur 12-23 bl yang Naik Berat Badan (N)</option>
                  <option value="">Jumlah Anak Umur 12-23 bl Tidak Naik berat badan (T)</option>
                  <option value="">Jumlah Anak Umur 12-23 bl 2T</option>
                  <option value="">Jumlah Anak Umur 12-23 bl yang Tidak Ditimbang Bulan Lalu (O)</option>
                  <option value="">Jumlah Anak Umur 12-23 bl Baru Pertama Kali Ditimbang (B)</option>
                  <option value="">Jumlah Anak Umur 12-23 bl yang BGM atau Kasus Gizi Buruk (&lt; -3 BB/U) &rarr; Kasus Baru & Lama</option>
                  <option value="">Jumlah Anak Umur 12-23 bl dengan Status BGM Baru atau Kasus Gizi Buruk (&lt; -3 BB/U) &rarr; Kasus Baru</option>
                  <option value="">Jumlah Anak Umur 12-23 bl kasus Gizi Kurang (KURUS: -3 BB/PB sampai &lt; -2 BB/PB) pada Bulan Ini</option>
                  <option value="">Jumlah Anak Umur 12-23 bl Kasus Gizi Kurang (KURUS: -3 BB/PB sampai &lt; -2 BB/PB) Mendapat PMT-P pada Bulan Ini</option>
                  <option value="">Jumlah Anak Umur 24-59 Seluruhnya (S)</option>
                  <option value="">Jumlah Anak Umur 24-59 bl dengan KMS (K)</option>
                  <option value="">Jumlah Anak Umur 24-59 bl yang Ditimbang (D)</option>
                  <option value="">Jumlah Anak Umur 24-59 bl yang Naik Berat Badan (N)</option>
                  <option value="">Jumlah Anak Umur 24-59 bl Tidak Naik Berat Badan (T)</option>
                  <option value="">Jumlah Anak Umur 24-59 bl 2T</option>
                  <option value="">Jumlah Anak Umur 24-59 bl yang Tidak Ditimbang Bulan Lalu (O)</option>
                  <option value="">Jumlah Anak Umur 24-59 bl Baru Pertama Kali Ditimbang (B)</option>
                  <option value="">Jumlah Anak Umur 24-59 bl Kasus Gizi Kurang (KURUS: -3 BB/TB sampai &lt; -2 BB/TB) pada Bulan Ini</option>
                  <option value="">Jumlah Anak Umur 24-59 bl Kasus Gizi Kurang (KURUS: -3 BB/TB sampai &lt; -2 BB/TB) Mendapat PMT pada Bulan Ini</option>
                  <option value="">Anak Umur 24-59 bl yang BGM atau Kasus Gizi Buruk (&lt; -3 BB/U) &rarr; Kasus Baru &amp; Lama</option>
                  <option value="">Anak Umur 24-59 bl dengan Status BGM Baru atau Kasus Gizi Buruk (&lt; -3 BB/U) &rarr; Kasus Baru</option>
                  <option value="">Anak Umur 12-59 bl yang Mendapatkan Kapsul Vitamin A Dosis Tinggi </option>
                  <option value="">Balita (12-23 bl) Gakin yang Mendapat MP-ASI / PMT-P</option>
                  <option value="">Balita (24-59 bl) Gakin yang Mendapat MP-ASI / PMT-P</option>
                </optgroup>
                <optgroup label="Ibu Hamil">
                  <option value="">Tampilkan Semua Kegiatan Ibu Hamil</option>
                  <option value="">Ibu Hamil yang Mendapatkan Tablet Tambah Darah (Fe)</option>
                  <option value="">Pertama Kali (Fe1)</option>
                  <option value="">Ketiga Kali (Fe3)</option>
                  <option value="">Fe1 Luar Wilayah</option>
                  <option value="">Fe3 Luar Wilayah</option>
                  <option value="">Lila &lt; 23,5cm atau Penambahan BB &lt; 9kg Selama Kehamilan</option>
                  <option value="">Ibu Hamil Anemia dengan HB &lt; 11gr %</option>
                </optgroup>
                <optgroup label="Ibu Nifas">
                  <option value="">Tampilkan Semua Kegiatan Ibu Nifas</option>
                  <option value="">Ibu Nifas yang Mendapatkan 2 Kapsul Vitamin A Dosis Tinggi</option>
                  <option value="">Ibu Nifas yang Mendapatkan Tablet Fe</option>
                  <option value="">Jumlah Ibu Hamil Risiko Kurang Energi Kronis (KEK) dengan Lingkar Lengan Atas (LiLA) &lt; 23,5cm yang Mendapat PMT pada Bulan Ini</option>
                </optgroup>
                <optgroup label="Gizi Buruk">
                  <option value="">Tampilkan Semua Kegiatan Gizi Buruk</option>
                  <option value="">Bayi (0-11 bl) yang Bergizi Buruk &rarr; Kasus Baru</option>
                  <option value="">Bayi (0-11 bl) yang Bergizi Buruk &rarr; Kasus Baru &amp; Lama</option>
                  <option value="">Anak Umur 12-23 bl yang Bergizi Buruk &rarr; Kasus Baru</option>
                  <option value="">Anak Umur 12-23 bl yang Bergizi Buruk &rarr; Kasus Baru &amp; Lama</option>
                  <option value="">Anak Umur 24-59 bl yang Bergizi Buruk &rarr; Kasus Baru</option>
                  <option value="">Anak Umur 24-59 bl yang Bergizi Buruk &rarr; Kasus Baru &amp; Lama</option>
                  <option value="">Kasus Gizi Buruk Baru pada Bulan Ini</option>
                  <!--a) Tanpa gejala klinis-->
                  <!--b) Marasmus-->
                  <!--c) kwashiorkor-->
                  <!--d) Marasmic-kwashiorkor-->
                  <!--e) Dan lain-lain ……………………………………………………………..-->
                  <option value="">Kasus Gizi Buruk Baru yang Masih dirawat Pada Bulan Ini</option>
                  <option value="">Kasus Gizi Buruk Lama yang Masih dirawat Sampai Pada Bulan Ini</option>
                  <option value="">Kasus Gizi Buruk Baru yang Meninggal Pada Bulan Ini</option>
                  <option value="">Kasus Gizi Buruk Lama yang Meninggal Pada Bulan Ini</option>
                </optgroup>
                <optgroup label="Masalah Gizi Lain">
                  <option value="">Tampilkan Semua Kegiatan Masalah Gizi Lain</option>
                  <option value="">Desa / Kelurahan Disurvei Garam Beryodium</option>
                  <option value="">Desa / Kelurahan dengan Garam Beryodium Baik</option>
                  <option value="">Desa / Kelurahan Endemis GAKI</option>
                  <option value="">Penduduk yang Menderita GAKI</option>
                  <option value="">WUS yang Diberi Kapsul Iodium</option>
                </optgroup>
              </select>
            </div><!-- /.form-group -->

            <div class="form-group" style="padding: 0 15px;">
              <label>Jenis Kelamin</label>
              <div class="radio">
                <label class="radio-inline" for="optionsRadios1">
                  <input type="radio" name="f_gender" id="optionsRadios1" checked="checked" value="all"> Tampilkan Semua
                </label>
                <label class="radio-inline" for="optionsRadios2">
                  <input type="radio" name="f_gender" id="optionsRadios2" value="P"> Pria Saja
                </label>
                <label class="radio-inline" for="optionsRadios3">
                  <input type="radio" name="f_gender" id="optionsRadios3" value="W"> Wanita Saja
                </label>
              </div>
            </div><!-- /.form-group -->

            <div class="form-group" style="padding: 0 15px;">
              <label>Unduh Laporan</label>
              <?php
              echo form_open('/report/gizi/export', ['target'=>'_self']);
              echo form_hidden('xls_diagnosis_json', $model['diagnosis_data']);
              echo form_hidden('xls_specification_json', json_encode($model['report_spec']));
              echo form_submit('xls_submit', 'Unduh Laporan Sebagai Berkas Excel (.XLS)', ['class'=>'btn btn-primary']);
              echo form_close();
              ?>
            </div><!-- /.form-group -->
          </div><!-- /.box-body -->
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->

    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-body no-padding table-scroll">
          <table class="table table-bordered table-lp" id="tblExport">
            <style>
              .middle-aligned { vertical-align:middle !important; }
              .top-aligned { vertical-align:top; }
              tr:nth-child(2n) { background-color: #fafafa; }
            </style>
            <?php echo $markup['gizi_table']; ?>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->



    <div class="col-md-12">
              <div class="box box-solid">
                <div class="box-body no-padding table-scroll">
					<!--<table class="table table-bordered table-lp" id="tblExport">
					  <tr class="table_head text-center">
						<th rowspan="2" class="text-center">NO</th>
						<th rowspan="2" class="text-center">KEGIATAN</th>
						<th colspan="2" class="text-center">CINAMBO</th>
					  </tr>
					  <tr class="table_head text-center">
						<td>Laki-Laki</td>
						<td>Perempuan</td>
					  </tr>
					  <tr>
						<td></td>
						<td>Jumlah Posyandu yang ada</td>
						<td>55</td>
						<td>-</td>
					  </tr>
					  <tr>
						<td></td>
						<td>Jumlah Posyandu yang melapor pada Bulan Ini</td>
						<td>55</td>
						<td>-</td>
					  </tr>
					  <tr>
						<td colspan="4"  class="table_head text-center">BAYI</td>
					  </tr>
					  <tr>
						<td>1</td>
						<td>Jumlah Bayi (0-6 bulan) atau (0-5 bulan/ 5 bulan 29 hari) seluruhnya (S)</td>
						<td>175</td>
						<td>163</td>
					  </tr>
					  <tr>
						<td>2</td>
						<td>Bayi (0-6 bulan) atau (0-5 bulanl / 5 bulan 29 hari) dengan ASI EkskLusif</td>
						<td>76</td>
						<td>73</td>
					  </tr>
					  <tr>
						<td>3</td>
						<td>Bayi (6 bulan) seluruhnya (S)</td>
						<td>55</td>
						<td>53</td>
					  </tr>
					  <tr>
						<td>4</td>
						<td>Bayi (6 bulan) yang mendapatkan ASI saja --&gt; LULUS ASI EKSKLUSIF</td>
						<td>35</td>
						<td>37</td>
					  </tr>
					  <tr>
						<td>5</td>
						<td>Jumlah bayi kasus gizi kurang (KURUS: -3 BB/PB sampai &lt; -2BB/PB) umur 0-6 bulan pada BULAN INI</td>
						<td>-</td>
						<td>1 </td>
					  </tr>
					  <tr>
						<td>6</td>
						<td>Bayi (0-11 bulan) Seluruhnya (S)</td>
						<td>402</td>
						<td>390</td>
					  </tr>
					  <tr>
						<td>7</td>
						<td>Bayi (0-11 bulan) dengan KMS (K)</td>
						<td>402</td>
						<td>390</td>
					  </tr>
					  <tr>
						<td>8</td>
						<td>Bayi (0-11 bulan) yang ditimbang (D)</td>
						<td>370</td>
						<td>361</td>
					  </tr>
					  <tr>
						<td>9</td>
						<td>Bayi (0-11 bulan) naik berat badan (N)</td>
						<td>212</td>
						<td>194</td>
					  </tr>
					  <tr>
						<td>10</td>
						<td>Bayi (0-11 bulan) tidak naik berat badan (T)</td>
						<td>51</td>
						<td>43</td>
					  </tr>
					  <tr>
						<td>11</td>
						<td>Bayi (0-11 bulan) 2T</td>
						<td>2</td>
						<td>1</td>
					  </tr>
					  <tr>
						<td>12</td>
						<td>Bayi (0-11 bulan) yang tidak ditimbang bulan lalu (O)</td>
						<td>78</td>
						<td>82</td>
					  </tr>
					  <tr>
						<td>13</td>
						<td>Bayi (0-11 bulan) Baru pertama kali ditimbang (B)</td>
						<td>29</td>
						<td>42</td>
					  </tr>
					  <tr>
						<td>14</td>
						<td>Bayi (0-11 bulan) B G M</td>
						<td>-</td>
						<td>-</td>
					  </tr>
					  <tr>
						<td>15</td>
						<td>Bayi (0-11 bulan) B G M Baru</td>
						<td>-</td>
						<td>-</td>
					  </tr>
					  <tr>
						<td>16</td>
						<td>Bayi ( 6-11 blulan) yang mendapatkan Vitamin A dosis tinggi</td>
						<td>316</td>
						<td>317</td>
					  </tr>
					  <tr>
						<td>17</td>
						<td>Bayi (6-11 bulan) yang mendapat MP-ASI/ PMT-P</td>
						<td>-</td>
						<td>-</td>
					  </tr>
					  <tr>
						<td>18</td>
						<td>Jumlah bayi umur 6-11 bulan kasus gizi buruk (&lt; -3 BB/U)</td>
						<td>-</td>
						<td>-</td>
					  </tr>
					  <tr>
						<td>19</td>
						<td>Jumlah bayi, umur 6-11 bulan kasus gizi kurang (KURUS: -3 BB/PB sampai &lt; -2 BB/PB) pada BULAN INI</td>
						<td>-</td>
						<td>-</td>
					  </tr>
					  <tr>
						<td>20</td>
						<td>Jumlah bayi, umur 6-11 bulan kasus gizi kurang (KURUS: -3 BB/PB sampai &lt; -2 BB/PB) mendapat PMT-P pada BULAN INI</td>
						<td>-</td>
						<td>-</td>
					  </tr>
					  <tr>
						<td>21</td>
						<td>Jumlah anak Balita 6-23 bulan yang mendapat taburia</td>
						<td>-</td>
						<td>-</td>
					  </tr>
					</table>-->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
<?php endif; ?>
