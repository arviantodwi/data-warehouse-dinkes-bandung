<section class="content">
  <?php if( is_null( $this->uri->segment(3) ) ): ?>
  <div class="row">
    <?php echo form_open('report/bulanan/filter'); ?>
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Filter Laporan Bulanan (LB1)</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-12">
              <div class="form-group col-md-6" style="padding-left: 0;">
                <label>Kota</label>
                <input class="form-control" type="text" value="Bandung" disabled="disabled" style="width: 100%;" />
              </div><!-- /.form-group -->
            </div>
          </div>

          <div class="box-header with-border">
            <h3 class="box-title">Periode</h3>
          </div><!-- /.box-header -->
          <div id="periode-chain" class="box-body">
            <div class="col-md-12">
              <div class="form-group col-md-6" style="padding-left: 0;">
                <label>Rentang Awal - Tahun</label>
                <select class="form-control" name="f_period_start[]" data-is-chain="true" data-chain-control="tahun-awal" style="width: 100%;">
                  <option selected="selected" disabled="disabled" value="none">Pilih Tahun</option>
                  <?php foreach($model['years'] as $value): ?>
                  <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                  <?php endforeach; ?>
                </select>
              </div><!-- /.form-group -->
              <div class="form-group col-md-6" style="position:relative; padding-right: 0;">
                <label>Rentang Awal - Bulan</label>
                <div class="preloader" style="width: 100%; height: 34px; background: white; line-height: 34px;">Pilih rentang awal Tahun terlebih dahulu.</div>
                <select class="form-control" name="f_period_start[]" data-is-chain="true" data-chain-control="bulan-awal" disabled="disabled" style="width: 100%; display:none;">
                  <option selected="selected" disabled="disabled" value="none">Pilih Bulan</option>
                  <option value="all">Tampilkan Semua Bulan (Setahun Penuh)</option>
                </select>
              </div><!-- /.form-group -->
            </div><!-- /.col -->
            <div class="col-md-12">
              <div class="form-group col-md-6" style="position: relative; padding-left: 0;">
                <label>Rentang Akhir - Tahun</label>
                <div class="preloader" style="width: 100%; height: 34px; background: white; line-height: 34px;">Pilih rentang awal terlebih dahulu.</div>
                <select class="form-control" name="f_period_end[]" data-is-chain="true" data-chain-control="tahun-akhir" style="width: 100%; display:none;">
                  <option selected="selected" disabled="disabled" value="none">Pilih Tahun</option>
                </select>
              </div><!-- /.form-group -->
              <div class="form-group col-md-6" style="position:relative; padding-right: 0;">
                <label>Rentang Akhir - Bulan</label>
                <div class="preloader" style="width: 100%; height: 34px; background: white; line-height: 34px;">Pilih rentang awal terlebih dahulu.</div>
                <select class="form-control" name="f_period_end[]" data-is-chain="true" data-chain-control="bulan-akhir" disabled="disabled" style="width: 100%; display:none;">
                  <option selected="selected" disabled="disabled" value="none">Pilih Bulan</option>
                </select>
              </div><!-- /.form-group -->
            </div><!-- /.col -->
          </div><!-- /.box-body -->

          <div class="box-header with-border">
            <h3 class="box-title">Area</h3>
          </div><!-- /.box-header -->
          <div id="area-chain" class="box-body">
            <div class="col-md-12">
              <div class="form-group col-md-6" style="position:relative; padding-left: 0;">
                <label>Kecamatan</label>
                <select class="form-control" name="f_kecamatan" data-is-chain="true" data-chain-control="kecamatan" style="width: 100%;">
                  <option selected="selected" disabled="disabled" value="none">Pilih Kecamatan</option>
                  <option value="all">Tampilkan Semua Kecamatan</option>
                  <?php foreach( $model['kecamatan'] as $value): ?>
                  <option value="<?php echo $value['kode_kecamatan']; ?>"><?php echo $value['nama_kecamatan']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div><!-- /.form-group -->
              <div class="form-group col-md-6" style="position:relative; padding-right: 0;">
                <label>Puskesmas</label>
                <div class="preloader" style="width: 100%; height: 34px; background: white; line-height: 34px;">Pilih Kecamatan terlebih dahulu.</div>
                <select class="form-control" name="f_puskesmas" data-is-chain="true" data-chain-control="puskesmas" disabled="disabled" style="width: 100%; display:none;">
                  <option selected="selected" disabled="disabled" value="none">Pilih Puskesmas</option>
                  <option value="all">Tampilkan Semua Puskesmas</option>
                </select>
              </div><!-- /.form-group -->
            </div>
          </div><!-- /.box-body -->

          <div class="box-header with-border">
            <h3 class="box-title">Nama Penyakit</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="form-group col-md-12">
              <!--<label>Nama Penyakit</label>-->
              <select class="form-control diseases-filter" name="f_disease[]" multiple="multiple" data-placeholder="Pilih Nama Penyakit" style="width: 100%;">
                <?php foreach($model['diseases'] as $key => $value): ?>
                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                <?php endforeach; ?>
              </select>
              <p style="margin-top: 7px;">Ketikkan nama penyakit yang ingin dicari, lalu pilih satu nama penyakit dari daftar hasil pencarian. Ulangi pencarian nama penyakit sampai semua nama penyakit yang Anda inginkan terdaftar di sini. Abaikan atau kosongkan jika ingin menampilkan semua penyakit.</p>
            </div><!-- /.form-group -->
          </div>

          <div class="box-footer">
            <?php
              $attr = array('class' => 'btn btn-block btn-info', 'type' => 'submit', 'style' => 'float:left; width:72%;', 'content' => 'Tampilkan Laporan');
              echo form_button($attr);
            ?>
            <a href="#" style="float:right; width:27%; line-height:34px; text-align:center;">Ubah ke awal</a>
          </div>
        </div><!-- /.box -->
      </div><!-- /.col -->
    <?php echo form_close() ?>
  </div> <!-- /.row -->

  <script type="text/javascript">
    (function(jq) {
      var Dinkes = function(element, options) {
        var obj  = this;
        var conf = jq.extend({}, options || {});
        var elem = jq(element);

        this.constructor = function() {

        }

        this.constructor();
      }

      var FormChain = function(element, options) {
        var defaults = {
          /** NAMESPACES **/
          controlClass      : '.form-control',
          controlGroupClass : '.form-group',
          dynamicDataClass  : '.dyndata',
          /** PARAMETERS **/
          chainControlsName : [],
          preloaderText     : {
            starter    : [],
            onAjaxSend : 'Memuat...',
            onSkip     : 'Masukan data ini akan diabaikan'
          },
          /** CALLBACKS **/
          onValueChange     : null,
          onInit            : null,
          onAjaxSuccess     : null,
          onAjaxFailed      : null
        };
        var proto    = jq.extend(true, defaults, options);
        var obj      = this;
        var root     = jq(element);
        var inputs   = [];
        var scope    = [];
        var code     = [];

        obj.makeInputAlias = null;

        var _constructor = function() {
          var controls = root.find(proto.controlClass);
          var counter = 0;

          // index, control
          jq.each(controls, function(i, c) {
            var chain = jq(c).data('is-chain');

            if(typeof chain !== 'undefined' && chain) {
              inputs[counter] = c;
              scope[counter] = c.dataset.chainControl;
              c.dataset.chainOrder = counter;
              counter++;
            }
            else { return true };
          });
        }

        // order
        var _do_onValueChange = function(o) {
          if( typeof proto.onValueChange !== 'function' ) { return; }
          proto.onValueChange.call(this, obj, scope[o]);
        }

        // order, value, event
        var _get_current_value = function(o, v, e) {
          _do_onValueChange(o);

          if(typeof v === 'undefined') {
            console.log('Value was undefined!');
          }
          else if( v.toLowerCase() === 'none' || v === '' || v === null ) {
            console.log('Value is not permitted for POST data');
          }
          else if( o !== inputs.length - 1 ) {
            if(obj.makeInputAlias !== null && obj.makeInputAlias !== '') {
              code[o] = root.find(proto.controlClass + '[data-chain-control=' + obj.makeInputAlias + ']')[0].value;
              obj.makeInputAlias = null;
            }
            else { code[o] = v; }

            if(v == 'all') { _reset_further_input(o); }
            else { _fetch_options_data(o); }
          }
        }

        // order, refetch
        var _reset_further_input = function(o, r) {
          r = typeof r != 'undefined' ? r : false;

          if(r == true || r == 1) {
            var nextInput = jq(inputs[o+1]);

            nextInput.prev('.preloader').html(proto.preloaderText.onAjaxSend);

            if( nextInput.is(':visible') ) {
              nextInput.hide().prev('.preloader').show();
            }

            o += 1;
          }

          var nextAllInputs = [];
          var textString = (r == true || r == 1) ? proto.preloaderText.starter[o] : proto.preloaderText.onSkip;

          for(var next=o+1; next<inputs.length; next++) {
            nextAllInputs.push(inputs[next]);
            inputs[next].selectedIndex = 0;

            if( jq(inputs[next]).prev('.preloader').is(':visible') ) {
              jq(inputs[next]).prev('.preloader').html(textString);
            }
            else {
              jq(inputs[next]).prev('.preloader').html(textString).show();
            }
          }

          jq(nextAllInputs).prop('disabled', true).hide();
        }

        // order
        var _fetch_options_data = function(o) {
          var previousScope = scope[o];
          // var responseCondition = false;
          var nextInput = jq(inputs[o+1]);

          jq.ajax({
            method   : 'POST',
            dataType : 'json',
            data     : { prev_scope: previousScope, code: code[o] },
            timeout  : 3500,
            url      : '/api/json/get_chain_options',
            //
            beforeSend: function() {
              _reset_further_input(o, true);
            },
            // response
            success: function(r) {
              var optionMarkup = '';
              var fetchedData = [];

              if( nextInput.children(proto.dynamicDataClass).length > 0 ) {
                nextInput.children(proto.dynamicDataClass).remove();
              }

              nextInput[0].selectedIndex = 0;

              if( nextInput.prop('disabled', true) ) {
                nextInput.prop('disabled', false);
              }

              for(var i=0; i<r.length; i++) {
                fetchedData[i] = [];

                // key, value
                jq.each(r[i], function(k, v) { fetchedData[i].push(v); });

                optionMarkup += '<option class="'+proto.dynamicDataClass.substr(1)+'" value="'+fetchedData[i][0]+'">'+fetchedData[i][1]+'</option>';
              }

              nextInput.append(optionMarkup);
            },
            // response, status, message
            error: function(r, s, m) {
              console.log(s);
              console.log(m);
            },
            // response
            complete: function(r) {
              // console.log(previousScope);
              var delay = 725;
              setTimeout(function() {
                jq(inputs[o+1]).show().prev('.preloader').hide();
              }, delay);
            }
          });
        }

        // Call the constructor to begin the
        // initialization of plugin
        _constructor();

        // event
        jq(inputs).on('change', function(e) {
          var order = parseInt( this.dataset.chainOrder );
          var value = this.selectedOptions[0].value;

          _get_current_value(order, value, e);
        });

        // event
        jq(inputs).on('keyup', function(e) {
          if(e.which == 38 || e.which == 40) {
            jq(this).trigger('change');
            return false;
          }
        });
      }

      jq.fn.extend({
        dinkes: function(options) {
          return this.each(function() {
            if( jq(this).data('dw-extension') ) { return; }

            var dinkes = new Dinkes(this, options);
            jq(this).data('dw-extension', dinkes);
          });
        },
        formchain: function(options) {
          if( !jq(this).data('dw-extension') ) { return; }

          return this.each(function() {
            if( jq(this).data('ext-fc') ) { return; }

            var formchain = new FormChain(this, options);
            jq(this).data('ext-fc', formchain);
          });
        }
      });

      jq('#periode-chain').dinkes().formchain({
        preloaderText: {
          starter: [
            'Pilih rentang awal Tahun terlebih dahulu.',
            'Pilih rentang awal terlebih dahulu.',
            'Pilih rentang awal terlebih dahulu.'
          ]
        },
        onValueChange: function(MyChain, currentInput) {
          if (currentInput === 'bulan-awal') {
            MyChain.makeInputAlias = 'tahun-awal';
          }
        }
      });

      jq('#area-chain').dinkes().formchain();

      jq('.diseases-filter').select2();
    })(jQuery);
  </script>
  <?php elseif( $this->uri->segment(3) == 'filter' ): ?>
  <div class="row">
    <section class="col-xs-12 pull-right">
      <div class="btn-group pull-left">
        <h3>Laporan Bulanan (LB1)</h3>
      </div>
    </section>
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-body no-padding">
          <div class="box-body col-md-6">
            <div class="box-header with-border" style="margin-bottom: 12px;">
              <h3 class="box-title">Data Laporan</h3>
            </div><!-- /.box-header -->
            <dl class="dl-horizontal">
              <dt>Periode Laporan</dt>
              <dd><?php echo $model['report_spec']['period_start']; ?> s.d. <?php echo $model['report_spec']['period_end']; ?></dd>
            </dl>
            <dl class="dl-horizontal">
              <dt>Kecamatan</dt>
              <dd><?php echo $model['report_spec']['kecamatan_name']; ?></dd>
            </dl>
            <dl class="dl-horizontal">
              <dt>Puskesmas</dt>
              <dd><?php echo $model['report_spec']['puskesmas_name']; ?></dd>
            </dl>
            <dl class="dl-horizontal">
              <dt>UPT Puskesmas</dt>
              <dd><?php echo $model['report_spec']['upt_name']; ?></dd>
            </dl>
          </div><!-- /.box-body -->
          <div class="box-body col-md-6">
            <div class="box-header with-border" style="margin-bottom: 12px;">
              <h3 class="box-title">Pengaturan Laporan</h3>
            </div><!-- /.box-header -->
            <div class="form-group" style="padding: 0 15px;">
              <label>Umur</label>
              <select class="form-control" style="width: 100%;">
                <option selected="selected">Tampilkan Semua Umur</option>
                <option>0 - 7 Hari</option>
                <option>8 - 28 Hari</option>
                <option>29 Hari - 1 Tahun</option>
                <option>0 - 4 Tahun</option>
                <option>5 - 9 Tahun</option>
                <option>10 - 14 Tahun</option>
                <option>15 - 19 Tahun</option>
                <option>20 - 44 Tahun</option>
                <option>45 - 54 Tahun</option>
                <option>55 - 59 Tahun</option>
                <option>60 - 69 Tahun</option>
                <option>Di atas 70 Tahun</option>
              </select>
            </div><!-- /.form-group -->
            <div class="form-group" style="padding: 0 15px;">
              <label>Jenis Kelamin</label>
              <div class="radio">
                <label class="radio-inline" for="optionsRadios1">
                  <input type="radio" name="f_gender" id="optionsRadios1" checked="checked" value="all"> Tampilkan Semua
                </label>
                <label class="radio-inline" for="optionsRadios2">
                  <input type="radio" name="f_gender" id="optionsRadios2" value="P"> Pria Saja
                </label>
                <label class="radio-inline" for="optionsRadios3">
                  <input type="radio" name="f_gender" id="optionsRadios3" value="W"> Wanita Saja
                </label>
              </div>
            </div><!-- /.form-group -->
            <div class="form-group" style="padding: 0 15px;">
              <label>Unduh Laporan</label>
              <?php
                echo form_open('/report/bulanan/export', ['target'=>'_self']);
                echo form_hidden('xls_diagnosis_json', $model['diagnosis_data']);
                echo form_hidden('xls_specification_json', json_encode($model['report_spec']));
                echo form_submit('xls_submit', 'Unduh Laporan Sebagai Berkas Excel (.XLS)', ['class'=>'btn btn-primary']);
                echo form_close();
              ?>
            </div><!-- /.form-group -->
          </div><!-- /.box-body -->
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-body no-padding table-scroll">
          <table class="table table-bordered table-lp" id="tblExport" style="width:100%;">
            <thead>
              <tr>
                <th colspan="36" class="table_head text-center">TABEL DIAGNOSIS LAPORAN BULANAN (LB1)</th>
              </tr>
              <tr class="table_head">
                <style>
                  .middle-aligned {
                    vertical-align:middle !important;
                  }
                  .top-aligned {
                    vertical-align:top;
                  }
                  tr:nth-child(2n) {
                    background-color: #fafafa;
                  }
                </style>
                <td class="text-center middle-aligned" rowspan="2">Subjenis Penyakit</td>
                <td class="text-center middle-aligned" rowspan="2">No.</td>
                <td class="text-center middle-aligned" rowspan="2">Kode</td>
                <td class="text-center middle-aligned" rowspan="2">Nama Penyakit</td>
                <td class="text-center middle-aligned" colspan="2">0-7H</td>
                <td class="text-center middle-aligned" colspan="2">8-28H</td>
                <td class="text-center middle-aligned" colspan="2">29H-1T</td>
                <td class="text-center middle-aligned" colspan="2">1-4T</td>
                <td class="text-center middle-aligned" colspan="2">5-9T</td>
                <td class="text-center middle-aligned" colspan="2">10-14T</td>
                <td class="text-center middle-aligned" colspan="2">15-19T</td>
                <td class="text-center middle-aligned" colspan="2">20-44T</td>
                <td class="text-center middle-aligned" colspan="2">45-54T</td>
                <td class="text-center middle-aligned" colspan="2">55-59T</td>
                <td class="text-center middle-aligned" colspan="2">60-69T</td>
                <td class="text-center middle-aligned" colspan="2">&#8805;70T</td>
                <td class="text-center middle-aligned" colspan="3">Kasus Baru</td>
                <td class="text-center middle-aligned" colspan="3">Kasus Lama</td>
                <td class="text-center middle-aligned" rowspan="2">Total</td>
                <td class="text-center middle-aligned" rowspan="2">Peserta Gakin</td>
              </tr>
              <tr class="table_head">
                <?php for($i=0; $i<12; $i++): ?>
                <td class="text-center">L</td>
                <td class="text-center">P</td>
                <?php endfor; ?>
                <?php for($i=0; $i<2; $i++): ?>
                <td class="text-center">L</td>
                <td class="text-center">P</td>
                <td class="text-center">Jml</td>
                <?php endfor; ?>
              </tr>
            </thead>
            <tbody>
              <?php print_r( $markup['diagnosis_table'] ); ?>
            </tbody>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
  <?php endif; ?>
</section>
