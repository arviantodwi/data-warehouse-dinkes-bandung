<header class="main-header">
  <!-- Logo -->
  <a href="<?php echo base_url(); ?>" class="logo">
    <!-- logo mini sidebar -->
    <span class="logo-mini">
      <img src="<?php echo base_url('images/icon-bandung-small.png'); ?>">
    </span>
    <!-- logo normal dan mobile mode-->
    <span class="logo-lg clearfix">
      <img src="<?php echo base_url('images/icon-bandung-small.png'); ?>">
      <p class="text-top">Data Warehouse</p>
      <p class="text-bottom">Dinas Kesehatan Kota Bandung</p>
    </span>
  </a>
  <nav class="navbar navbar-static-top clearfix" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?php echo base_url('images/user160x.jpg'); ?>" class="user-image">
            <span class="hidden-xs"><?php echo $model['public']['username']; ?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="<?php echo base_url('images/user160x.jpg'); ?>" class="img-circle">
              <p><?php echo $model['public']['real_name']; ?>
                <small><?php echo $model['public']['user_nip']; ?></small>
              </p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
                <a href="/auth/logout" class="btn btn-default btn-flat">Keluar</a>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>
