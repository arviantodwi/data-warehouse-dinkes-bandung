<!DOCTYPE html>
<html lang="id">
<head>
  <!-- Core Meta -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- Additional Meta -->
  <title><?php echo $markup['page_title']; ?> &#8226 Data Warehouse | Dinas Kesehatan Kota Bandung</title>
  
  <!-- Fonts -->
  <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
  <!--<link rel="stylesheet" type="text/css" href="--><?php //echo base_url('css/overide.css'); ?><!--">-->
  
  <!-- Inline Stylesheet -->
  <!--<style type="text/css">
    /*! morris.js CSS v0.5.1 - http://morrisjs.github.io/morris.js/ */
    .morris-hover{position:absolute;z-index:1090}.morris-hover.morris-default-style{border-radius:10px;padding:6px;color:#f9f9f9;background:rgba(0,0,0,.8);border:2px solid rgba(0,0,0,.9);font-weight:600;font-size:14px;text-align:center}.morris-hover.morris-default-style .morris-hover-row-label{font-weight:700;margin:.25em 0}.morris-hover.morris-default-style .morris-hover-point{white-space:nowrap;margin:.1em 0}
  </style>-->
  
  <!-- jQuery 1.12.2 -->
  <script src="<?php echo $this->config->item('cdn', 'jquery'); ?>"></script>
  <script>if(!window.jQuery) document.write('<script src="<?php echo $this->config->item('local', 'jquery'); ?>"><\/script>');</script>

  <!-- Bootstrap CDN -->
  <?php
  $scripts = $this->config->item('bootstrap');

  foreach($scripts as $file) {
    $fileExt = explode('.', $file);

    if($fileExt[ count($fileExt)-1 ] == 'css')
      echo '<link rel="stylesheet" type="text/css" href="'.$file.'">';
    elseif($fileExt[ count($fileExt)-1 ] == 'js')
      echo '<script src="'.$file.'"></script>';
  }
  ?>

  <!-- App Stylesheet -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url( $this->config->item('css', 'dw_script') ); ?>">

  <!-- Plugins & Extensions -->
  <?php
  $scripts = $this->config->item($markup['uri'][0], 'plugin');
  
  foreach($scripts as $file) {
    $fileExt = explode('.', $file);

    if($fileExt[ count($fileExt)-1 ] == 'css')
      echo '<link rel="stylesheet" type="text/css" href="'.$file.'">';
    elseif($fileExt[ count($fileExt)-1 ] == 'js')
      echo '<script src="'.$file.'"></script>';
  }
  ?>
  
  <!-- IE Hack -->
  <!--[if lt IE 9]>
  <?php
  foreach( $this->config->item('ltie') as $file ) {
    echo '<script src="'.base_url($file).'"></script>';
  }
  ?>
  <![endif]-->
</head>
<body class="<?php echo $markup['body_class']; ?>">
  <?php
    if( $this->router->fetch_class() == 'login') {
      echo '<div class="wrapper">';
      $this->load->view($markup['active_view']);
      echo '</div>';
    }
    else {
      $this->load->view($markup['active_view']);
    }

    $scripts = $this->config->item('js', 'dw_script');

    foreach($scripts as $file) {
      echo '<script src="'.base_url($file).'"></script>';
    }
  ?>
</body>
</html>