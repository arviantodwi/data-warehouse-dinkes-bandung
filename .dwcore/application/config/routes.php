<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'landing';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// Dashboard
$route['dashboard'] = 'cpanel/dashboard';

// Report
$route['report/bulanan'] = 'cpanel/report/lb1';
$route['report/bulanan/(:any)'] = 'cpanel/report/lb1/$1';
$route['report/(kesehatan|kematian)_ia'] = 'cpanel/report/ibu_anak/$1_ia';
$route['report/(kesehatan|kematian)_ia/(:any)'] = 'cpanel/report/ibu_anak/$1_ia/$2';
$route['report/kb'] = 'cpanel/report/kb';
$route['report/kb/(:any)'] = 'cpanel/report/kb/$1';
$route['report/gizi'] = 'cpanel/report/gizi';
$route['report/gizi/(:any)'] = 'cpanel/report/gizi/$1';

// Import
$route['import/spreadsheet'] = 'cpanel/import/spreadsheet';
$route['import/spreadsheet/(:any)'] = 'cpanel/import/spreadsheet/$1';
$route['import/sikda'] = 'cpanel/import/sikda';
$route['import/sikda/(:any)'] = 'cpanel/import/sikda/$1';

// Config
$route['config/(:any)'] = 'cpanel/config/$1';
$route['config/(:any)/verify'] = 'cpanel/config/$1/verify';

// Change Log
$route['log'] = 'cpanel/log';

// API
$route['api/json/(:any)'] = 'json/$1';

// $route['report/(:any)/get_json_response/(:any)'] = 'cpanel/get_json_response/$2';