<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

// Path
defined('JS_PATH')      OR define( 'JS_PATH', 'js/');
defined('JS_SRC_PATH')  OR define( 'JS_SRC_PATH', JS_PATH.'src/');
defined('CSS_PATH')     OR define( 'CSS_PATH', 'css/');
defined('CSS_SRC_PATH') OR define( 'CSS_SRC_PATH', CSS_PATH.'src/');

// DW App Script
defined('APP_CSS')  OR define('APP_CSS', CSS_PATH.'datakesbdw.css');
defined('APP_JS')   OR define('APP_JS', JS_PATH.'app.js');
defined('DEMO_JS')  OR define('DEMO_JS', JS_PATH.'demo.js');

// Jquery
defined('JQUERY') OR define('JQUERY', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js');

// Bootstrap 3
defined('BOOTSTRAP_CSS') OR define('BOOTSTRAP_CSS', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css');
defined('BOOTSTRAP_JS')  OR define('BOOTSTRAP_JS', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js');

// Polyfill
defined('HTML5SHIV') OR define('HTML5SHIV', 'https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js');
defined('RESPOND')   OR define('RESPOND', 'https://oss.maxcdn.com/respond/1.4.2/respond.min.js');

// 3rd Party Plugin and Extension
defined('RAPHAEL')              OR define('RAPHAEL', 'https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js');
defined('MORRIS')               OR define('MORRIS', 'https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js');
defined('DROPZONE_CSS')         OR define('DROPZONE_CSS', 'https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css');
defined('DROPZONE_JS')          OR define('DROPZONE_JS', 'https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js');
defined('SELECT2_CSS')          OR define('SELECT2_CSS', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css');
defined('SELECT2_JS')           OR define('SELECT2_JS', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js');
defined('DATATABLES_CSS')       OR define('DATATABLES_CSS', 'https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css');
defined('DATATABLES_CORE')      OR define('DATATABLES_CORE', 'https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js');
defined('DATATABLES_BOOTSTRAP') OR define('DATATABLES_BOOTSTRAP', 'https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js');

// Config Items
$config['dw_script'] = array(
  'css' => APP_CSS,
  'js' => array(APP_JS, DEMO_JS)
);

$config['jquery'] = array(
  'cdn' => JQUERY,
  'local' => JS_PATH.'jquery-1.12.2.min.js'
);

$config['bootstrap'] = array(
  'css' => BOOTSTRAP_CSS,
  'js'  => BOOTSTRAP_JS
);

$config['ltie'] = array(HTML5SHIV, RESPOND);

$config['plugin'] = array(
  'auth' => array(),
  'dashboard' => array(RAPHAEL, MORRIS),
  'import' => array(DROPZONE_CSS, DROPZONE_JS),
  'report' => array(SELECT2_CSS, SELECT2_JS),
  'config' => array(),
  'log' => array(DATATABLES_CSS, DATATABLES_CORE, DATATABLES_BOOTSTRAP)
);

