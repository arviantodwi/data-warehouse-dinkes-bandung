<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

/**
 * Class Landing
 */
class Landing extends CI_Controller {
  /**
   * Landing constructor.
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * 
   */
  public function index() {
    redirect( base_url('auth/login') );
  }
}