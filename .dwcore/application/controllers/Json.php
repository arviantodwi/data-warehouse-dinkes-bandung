<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

/**
 * Class Json
 */
class Json extends MY_Controller {
  /**
   * @var string
   */
  private $contentType;

  /**
   * Json constructor.
   */
  public function __construct() {
    parent::__construct();
    
    $this->contentType = 'application/json';
  }

  /**
   *
   */
  public function get_chain_options() {
    $this->load->model('report/lb1_model');
    
    $previousScope = $this->input->post('prev_scope', TRUE);
    $code = $this->input->post('code', TRUE);
    $chainResult = array();
    
    if($previousScope == 'tahun-awal' || $previousScope == 'tahun-akhir') {
      $chainResult = $this->lb1_model->fetch_month($code);
    }
    elseif($previousScope == 'bulan-awal') {
      $chainResult = $this->lb1_model->fetch_year($code);
    }
    // elseif($previousScope == 'bulan-akhir') {
    //   $chainResult = $this->lb1_model->fetch_kecamatan();
    // }
    elseif($previousScope == 'kecamatan') {
      $chainResult = $this->lb1_model->fetch_puskesmas($code);
    }
    
    if( !empty($chainResult) && !is_null($chainResult) ) {
      $chainResult = json_encode($chainResult);
    }
    else {
      $chainResult = json_encode( array('message' => 'Ada kesalahan pada penelusuran data di dalam basis data. Harap hubungi Administrator.') );
    }
      
    $this->output->set_content_type($this->contentType)->set_output($chainResult);
  }

  /**
   *
   */
  public function xls_upload() {
    if( !empty($_FILES) ) {
      $tempFile = $_FILES['xls_file']['tmp_name'];
      $targetPath = (ENVIRONMENT !== 'production') ? APPPATH.'data/xls/temp/' : '/usr/local/DinkesBdg/import/temp/';
      
      if( !is_dir($targetPath) )
        mkdir($targetPath, 0777, TRUE);
      
      $fileName = $_FILES['xls_file']['name'];
      
      if( file_exists($targetPath.$fileName) )
        $fileName = $this->file_exist_renamer($targetPath, $_FILES['xls_file']['name']);
        
      move_uploaded_file($tempFile, $targetPath.$fileName);
      
      $this->output->set_content_type($this->contentType)->set_output( json_encode($fileName) );
    }
  }

  /**
   * @param $path
   * @param $fileName
   *
   * @return string
   */
  private function file_exist_renamer($path, $fileName) {
    if( $pos = strrpos($fileName, '.') ) {
      $name = substr($fileName, 0, $pos);
      $ext = substr($fileName, $pos);
    }
    else
      $name = $fileName;

    $newPath = $path.'/'.$fileName;
    $newName = $fileName;
    $counter = 1;
    
    while( file_exists($newPath) ) {
      $newName = $name." (duplikasi {$counter})".$ext;
      $newPath = $path.'/'.$newName;
      $counter++;
    }

    return $newName;
  }
} 