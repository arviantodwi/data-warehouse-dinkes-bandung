<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

class Spreadsheet extends MY_Controller {
  private $data = array(
    'markup' => array(),
    'model' => array()
  );

  public function __construct() {
    parent::__construct();

    $this->data['markup'] = array(
      'active_view' => 'cpanel_view',
      'body_class'  => 'hold-transition skin-green-light sidebar-mini',
      'page_title'  => 'Unggah Laporan'
    );

    $this->set_view_data($this->data, 'markup');
  }

  public function index() {
    $this->load->helper('form');
    $years = array();
    $currentYear = (int)date('Y');
    for($year=$currentYear; $year>=2010; $year--)
      array_unshift( $years, (string)$year );

    $months = array( 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember' );
    $period = array('bulan' => $months, 'tahun' => $years);

    $this->load->model('report/spreadsheet_model', 'ss_model');
    $puskesmas = $this->ss_model->fetch_puskesmas( (int)$_SESSION['credential']['puskesmas_id'] );

    $this->set_view_data('periode', $period, 'model');
    $this->set_view_data('puskesmas', $puskesmas, 'model');
    $this->render();
  }

  public function do_reupload() {
    $existFileName = $this->input->post('exist_filename', TRUE);

    unlink('/usr/local/DinkesBdg/Service_DinkesBDG/DT_FINAL/'.$existFileName);

    $newReuploadPosted = array(
      'puskesmas' => $this->input->post('puskesmas_name', TRUE),
      'report'    => $this->input->post('report_type', TRUE),
      'raw_type'  => $this->input->post('report_raw_name', TRUE),
      'month'     => $this->input->post('month_num', TRUE),
      'year'      => $this->input->post('years', TRUE),
      'file_name' => $this->input->post('response_filename', TRUE)
    );

    $this->post_upload(TRUE, $newReuploadPosted);
  }

  public function post_upload($reupload=FALSE, $reuploadData=null) {
    if( !$reupload ) {
      $postedData = array(
        'puskesmas' => $this->input->post('puskesmas_name', TRUE),
        'report'    => $this->input->post('report_type', TRUE),
        'raw_type'  => $this->input->post('report_raw_name', TRUE),
        'month'     => $this->input->post('month_num', TRUE),
        'year'      => $this->input->post('years', TRUE),
        'file_name' => $this->input->post('response_filename', TRUE)
      );
    }
    else {
      $postedData = array(
        'puskesmas' => $reuploadData['puskesmas'],
        'report'    => $reuploadData['report'],
        'raw_type'  => $reuploadData['raw_type'],
        'month'     => $reuploadData['month'],
        'year'      => $reuploadData['year'],
        'file_name' => $reuploadData['file_name']
      );
    }

    $nulledPostFound = FALSE;
    $filePath = (ENVIRONMENT !== 'production') ? APPPATH.'data/xls/temp/' : '/usr/local/DinkesBdg/import/temp/';

    foreach($postedData as $key => $value) {
      if( is_null($value) ) {
        $nulledPostFound = TRUE;
        break;
      }
    }

    if($nulledPostFound) {
      unlink($filePath.$postedData['file_name']);
      $this->session->set_flashdata('postErrorNoData', TRUE);
    }
    else {
      $tempFile = $filePath.$postedData['file_name'];
      $ext = substr( $postedData['file_name'], strrpos($postedData['file_name'], '.') );
      $moveTarget = (ENVIRONMENT !== 'production') ? APPPATH.'data/xls/waiting_for_approval/' : '/usr/local/DinkesBdg/Service_DinkesBDG/DT_STAGING/';

      if(ENVIRONMENT !== 'production') {
        switch( strtolower($postedData['report']) ) {
          case 'ki1':
          case 'ka1':
            $moveTarget .= 'kia/';
          break;

          case 'ki2':
          case 'ka2':
            $moveTarget .= 'kia2/';
          break;

          default:
            $moveTarget .= strtolower($postedData['report']).'/';
          break;
        }
      }

      if( !is_dir($moveTarget) )
        mkdir($moveTarget, 0777, TRUE);

      $monthToString = (strlen($postedData['month']) < 2) ? '0'.$postedData['month'] : $postedData['month'];
      $fileName = strtoupper( $postedData['puskesmas'].'_'.$postedData['report'].'_'.$postedData['year'].$monthToString).$ext;
      $newFile = $moveTarget.$fileName;

      switch( strtolower($postedData['report']) ) {
        case 'ki1':
          $typeOnFinal = 'Kes_I';
        break;

        case 'ka1':
          $typeOnFinal = 'Kes_A';
        break;

        case 'ki2':
        case 'ka2':
          $typeOnFinal = 'KMT';
        break;

        default:
          $typeOnFinal = strtoupper( $postedData['report'] );
        break;
      }
      $fileNameOnFinal = strtoupper( $postedData['puskesmas'] ).'_'.$typeOnFinal.'_00'.$monthToString.'_'.$postedData['year'].$ext;

      if( file_exists('/usr/local/DinkesBdg/Service_DinkesBDG/DT_FINAL/'.$fileNameOnFinal) ) {
        $sessionData = array(
          'message' => 'Berkas '.$fileName.' sudah pernah diproses. Apakah Anda ingin mengganti laporan lama dengan laporan yang baru saja diunggah?',
          'details' => array(
            'currentFile' => $fileName,
            'existedFile' => $fileNameOnFinal
          ),
          'status' => 'duplikasi',
        );

        $this->session->set_flashdata('wsResponseAct', TRUE);
        $this->session->set_flashdata('wsResponse', $sessionData);
        $this->session->set_flashdata('oldUploadData', $postedData);
      }
      else {
        rename($tempFile, $newFile);

        // RESPONSE ON STAGING
        // {"message":"File Laporan Kesehatan Ibu sedang diproses.","detail":"","status":"SUKSES"}
        // {"message":"File Kesehatan Ibu Std Format 205.xlsx tidak dapat ditemukan.","detail":"","status":"ERROR"}
        // RESPONSE ON MASTER
        // {"message":"File template_lb1.xlsx tidak dapat ditemukan.","detail":"","status":"ERROR"}
        // RAW RESPONSE
        // <html><body>{"message":"File Laporan Gizi sedang diproses.","detail":"","status":"SUKSES"}</body></html>

        if(ENVIRONMENT !== 'production')
          $wsIP = '103.28.13.227:9090';
        else
          $wsIP = '127.0.0.1:9090';

        $wsUrl = 'http://'.$wsIP.'/upload?filename='.rawurlencode($fileName).'&type='.rawurlencode($postedData['raw_type']).'&puskesmas='.rawurlencode($postedData['puskesmas']).'&tahun='.$postedData['year'].'&bulan='.$monthToString;

        $stagingResponse = file_get_contents($wsUrl, FALSE);

        $this->convert_response($stagingResponse, $fileName);

        $sessionData = array(
          'message' => $stagingResponse->message,
          'details' => $stagingResponse->detail,
          'status' => $stagingResponse->status
        );

        $this->session->set_flashdata('wsResponse', $sessionData);
      }
    }

    redirect( base_url('import/spreadsheet') );
  }

  private function convert_response(&$response, $fileName) {
    $string = substr(substr($response, 12), 0, -14);
    $response = json_decode($string);

    if(strtolower($response->status) == 'berhasil') {
      $response->message = 'Berkas'.substr(substr($response->message, 4), 0, -1).', dengan nama baru yaitu <strong>'.$fileName.'</strong>';
    }
  }
}
