<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

class Sikda extends MY_Controller {
  private $data = array(
    'markup' => array(),
    'model' => array()
  );
  
  public function __construct() {
    parent::__construct();
    
    $this->data['markup'] = array(
      'active_view' => 'cpanel_view',
      'body_class'  => 'hold-transition skin-green-light sidebar-mini',
      'page_title'  => 'Impor SIKDA'
    );
    
    $this->set_view_data($this->data, 'markup');
  }
  
  public function index() {
    $this->render();
  }
}