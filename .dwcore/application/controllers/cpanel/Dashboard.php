<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

class Dashboard extends MY_Controller {
  private $data = array(
    'markup' => array(),
    'model'  => array()
  );
  
  public function __construct() {
    parent::__construct();
    
    $this->data['markup'] = array(
      'active_view' => 'cpanel_view',
      'body_class'  => 'hold-transition skin-green-light sidebar-mini',
      'page_title'  => 'Beranda'
    );
    
    $this->set_view_data($this->data);
  }
  
  public function index() {
    $this->render();
  }
}