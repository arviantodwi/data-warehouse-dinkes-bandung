<?php
defined('BASEPATH') or exit('Akses langsung tidak diperkenankan');

/**
 * Created by PhpStorm.
 * User: Arvianto
 * Date: 2/9/2016
 * Time: 11:39 PM
 */
class Log extends MY_Controller {
  private $data = array(
    'markup' => array(),
    'model' => array()
  );

  public function __construct() {
    parent::__construct();

    $this->data['markup'] = array(
      'active_view' => 'cpanel_view',
      'body_class'  => 'hold-transition skin-green-light sidebar-mini',
      'page_title'  => 'Riwayat Perubahan'
    );

    $this->set_view_data($this->data);
    $this->load->helper('form');
    $this->load->model('log_model');
  }

  public function index() {
    $logs = $this->log_model->fetch_log();
    $tableRows = '';
    $monthLocale = array( '01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
    $counter = 1;

    foreach($logs as $log) {
      $monthNum = substr($log['log_time'], 5, -12);
      $date = date( 'j', strtotime( $log['log_time'] ) )." {$monthLocale[$monthNum]} ".date('Y - H:i:s', strtotime( $log['log_time'] ) );

      $tableRows .= '<tr>';
      $tableRows .= '<td>'.$counter.'</td>';
      $tableRows .= '<td>'.$date.'</td>';
      $tableRows .= '<td>'.$log['report_type'].'</td>';
      $tableRows .= '<td>'.$log['kode_puskesmas'].' - '.$log['nama_puskesmas'].'</td>';
      $tableRows .= '<td>'.$log['log_message'].'</td>';
      $tableRows .= '</tr>';

      $counter++;
    }

    $this->set_view_data('logs_table', $tableRows, 'model');
    $this->render();
  }
}