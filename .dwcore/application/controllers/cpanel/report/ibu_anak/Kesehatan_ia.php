<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

class Kesehatan_ia extends MY_Controller {
  public function __construct() {
    parent::__construct();

    $this->load->model('report_model');
    $this->load->library('report');
    $this->load->helper('form');

    $this->report->set_report_markup_data('page_title', 'Laporan Kesehatan Ibu & Anak');
  }

  public function index() {
    $this->report->set_report_model_data([
      'years' => $this->report_model->fetch_year(),
      'kecamatan' => $this->report_model->fetch_kecamatan()
    ]);

    $this->set_view_data( $this->report->get_report_data() );

    $this->render();
  }

  public function filter() {
    $nulledFound = FALSE;
    $valuePosted = array(
      'period_start'   => array(
        'year' => $this->input->post('f_period_start[0]', TRUE),
        'month' => $this->input->post('f_period_start[1]', TRUE)
      ),
      'period_end'     => array(
        'year' => $this->input->post('f_period_end[0]', TRUE),
        'month' => $this->input->post('f_period_end[1]', TRUE)
      ),
      'kecamatan_code' => $this->input->post('f_kecamatan', TRUE),
      'puskesmas_code' => $this->input->post('f_puskesmas', TRUE),
      'kia_type'       => $this->input->post('f_kia_type', TRUE)
    );

    foreach($valuePosted as $key=>$value) {
      if( is_array($valuePosted[$key]) ) {
        foreach($value as $subkey=>$subvalue) {
          if($key == 'period_end' && is_null($subvalue) && $valuePosted['period_start']['month'] == 'all' )
            continue;

          if( is_null($subvalue) ) {
            $nulledFound = TRUE;
            break;
          }
        }
      }
      else {
        if( is_null($value) ) {
          if( $key == 'puskesmas_code' && $valuePosted['kecamatan_code'] == 'all' )
            continue;

          $nulledFound = TRUE;
          break;
        }
      }

      if( $nulledFound ) break;
    }

    if( $nulledFound ) {
      $this->session->set_flashdata('errNulledFilter', TRUE);
      redirect( base_url('report/kesehatan_ia') );
    }
    else {
      $this->load->model('report/kesehatan_ia_model', 'kia1_model');

      if($valuePosted['period_start']['month'] != 'all') {
        $periodId = array(
          // param 1: year, param 2: month number
          'start' => $this->report_model->get_period_id($valuePosted['period_start']['year'], $valuePosted['period_start']['month']),
          'end'   => $this->report_model->get_period_id($valuePosted['period_end']['year'], $valuePosted['period_end']['month'])
        );
      }
      else {
        $periodId = $this->report_model->get_period_id($valuePosted['period_start']['year'], 'all');
        $periodId = array(
          'start' => $periodId[0]['id'],
          'end' => $periodId[count($periodId)-1]['id']
        );
      }

      if( $valuePosted['kecamatan_code'] != 'all' ) {
        if( $valuePosted['puskesmas_code'] != 'all' ) {
          $puskesmasId = $this->report_model->get_puskesmas_id( $valuePosted['puskesmas_code'] );
        }
        else {
          $puskesmasId = $this->report_model->fetch_related_puskesmas_on_kecamatan( $valuePosted['kecamatan_code'] );
        }
      }
      else {
        $puskesmasId = $this->report_model->get_all_puskesmas_id();
      }

      $kia1Diagnosis = ($valuePosted['kia_type'] == 'kia_anak') ? $this->kia1_model->fetch_anak_diagnosis($valuePosted, $periodId, $puskesmasId) : $this->kia1_model->fetch_ibu_diagnosis($valuePosted, $periodId, $puskesmasId) ;

      //var_dump($kia1Diagnosis); exit();

      $monthString = array(1=>'Januari', 2=>'Februari', 3=>'Maret', 4=>'April', 5=>'Mei', 6=>'Juni', 7=>'Juli', 8=>'Agustus', 9=>'September', 10=>'Oktober', 11=>'November', 12=>'Desember');

      if( $valuePosted['period_start']['month'] !== 'all' ) {
        $valuePosted['period_start'] = $monthString[ $valuePosted['period_start']['month'] ].' '.$valuePosted['period_start']['year'];
        $valuePosted['period_end'] = $monthString[ $valuePosted['period_end']['month'] ].' '.$valuePosted['period_end']['year'];
      }
      else {
        $year = $valuePosted['period_start']['year'];
        $valuePosted['period_start'] = $monthString[1].' '.$year;
        $valuePosted['period_end'] = $monthString[12].' '.$year;
      }

      if( $valuePosted['kecamatan_code'] != 'all' ) {
        $valuePosted['kecamatan_name'] = $this->report_model->get_kecamatan_name( $valuePosted['kecamatan_code'] );
        $valuePosted['puskesmas_name'] = $this->report_model->get_puskesmas_name( $puskesmasId );

        if( is_array($valuePosted['puskesmas_name']) ) {
          $tempPuskesmasName = '';

          for($i=0; $i<count( $valuePosted['puskesmas_name'] ); $i++) {
            $tempPuskesmasName .= $valuePosted['puskesmas_name'][$i]['nama_puskesmas'];

            if($i !== count( $valuePosted['puskesmas_name'] )-1)
              $tempPuskesmasName .= ', ';
          }

          $valuePosted['puskesmas_name'] = $tempPuskesmasName;
        }

        $valuePosted['upt_name'] = ( substr($valuePosted['puskesmas_name'], 0, 3) == 'UPT' ) ? $valuePosted['puskesmas_name'] : $this->report_model->get_upt_from_kecamatan( $valuePosted['kecamatan_code'] );
      }
      else {
        $valuePosted['kecamatan_name'] = 'Semua Kecamatan';
        $valuePosted['puskesmas_name'] = '-';
        $valuePosted['upt_name'] = '-';
      }

      $valuePosted['report_name'] = ( $valuePosted['kia_type'] == 'kia_anak' ) ? 'Laporan Kesehatan Anak' : 'Laporan Kesehatan Ibu' ;

      $this->set_view_data( $this->report->get_report_data() );

      if( $valuePosted['kia_type'] == 'kia_anak' )
        $this->set_view_data('kesehatan_table', $this->make_k_anak_stack($kia1Diagnosis, $periodId['end']), 'markup');
      else
        $this->set_view_data('kesehatan_table', $this->make_k_ibu_stack($kia1Diagnosis, $periodId['end']), 'markup');

      $this->set_view_data('diagnosis_data', json_encode($kia1Diagnosis), 'model');
      $this->set_view_data('report_spec', $valuePosted, 'model');
      $this->render();
    }
  }

  public function export() {
    $this->load->library('export_xls');
    // set active sheet
    $this->export_xls->setActiveSheetIndex(0);
    $sheet = $this->export_xls->getActiveSheet();

    $diagnosisData = json_decode( $this->input->post('xls_diagnosis_json', TRUE), TRUE );
    $specificationData = json_decode( $this->input->post('xls_specification_json', TRUE), TRUE );
    $currentRow = 1;

    // set sheet title
    $sheet->setTitle('Sheet 1');

    // generate the top section of report
    $sheet->setCellValueByColumnAndRow('A', $currentRow, 'LAPORAN BULANAN KIA-KB');
    $sheet->mergeCells('A1:H1');
    $sheet->insertNewRowBefore($currentRow+1);

    $currentRow = $sheet->getHighestRow() + 1;

    for($i=0; $i<4; $i++) {
      $label = '';
      $value = '';

      if($i == 0) {
        $label = 'Tahun';
        $value = substr( $specificationData['period_start'], strlen( $specificationData['period_start'] ) - 4, strlen( $specificationData['period_start'] ) ).' - '.substr( $specificationData['period_end'], strlen( $specificationData['period_end'] ) - 4, strlen( $specificationData['period_end'] ) );
      }
      elseif($i == 1) {
        $label = 'Periode';
        $value = $specificationData['period_start'].' - '.$specificationData['period_end'];
      }
      elseif($i == 2) {
        $label = 'Puskesmas';
        $value = $specificationData['puskesmas_name'];
      }
      elseif($i == 3) {
        $label = 'UPT Puskesmas';
        $value = $specificationData['upt_name'];
      }

      $sheet->setCellValue('A'.$currentRow, $label);
      $sheet->setCellValue('E'.$currentRow, $value);
      $sheet->mergeCells("A{$currentRow}:D{$currentRow}");
      $sheet->mergeCells("E{$currentRow}:H{$currentRow}");

      $currentRow++;
    }

    if($specificationData['kia_type'] == 'kia_ibu')
      $sheet->setCellValueByColumnAndRow('A', $currentRow+1, 'A. KESEHATAN IBU');
    elseif($specificationData['kia_type'] == 'kia_anak')
      $sheet->setCellValueByColumnAndRow('A', $currentRow+1, 'B. KESEHATAN ANAK');

    $currentRow = $sheet->getHighestRow();
    $sheet->mergeCells( 'A'.$currentRow.':H'.$currentRow );
    $currentRow++;

    $sheet->setCellValueByColumnAndRow('A', $currentRow, 'Otomatis hitung dari n bulan lalu ditambah bulan ini, kecuali awal tahun kum = n');
    $sheet->mergeCells( 'A'.$currentRow.':H'.$currentRow );
    $currentRow++;

    // generate table head
    $theadLabel = array(
      0 => array(
        'label' => 'NO',
        'col'   => 'A',
        'merge' => 'A'.$currentRow.':A'.($currentRow+3)
      ),
      1 => array(
        'label' => 'RINCIAN KEGIATAN',
        'col'   => 'B',
        'merge' => 'B'.$currentRow.':D'.($currentRow+3)
      ),
      2 => array(
        'label' => 'CAKUPAN PROGRAM',
        'col'   => 'E',
        'merge' => 'E'.$currentRow.':H'.($currentRow+2)
      )
    );
    $theadSubLabel = array('n', 'kum', '%', 'gakin');

    for($i=0; $i<count($theadLabel); $i++) {
      $sheet->setCellValue( $theadLabel[$i]['col'].$currentRow, $theadLabel[$i]['label'] );
      $sheet->mergeCells( $theadLabel[$i]['merge'] );

      if($i == 2) {
        $newOrdinalCol = ord( $theadLabel[$i]['col'] );

        for($j=0; $j<count($theadSubLabel); $j++) {
          $sheet->setCellValue( chr($newOrdinalCol).($currentRow+3), $theadSubLabel[$j] );

          $newOrdinalCol++;
        }
      }
    }

    $currentRow = $sheet->getHighestRow() + 1;

    // generate table content
    $rowLabel = null;
    $rowValue = array(
      'n' => array(),
      'kum' => array(),
      'etc' => '-'
    );
    $highestPeriod = 0;

    if($specificationData['kia_type'] == 'kia_ibu') {
      $rowLabel = array(
        1 => array(
          'val' => 'IBU HAMIL',
          'sub' => array(
            'a' => 'Ibu Hamil Baru',
            'b' => 'Kunjungan Ibu Hamil K1 Murni',
            'c' => 'Kunjungan Ibu Hamil K1 Akses',
            'd' => 'Kunjungan Ibu Hamil K1',
            'e' => 'Kunjungan Ibu Hamil K4',
            'f' => array(
              'val' => 'Deteksi Risiko Ibu Hamil Oleh:',
              1 => 'Tenaga Kesehatan',
              2 => 'Masyarakat'
            ),
            'g' => array(
              'val' => 'Faktor Risiko Ibu Hamil:',
              1 => 'Usia Ibu < 20 Tahun atau > 35 Tahun',
              2 => 'Anak >= 4',
              3 => 'Jarak Persalinan Terakhir < 2 Tahun',
              4 => 'Tinggi Badan Ibu < 145cm',
              5 => 'Tekanan Darah > 140/90 mmHg',
              6 => 'Kelainan Jumlah Janin',
              7 => 'Kelainan Besar Janin',
              8 => 'Kelainan Letak dan Posisi Janin',
              9 => 'Menderita Penyakit Kronis',
              10 => 'Riwayat Obstetri Buruk',
              11 => 'Riwayat Kehamilan Buruk',
              12 => 'Riwayat Persalinan dengan Komplikasi',
              13 => 'Riwayat Penyakit Dalam Keluarga'
            ),
            'h' => 'Jumlah Rujukan Kasus Risiko Tinggi Ibu Hamil',
            'i' => 'Jumlah Ibu Hamil Punya Buku KIA'
          )
        ),
        2 => array(
          'val' => 'IBU BERSALIN',
          'sub' => array(
            'a' => 'Jumlah Persalinan',
            'b' => 'Jumlah Lahir Hidup',
            'c' => 'Jumlah Lahir Mati',
            'd' => 'Jumlah IUFD',
            'e' => array(
              'val' => 'Bayi Lahir Mati Berdasarkan Tempat',
              1 => 'Dokter SpOG',
              2 => 'Dokter Umum',
              3 => 'Bidan',
              4 => 'Rumah Sakit / RB / BPS',
              5 => 'Lain-lain'
            ),
            'f' => array(
              'val' => 'Jumlah Persalinan Oleh Tenaga Kesehatan',
              1 => 'Dokter SpOG',
              2 => 'Dokter Umum',
              3 => 'Bidan'
            ),
            'g' => 'Jumlah Persalinan Oleh Dukun Paraji',
            'h' => 'Jumlah Persalinan Oleh Lain-lain',
            'i' => 'Jumlah Persalinan Di Fasilitas Kesehatan',
            'j' => 'Jumlah Persalinan Di Non Fasilitas Kesehatan',
            'k' => 'Jumlah persalinan Di Rumah',
            'l' => 'Jumlah persalinan Di Tempat Lain-lain'
          )
        ),
        3 => array(
          'val' => 'IBU NIFAS',
          'sub' => array(
            'a' => 'Ibu Nifas Baru',
            'b' => 'Ibu Menyusui Baru',
            'c' => 'Jumlah Kunjungan Nifas 1 (KF1) - 6 Jam s.d 3 Hari',
            'd' => 'Jumlah Kunjungan Nifas 2 (KF2) - 4 Hari s.d 28 Hari',
            'e' => 'Jumlah Kunjungan Nifas 3 (KF3) - 29 Hari s.d 42 Hari'
          )
        ),
        4 => array(
          'val' => 'PENANGANAN KOMPLIKASI KEBIDANAN',
          'sub' => array(
            'a' => 'Jumlah Komplikasi Kebidanan',
            'b' => 'Jumlah Komplikasi Kebidanan Tertangani',
            'c' => 'Jumlah Komplikasi Kebidanan Dirujuk',
            'd' => 'Jumlah Komplikasi Kebidanan Ditangani Tetapi Meninggal',
            'e' => array(
              'val' => 'Kasus Komplikasi Kebidanan :',
              1 => 'Abortus',
              2 => 'Plasenta Previa',
              3 => 'Solusio Plasenta',
              4 => 'Robekan Rahim',
              5 => 'Atonia Uteri',
              6 => 'Retensio Plasenta',
              7 => 'Plasenta Inkarserata',
              8 => 'Sub Involusi Uteri',
              9 => 'kelainan Pembekuan Darah',
              10 => 'Ketuban Pecah Dini (KPD)',
              11 => 'Hipertensi Dalam Kehamilan (HDK)',
              12 => 'Ancaman Persalinan Prematur',
              13 => 'Infeksi Berat Dalam Kehamilan',
              14 => 'Infeksi Masa Nifas',
              15 => 'Distosia',
              16 => 'Lain-lain'
            )
          )
        ),
        5 => array(
          'val' => 'PELAYANAN ANTENATAL INTEGRASI TERKINI',
          'sub' => array(
            'a' => array(
              'val' => 'Pencegahan Penularan HIV dari Ibu ke Bayi (PMTCT)',
              1 => 'Jumlah Ibu hamil yang dikonseling PMTCT',
              2 => 'Jumlah Ibu hamil kasus PMTCT yang ditangani',
              3 => 'Jumlah Ibu Seropositif',
              4 => 'Jumlah Bayi Seropositif'
            ),
            'b' => array(
              'val' => 'Pencegahan Malaria dalam Kehamilan (PMDK)',
              1 => 'Jumlah Ibu hamil diperiksa darah malaria',
              2 => 'Jumlah Ibu Seropositif malaria'
            ),
            'c' => array(
              'val' => 'Pencegahan Kecacingan dalam Kehamilan',
              1 => 'Jumlah Ibu hamil diperiksa ankylostoma',
              2 => 'Jumlah Ibu hamil seropositif ankylostoma'
            ),
            'd' => array(
              'val' => 'Pencegahan IMS dalam Kehamilan',
              1 => 'Jumlah Ibu hamil diiperiksa Sifilis',
              2 => 'Jumlah Ibu hamil diperiksa Hepatitis'
            )
          )
        ),
        6 => array(
          'val' => 'PELAYANAN PENANGGULANGAN KEKERASAN TERHADAP PEREMPUAN',
          'sub' => array(
            'a' => 'Perempuan mengalami kekerasan mental',
            'b' => 'Perempuan mengalami kekerasan fisik',
            'c' => 'Perempuan mengalami kekerasan emosional',
            'd' => 'Perempuan mengalami penelantaran',
            'e' => 'Jumlah penanganan KtP'
          )
        ),
        7 => array(
          'val' => 'DUKUN PARAJI',
          'sub' => array(
            'a' => 'Jumlah ibu hamil yang diperiksa',
            'b' => 'Jumlah pertolongan persalinan',
            'c' => 'Jumlah bayi lahir hidup',
            'd' => 'Jumlah bayi lahir mati',
            'e' => 'Jumlah bayi BBLR',
            'f' => 'Jumlah kasus penyulit kehamilan',
            'g' => 'Jumlah kasus persalinan',
            'h' => 'Jumlah rujukan ibu',
            'i' => 'Jumlah rujukan bayi'
          )
        ),
        8 => array(
          'val' => 'PWS KIA LUAR WILAYAH',
          'sub' => array(
            'a' => 'Cakupan K1',
            'b' => 'Cakupan K4',
            'c' => 'Cakupan Pertolongan Persalinan oleh Nakes',
            'd' => 'Cakupan KF 1',
            'e' => 'Cakupan KF Lengkap',
            'f' => 'Cakupan KN 1',
            'g' => 'Cakupan KN Lengkap',
            'h' => 'Jumlah Ibu Hamil Risiko Tinggi',
            'i' => 'Jumlah Neonatus Risti',
            'j' => 'Jumlah komplikasi obstetri tertangani',
            'k' => 'Jumlah komplikasi neonatus tertangani'
          )
        )
      );
    }
    elseif($specificationData['kia_type'] == 'kia_anak') {
      $rowLabel = array(
        1 => array(
          'val' => 'NEONATUS',
          'sub' => array(
            'a' => 'Neonatal (0-29 Hari) Baru',
            'b' => 'Bayi Lahir Prematur < 37 Minggu',
            'c' => 'Bayi Berat Lahir Rendah < 2500gr',
            'd' => 'Jumlah BBLR Ditangani',
            'e' => 'Jumlah BBLR Dirujuk',
            'f' => 'Jumlah Neonatus Mendapat Vitamin K1',
            'g' => 'Jumlah Neonatus Melaksanakan Inisiasi Menyusu Dini',
            'h' => 'Jumlah Kunjungan Neonatal 1 (KN1) - 6 s.d 48 Jam',
            'i' => 'Jumlah Kunjungan Neonatal 2 (KN2) - 2 s.d 7 Hari',
            'j' => 'Jumlah Kunjungan Neonatal 3 (KN3) - 8 s.d 28 Hari',
            'k' => array(
              'val' => 'Penanganan Komplikasi Neonatus (0-28 Hari)',
              1 => 'Neonatus Komplikasi yang Tertangani / Selamat',
              2 => 'Neonatus Komplikasi yang Dirujuk',
              3 => 'Neonatus Komplikasi Ditangani Tetapi Meninggal'
            ),
            'l' => array(
              'val' => 'Komplikasi Pada Neonatal',
              1 => 'Prematuritas',
              2 => 'BBLR',
              3 => 'Asfiksia',
              4 => 'Infeksi Bakteri',
              5 => 'Kejang',
              6 => 'Ikterus',
              7 => 'Diare',
              8 => 'Hipotermia',
              9 => 'Tetanus Neonatorum',
              10 => 'Masalah Pemberian ASI',
              11 => 'Trauma Lahir',
              12 => 'Sindroma Gangguan Pernafasan',
              13 => 'Kelainan Kongenital',
              14 => 'Lain-lain'
            )
          )
        ),
        2 => array(
          'val' => 'BAYI',
          'sub' => array(
            'a' => 'Bayi 6 Bulan Baru',
            'b' => 'Bayi 7-11 Bulan Baru',
            'c' => 'Bayi (0 - < 1 Tahun) Baru',
            'd' => 'Jumlah Cakupan Pelayanan Bayi (29 Hari - 1 Tahun, Meliputi Kunjungan I, II, III, IV)',
            'e' => 'Jumlah Bayi Punya Buku KIA'
          )
        ),
        3 => array(
          'val' => 'BALITA',
          'sub' => array(
            'a' => 'Jumlah Cakupan Pelayanan Balita (12-59 Bulan)',
            'b' => 'Jumlah Balita Punya Buku KIA'
          )
        )
      );
    }

    foreach($rowLabel as $k1=>$v1) {
      $sheet->setCellValue('A'.$currentRow, $k1);
      $sheet->setCellValue('B'.$currentRow, $v1['val']);
      $sheet->mergeCells('B'.$currentRow.':D'.$currentRow);

      $currentRow++;

      foreach($v1['sub'] as $k2=>$v2) {
        $sheet->setCellValue('B'.$currentRow, $k2);

        if( !is_array($v2) ) {
          $sheet->setCellValue('C'.$currentRow, $v2);
          $sheet->mergeCells('C'.$currentRow.':D'.$currentRow);

          $currentRow++;
        }
        else {
          $sheet->setCellValue('C'.$currentRow, $v2['val']);
          $sheet->mergeCells('C'.$currentRow.':D'.$currentRow);

          $currentRow++;

          for($i=1; $i<=count($v2)-1; $i++) {
            $sheet->setCellValue('C'.$currentRow, $i);
            $sheet->setCellValue('D'.$currentRow, $v2[$i]);

            $currentRow++;
          }
        }
      }

      $currentRow++;
    }

    for($i=0; $i<count($diagnosisData); $i++) {
      unset($diagnosisData[$i]['nama_puskesmas']);
      unset($diagnosisData[$i]['ID_PUSKESMAS']);

      if( (int)$diagnosisData[$i]['ID_PERIODE'] > $highestPeriod )
        $highestPeriod = (int)$diagnosisData[$i]['ID_PERIODE'];

      if($i == count($diagnosisData)-1 ) {
        foreach($diagnosisData as $v1) {
          $valueCol = ($v1['ID_PERIODE'] == $highestPeriod) ? 'kum' : 'n';

          unset( $v1['ID_PERIODE'] );
          $tempData = array_values($v1);
          $tempCount = count($tempData);

          for($j=0; $j<$tempCount; $j++) {
            if( $j == 0 && (empty($rowValue['n']) || empty($rowValue['kum'])) ) {
              for($k=0; $k<$tempCount; $k++) {
                $rowValue['n'][$k] = 0;
                $rowValue['kum'][$k] = 0;
              }
            }

            if( $valueCol == 'n' )
              $rowValue['kum'][$j] += $tempData[$j];

            $rowValue[$valueCol][$j] += $tempData[$j];
          }
        }
      }
    }

    $initRowAdder = 15;

    for($i=0; $i<count($rowValue['n']); $i++) {
      if($specificationData['kia_type'] == 'kia_ibu') {
        $rowSkip = array( 20, 23, 39, 45, 51, 61, 68, 74, 91, 98, 101, 104, 107, 114, 125 );

        if( in_array( $i+$initRowAdder, $rowSkip ) ) {
          if( $i+$initRowAdder == 39 || $i+$initRowAdder == 61 || $i+$initRowAdder == 68 || $i+$initRowAdder == 107 || $i+$initRowAdder == 114 || $i+$initRowAdder == 125 ) {
            $initRowAdder += 2;
          }
          elseif( $i+$initRowAdder == 91 ) {
            $initRowAdder += 3;
          }
          else {
            $initRowAdder += 1;
          }
        }
      }
      elseif($specificationData['kia_type'] == 'kia_anak') {
        $rowSkip = array( 25, 29, 44, 51 );

        if( in_array( $i+$initRowAdder, $rowSkip ) ) {
          if( $i+$initRowAdder == 44 || $i+$initRowAdder == 51 ) {
            $initRowAdder += 2;
          }
          else {
            $initRowAdder += 1;
          }
        }
      }

      $sheet->setCellValue( 'E'.($i+$initRowAdder), $rowValue['n'][$i] );
      $sheet->setCellValue( 'F'.($i+$initRowAdder), $rowValue['kum'][$i] );
      $sheet->setCellValue( 'G'.($i+$initRowAdder), $rowValue['etc'] );
      $sheet->setCellValue( 'H'.($i+$initRowAdder), $rowValue['etc'] );
    }

    // post sheet styling
    $alignment = $sheet->getStyle('A9:H9')->getAlignment();
    $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

    $alignment = $sheet->getStyle('A10:H13')->getAlignment();
    $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $alignment->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    $alignment = $sheet->getStyle( 'A14:A'.$sheet->getHighestRow() )->getAlignment();
    $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $alignment->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    $alignment = $sheet->getStyle( 'B14:D'.$sheet->getHighestRow() )->getAlignment();
    $alignment->setWrapText(true);
    $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $alignment->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    $sheet->getColumnDimension('A')->setWidth(4);
    $sheet->getColumnDimension('B')->setWidth(4);
    $sheet->getColumnDimension('C')->setWidth(4);
    $sheet->getColumnDimension('D')->setWidth(30);
    $sheet->getColumnDimension('E')->setWidth(8);
    $sheet->getColumnDimension('F')->setWidth(8);
    $sheet->getColumnDimension('G')->setWidth(8);
    $sheet->getColumnDimension('H')->setWidth(8);

    // set what the name of the file to be downloaded
    $fileName = 'test_xls.xls';
    // set related headers and download the generated file
    // tell the browser about the xls mime-type
    header('Content-Type: application/vnd.ms-excel');
    // attach the generated xls file
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    // don't cache the file
    header('Cache-Control: max-age=0');
    // write XLS object and set to PHP output
    $objWriter = PHPExcel_IOFactory::createWriter($this->export_xls, 'Excel5');
    $objWriter->save('php://output');

    // and your download will start :)
  }

  /*private function in_array_loop($needle, $haystack, $strict=false) {
    foreach ($haystack as $item) {
      if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_loop($needle, $item, $strict))) {
        return true;
      }
    }

    return false;
  }*/

  private function make_k_ibu_stack($kiaData, $endPeriodId) {
    $options = array(
      0 => array('Tenaga Kesehatan', 'Masyarakat'),
      1 => array('Usia Ibu &#60; 20 Tahun atau &#62; 35 Tahun', 'Anak &#8805; 4', 'Jarak Persalinan Terakhir &#60; 2 Tahun', 'Tinggi Badan Ibu &#60; 145 cm', 'Tekanan Darah &#62; 140/90 mmHg', 'Kelainan Jumlah Janin', 'Kelainan Besar Janin', 'Kelainan Letak dan Posisi Janin', 'Menderita Penyakit Kronis', 'Riwayat Obstetri Buruk', 'Riwayat Kehamilan Buruk', 'Riwayat Persalinan Dengan Komplikasi', 'Riwayat Penyakit Dalam Keluarga'),
      2 => array('Dokter SpOG', 'Dokter Umum', 'Bidan', 'Rumah Sakit / RB / BPS', 'Lain-lain'),
      3 => array('Dokter SpOG', 'Dokter Umum', 'Bidan'),
      4 => array('Abortus', 'Plasenta Previa', 'Solusio Plasenta', 'Robekan Rahim', 'Atonia Uteri', 'Retensio Plasenta', 'Plasenta Inkarserata', 'Sub Involusi Uteri', 'Kelainan Pembekuan Darah', 'Ketuban Pecah Dini (KPD)', 'Hipertensi Dalam Kehamilan (HDK)', 'Ancaman Persalinan Prematur', 'Infeksi Berat Dalam Kehamilan', 'Infeksi Masa Nifas', 'Distosia', 'Lain-lain'),
      5 => array('Jumlah Ibu hamil yang Dikonseling PMTCT', 'Jumlah Ibu Hamil Kasus PMTCT yang Ditangani', 'Jumlah Ibu Seropositif', 'Jumlah Bayi Seropositif'),
      6 => array('Jumlah Ibu Hamil Diperiksa Darah Malaria', 'Jumlah Ibu Seropositif Malaria'),
      7 => array('Jumlah Ibu Hamil Diperiksa Ankylostoma', 'Jumlah Ibu Hamil Seropositif Ankylostoma'),
      8 => array('Jumlah Ibu Hamil Diperiksa Sifilis', 'Jumlah Ibu Hamil Diperiksa Hepatitis')
    );
    $category = array(
      0 => array(
        'main' => 'Ibu Hamil',
        'sub'  => array(
          'Ibu Hamil Baru'                               => null,
          'Kunjungan Ibu Hamil K1 Murni'                 => null,
          'Kunjungan Ibu Hamil K1 Akses'                 => null,
          'Kunjungan Ibu Hamil K1'                       => null,
          'Kunjungan Ibu Hamil K4'                       => null,
          'Deteksi Risiko Ibu Hamil Oleh'                => $options[0],
          'Faktor Risiko Ibu Hamil Oleh'                 => $options[1],
          'Jumlah Rujukan Kasus Risiko Tinggi Ibu Hamil' => null,
          'Jumlah Ibu Hamil Punya Buku KIA'              => null
        )
      ),
      1 => array(
        'main' => 'Ibu Bersalin',
        'sub'  => array(
          'Jumlah Persalinan'                           => null,
          'Jumlah Lahir Hidup'                          => null,
          'Jumlah Lahir Mati'                           => null,
          'Jumlah IUFD'                                 => null,
          'Bayi Lahir Mati Berdasarkan Tempat'          => $options[2],
          'Jumlah Persalinan Oleh Tenaga Kesehatan'     => $options[3],
          'Jumlah Persalinan Oleh Dukun Paraji'         => null,
          'Jumlah Persalinan Oleh Lain-lain'            => null,
          'Jumlah Persalinan Di Fasilitas Kesehatan'    => null,
          'Jumlah Persalinan Di Nonfasilitas Kesehatan' => null,
          'Jumlah Persalinan Di Rumah'                  => null,
          'Jumlah Persalinan Di Tempat Lain-lain'       => null
        )
      ),
      2 => array(
        'main' => 'Ibu Nifas',
        'sub'  => array(
          'Ibu Nifas Baru'                                        => null,
          'Ibu Menyusui Baru'                                     => null,
          'Jumlah Kunjungan Nifas 1 (KF1) - 6 Jam s.d. 3 Hari'    => null,
          'Jumlah Kunjungan Nifas 2 (KF2) - 4 Hari s.d. 28 Hari'  => null,
          'Jumlah Kunjungan Nifas 3 (KF3) - 29 Hari s.d. 42 Hari' => null
        )
      ),
      3 => array(
        'main' => 'Penanganan Komplikasi Kebidanan',
        'sub'  => array(
          'Jumlah Komplikasi Kebidanan'                            => null,
          'Jumlah Komplikasi Kebidanan Tertangani'                 => null,
          'Jumlah Komplikasi Kebidanan Dirujuk'                    => null,
          'Jumlah Komplikasi Kebidanan Ditangani Tetapi Meninggal' => null,
          'Kasus Komplikasi Kebidanan'                             => $options[4]
        )
      ),
      4 => array(
        'main' => 'Pelayanan Antenatal Integrasi Terkini',
        'sub'  => array(
          'Pencegahan Penularan HIV Dari Ibu ke Bayi (PMTCT)' => $options[5],
          'Pencegahan Malaria Dalam Kehamilan (PMDK)'         => $options[6],
          'Pencegahan Kecacingan Dalam Kehamilan'             => $options[7],
          'Pencegahan IMS Dalam Kehamilan'                    => $options[8]
        )
      ),
      5 => array(
        'main' => 'Pelayanan Penanggulangan Kekerasan Terhadap Perempuan',
        'sub'  => array(
          'Perempuan Mengalami Kekerasan Mental'    => null,
          'Perempuan Mengalami Kekerasan Fisik'     => null,
          'Perempuan Mengalami Kekerasan Emosional' => null,
          'Perempuan Mengalami Penelantaran'        => null,
          'Jumlah Penanganan KTP'                   => null
        )
      ),
      6 => array(
        'main' => 'Dukun Paraji',
        'sub'  => array(
          'Jumlah Ibu Hamil yang Diperiksa' => null,
          'Jumlah Pertolongan Persalinan'   => null,
          'Jumlah Bayi Lahir Hidup'         => null,
          'Jumlah Bayi Lahir Mati'          => null,
          'Jumlah Bayi BBLR'                => null,
          'Jumlah Kasus Penyulit Kehamilan' => null,
          'Jumlah Kasus Persalinan'         => null,
          'Jumlah Rujukan Ibu'              => null,
          'Jumlah Rujukan Bayi'             => null
        )
      ),
      7 => array(
        'main' => 'PWS KIA Luar Wilayah',
        'sub'  => array(
          'Cakupan K1'                                => null,
          'Cakupan K4'                                => null,
          'Cakupan Pertolongan Persalinan Oleh Nakes' => null,
          'Cakupan KF 1'                              => null,
          'Cakupan KF Lengkap'                        => null,
          'Cakupan KN 1'                              => null,
          'Cakupan KN Lengkap'                        => null,
          'Jumlah Ibu Hamil Risiko Tinggi'            => null,
          'Jumlah Neonatus Risti'                     => null,
          'Jumlah Komplikasi Obstetri Tertangani'     => null,
          'Jumlah Komplikasi Neonatus Tertangani'     => null
        )
      )
    );
    $stack = array();

    $this->prepare_stack($stack, $category, $kiaData, $endPeriodId, 'kia_ibu');

    $table = $this->prepare_table($stack, $category, 'kia_ibu');

    return $table;
  }

  private function make_k_anak_stack($kiaData, $endPeriodId) {
    $options = array(
      0 => array('Neonatus Komplikasi yang Tertangani / Selamat', 'Neonatus Komplikasi yang Dirujuk', 'Neonatus Komplikasi Ditangani Tetapi Meninggal'),
      1 => array('Prematuritas', 'BBLR', 'Asfiksia', 'Infeksi Bakteri', 'Kejang', 'Ikterus', 'Diare', 'Hipotermia', 'Tetanus Neonatorum', 'Masalah Pemberian ASI', 'Trauma Lahir', 'Sindroma Gangguan Pernafasan', 'Kelainan Kongenital', 'Lain-lain')
    );
    $category = array(
      0 => array(
        'main' => 'Neonatus',
        'sub'  => array(
          'Neonatal (0-29 Hari) Baru'                          => null,
          'Bayi Lahir Prematur &#60; 37 Minggu'                => null,
          'Bayi Berat Lahir Rendah &#60; 2500 gr'              => null,
          'Jumlah BBLR Ditangani'                              => null,
          'Jumlah BBLR Dirujuk'                                => null,
          'Jumlah Neonatus Mendapat Vitamin K1'                => null,
          'Jumlah Neonatus Melaksanakan Inisiasi Menyusu Dini' => null,
          'Jumlah Kunjungan Neonatal 1 (KN1) - 6 s.d. 48 Jam'  => null,
          'Jumlah Kunjungan Neonatal 2 (KN2) - 2 s.d. 7 Hari'  => null,
          'Jumlah Kunjungan Neonatal 3 (KN3) - 8 s.d. 28 Hari' => null,
          'Penanganan Komplikasi Neonatus (0-28 Hari)'         => $options[0],
          'Komplikasi Pada Neonatal'                           => $options[1]
        )
      ),
      1 => array(
        'main'  => 'Bayi',
        'sub'   => array(
          'Bayi 6 Bulan Baru'                                                                    => null,
          'Bayi 7-11 Bulan Baru'                                                                 => null,
          'Bayi (0 - &#60;1 Tahun) Baru'                                                         => null,
          'Jumlah Cakupan Pelayanan Bayi (29 Hari - 1 Tahun, Meliputi Kunjungan I, II, III, IV)' => null,
          'Jumlah Bayi Punya Buku KIA'                                                           => null
        )
      ),
      2 => array(
        'main' => 'Balita',
        'sub'  => array(
          'Jumlah Cakupan Pelayanan Balita (12 Bulan - 59 Bulan)' => null,
          'Jumlah Balita Punya Buku KIA'                          => null
        )
      )
    );
    $stack = array();

    $this->prepare_stack($stack, $category, $kiaData, $endPeriodId, 'kia_anak');
    //var_dump($stack); exit();
    $table = $this->prepare_table($stack, $category, 'kia_anak');

    return $table;
  }

  private function prepare_stack(&$stack, $category, $kiaData, $endPeriodId, $reportType) {
    $highestPeriodId = 0;

    for($k=0; $k<count($kiaData); $k++) {
      if( (int)$kiaData[$k]['ID_PERIODE'] > $highestPeriodId )
        $highestPeriodId = (int)$kiaData[$k]['ID_PERIODE'];
    }

    foreach($kiaData as $dataValue) {
      $currentPeriodIdCheck = (int)$dataValue['ID_PERIODE'];

      /*
      if( !array_key_exists($dataValue['nama_puskesmas'], $stack) ) {
        $stack[ $dataValue['nama_puskesmas'] ] = array();

        for($i=0; $i<count($category); $i++) {
          foreach( $category[$i]['sub'] as $subKey=>$subValue ) {
            if( is_null( $subValue ) ) {
              $stack[ $dataValue['nama_puskesmas'] ][$i][$subKey] = array(
                'n'     => 0,
                'kum'   => 0,
                '%'     => '-',
                'gakin' => '-'
              );
            }
            else {
              for($j=0; $j<count($subValue); $j++) {
                $stack[ $dataValue['nama_puskesmas'] ][$i][$subKey][$subValue[$j]] = array(
                  'n'     => 0,
                  'kum'   => 0,
                  '%'     => '-',
                  'gakin' => '-'
                );
              }
            }
          }
        }
      }
      */
      
      if( empty($stack) ) {
        for($i=0; $i<count($category); $i++) {
          foreach( $category[$i]['sub'] as $subKey=>$subValue ) {
            if( is_null( $subValue ) ) {
              $stack[$i][$subKey] = array(
                'n'     => 0,
                'kum'   => 0,
                '%'     => '-',
                'gakin' => '-'
              );
            }
            else {
              for($j=0; $j<count($subValue); $j++) {
                $stack[$i][$subKey][$subValue[$j]] = array(
                  'n'     => 0,
                  'kum'   => 0,
                  '%'     => '-',
                  'gakin' => '-'
                );
              }
            }
          }
        }
      }

      for($i=0; $i<count($category); $i++) {
        $this->push_options_value($stack, $i, 'kum', $dataValue, $reportType);

        if($currentPeriodIdCheck < $highestPeriodId)
          $this->push_options_value($stack, $i, 'n', $dataValue, $reportType);
      }
    }
  }

  private function push_options_value(&$stack, $counter, $type, $data, $reportType) {
    if($reportType == 'kia_anak') {
      switch($counter) {
        case 0:
          $stack[$counter]['Neonatal (0-29 Hari) Baru'][$type] += $data['NEO_0_29'];
          $stack[$counter]['Bayi Lahir Prematur &#60; 37 Minggu'][$type] += $data['PREMATUR_37'];
          $stack[$counter]['Bayi Berat Lahir Rendah &#60; 2500 gr'][$type] += $data['BBLR_2500'];
          $stack[$counter]['Jumlah BBLR Ditangani'][$type] += $data['BBLR_DITANGANI'];
          $stack[$counter]['Jumlah BBLR Dirujuk'][$type] += $data['BBLR_DIRUJUK'];
          $stack[$counter]['Jumlah Neonatus Mendapat Vitamin K1'][$type] += $data['NEO_K1'];
          $stack[$counter]['Jumlah Neonatus Melaksanakan Inisiasi Menyusu Dini'][$type] += $data['NEO_IMD'];
          $stack[$counter]['Jumlah Kunjungan Neonatal 1 (KN1) - 6 s.d. 48 Jam'][$type] += $data['KN1_6_48'];
          $stack[$counter]['Jumlah Kunjungan Neonatal 2 (KN2) - 2 s.d. 7 Hari'][$type] += $data['KN2_2_7'];
          $stack[$counter]['Jumlah Kunjungan Neonatal 3 (KN3) - 8 s.d. 28 Hari'][$type] += $data['KN3_8_28'];

          $stack[$counter]['Penanganan Komplikasi Neonatus (0-28 Hari)']['Neonatus Komplikasi yang Tertangani / Selamat'][$type] += $data['NEO_KOMP_SELAMAT'];
          $stack[$counter]['Penanganan Komplikasi Neonatus (0-28 Hari)']['Neonatus Komplikasi yang Dirujuk'][$type] += $data['NEO_KOMP_DIRUJUK'];
          $stack[$counter]['Penanganan Komplikasi Neonatus (0-28 Hari)']['Neonatus Komplikasi Ditangani Tetapi Meninggal'][$type] += $data['NEO_KOMP_MENINGGAL'];

          $stack[$counter]['Komplikasi Pada Neonatal']['Prematuritas'][$type] += $data['NEO_KOMP_PREMATUR'];
          $stack[$counter]['Komplikasi Pada Neonatal']['BBLR'][$type] += $data['NEO_KOMP_BBLR'];
          $stack[$counter]['Komplikasi Pada Neonatal']['Asfiksia'][$type] += $data['NEO_KOMP_ASFIKSIA'];
          $stack[$counter]['Komplikasi Pada Neonatal']['Infeksi Bakteri'][$type] += $data['NEO_KOMP_INFEKSI_BKR'];
          $stack[$counter]['Komplikasi Pada Neonatal']['Kejang'][$type] += $data['NEO_KOMP_KEJANG'];
          $stack[$counter]['Komplikasi Pada Neonatal']['Ikterus'][$type] += $data['NEO_KOMP_IKTERUS'];
          $stack[$counter]['Komplikasi Pada Neonatal']['Diare'][$type] += $data['NEO_KOMP_DIARE'];
          $stack[$counter]['Komplikasi Pada Neonatal']['Hipotermia'][$type] += $data['NEO_KOMP_HIPOTERMIA'];
          $stack[$counter]['Komplikasi Pada Neonatal']['Tetanus Neonatorum'][$type] += $data['NEO_KOMP_TETANUS'];
          $stack[$counter]['Komplikasi Pada Neonatal']['Masalah Pemberian ASI'][$type] += $data['NEO_KOMP_MSL_ASI'];
          $stack[$counter]['Komplikasi Pada Neonatal']['Trauma Lahir'][$type] += $data['NEO_KOMP_TRAUMA_LHR'];
          $stack[$counter]['Komplikasi Pada Neonatal']['Sindroma Gangguan Pernafasan'][$type] += $data['NEO_KOMP_GANGGUAN_NAPAS'];
          $stack[$counter]['Komplikasi Pada Neonatal']['Kelainan Kongenital'][$type] += $data['NEO_KOMP_KEL_KONGENITAL'];
          $stack[$counter]['Komplikasi Pada Neonatal']['Lain-lain'][$type] += $data['NEO_KOMP_LAIN_LAIN'];
        break;

        case 1:
          $stack[$counter]['Bayi 6 Bulan Baru'][$type] += $data['BAYI_6_BARU'];
          $stack[$counter]['Bayi 7-11 Bulan Baru'][$type] += $data['BAYI_7_11_BARU'];
          $stack[$counter]['Bayi (0 - &#60;1 Tahun) Baru'][$type] += $data['BAYI_0_1TH_BARU'];
          $stack[$counter]['Jumlah Cakupan Pelayanan Bayi (29 Hari - 1 Tahun, Meliputi Kunjungan I, II, III, IV)'][$type] += $data['BAYI_29_1TH_PELAYANAN'];
          $stack[$counter]['Jumlah Bayi Punya Buku KIA'][$type] += $data['BAYI_KIA'];
        break;

        case 2:
          $stack[$counter]['Jumlah Cakupan Pelayanan Balita (12 Bulan - 59 Bulan)'][$type] += $data['BALITA_12_59_PELAYANAN'];
          $stack[$counter]['Jumlah Balita Punya Buku KIA'][$type] += $data['BALITA_12_59_KIA'];
        break;
      }
    }
    elseif($reportType == 'kia_ibu') {
      switch($counter) {
        case 0:
          $stack[$counter]['Ibu Hamil Baru'][$type] += $data['HML_HAMIL_BARU'];
          $stack[$counter]['Kunjungan Ibu Hamil K1 Murni'][$type] += $data['HML_HAMIL_K1_M'];
          $stack[$counter]['Kunjungan Ibu Hamil K1 Akses'][$type] += $data['HML_HAMIL_K1_ASK'];
          $stack[$counter]['Kunjungan Ibu Hamil K1'][$type] += $data['HML_HAMIL_K1'];
          $stack[$counter]['Kunjungan Ibu Hamil K4'][$type] += $data['HML_HAMIL_K4'];

          $stack[$counter]['Deteksi Risiko Ibu Hamil Oleh']['Tenaga Kesehatan'][$type] += $data['HML_RESIKO_DETEKSI_TKS'];
          $stack[$counter]['Deteksi Risiko Ibu Hamil Oleh']['Masyarakat'][$type] += $data['HML_RESIKO_DETEKSI_MAS'];

          $stack[$counter]['Faktor Risiko Ibu Hamil Oleh']['Usia Ibu &#60; 20 Tahun atau &#62; 35 Tahun'][$type] += $data['HML_RESIKO_20_35'];
          $stack[$counter]['Faktor Risiko Ibu Hamil Oleh']['Anak &#8805; 4'][$type] += $data['HML_RESIKO_ANAK4'];
          $stack[$counter]['Faktor Risiko Ibu Hamil Oleh']['Jarak Persalinan Terakhir &#60; 2 Tahun'][$type] += $data['HML_RESIKO_JARAK_2'];
          $stack[$counter]['Faktor Risiko Ibu Hamil Oleh']['Tinggi Badan Ibu &#60; 145 cm'][$type] += $data['HML_RESIKO_TINGGI_145'];
          $stack[$counter]['Faktor Risiko Ibu Hamil Oleh']['Tekanan Darah &#62; 140/90 mmHg'][$type] += $data['HML_RESIKO_TENSI_140'];
          $stack[$counter]['Faktor Risiko Ibu Hamil Oleh']['Kelainan Jumlah Janin'][$type] += $data['HML_RESIKO_KELAINAN_JML_JANIN'];
          $stack[$counter]['Faktor Risiko Ibu Hamil Oleh']['Kelainan Besar Janin'][$type] += $data['HML_RESIKO_KELAINAN_BSR_JANIN'];
          $stack[$counter]['Faktor Risiko Ibu Hamil Oleh']['Kelainan Letak dan Posisi Janin'][$type] += $data['HML_RESIKO_KELAINAN_POS_JANIN'];
          $stack[$counter]['Faktor Risiko Ibu Hamil Oleh']['Menderita Penyakit Kronis'][$type] += $data['HML_RESIKO_KRONIS'];
          $stack[$counter]['Faktor Risiko Ibu Hamil Oleh']['Riwayat Obstetri Buruk'][$type] += $data['HML_RESIKO_OBSTETRI'];
          $stack[$counter]['Faktor Risiko Ibu Hamil Oleh']['Riwayat Kehamilan Buruk'][$type] += $data['HML_RESIKO_RIWAYAT_HAMIL_BURUK'];
          $stack[$counter]['Faktor Risiko Ibu Hamil Oleh']['Riwayat Persalinan Dengan Komplikasi'][$type] += $data['HML_RESIKO_RIWAYAT_KOMPLIKASI'];
          $stack[$counter]['Faktor Risiko Ibu Hamil Oleh']['Riwayat Penyakit Dalam Keluarga'][$type] += $data['HML_RESIKO_RIWAYAT_PENYAKIT'];

          $stack[$counter]['Jumlah Rujukan Kasus Risiko Tinggi Ibu Hamil'][$type] += $data['JML_RUJUKAN_RESTI'];
          $stack[$counter]['Jumlah Ibu Hamil Punya Buku KIA'][$type] += $data['JML_BUKU_KIA'];
        break;

        case 1:
          $stack[$counter]['Jumlah Persalinan'][$type] += $data['JML_PERSALINAN'];
          $stack[$counter]['Jumlah Lahir Hidup'][$type] += $data['JML_LAHIR_HIDUP'];
          $stack[$counter]['Jumlah Lahir Mati'][$type] += $data['JML_LAHIR_MATI'];
          $stack[$counter]['Jumlah IUFD'][$type] += $data['JML_IUFD'];

          $stack[$counter]['Bayi Lahir Mati Berdasarkan Tempat']['Dokter SpOG'][$type] += $data['SALIN_BAYI_LHR_MATI_SPOG'];
          $stack[$counter]['Bayi Lahir Mati Berdasarkan Tempat']['Dokter Umum'][$type] += $data['SALIN_BAYI_LHR_MATI_UMUM'];
          $stack[$counter]['Bayi Lahir Mati Berdasarkan Tempat']['Bidan'][$type] += $data['SALIN_BAYI_LHR_MATI_BIDAN'];
          $stack[$counter]['Bayi Lahir Mati Berdasarkan Tempat']['Rumah Sakit / RB / BPS'][$type] += $data['SALIN_BAYI_LHR_MATI_RS'];
          $stack[$counter]['Bayi Lahir Mati Berdasarkan Tempat']['Lain-lain'][$type] += $data['SALIN_BAYI_LHR_MATI_LAIN'];

          $stack[$counter]['Jumlah Persalinan Oleh Tenaga Kesehatan']['Dokter SpOG'][$type] += $data['SALIN_SPOG'];
          $stack[$counter]['Jumlah Persalinan Oleh Tenaga Kesehatan']['Dokter Umum'][$type] += $data['SALIN_UMUM'];
          $stack[$counter]['Jumlah Persalinan Oleh Tenaga Kesehatan']['Bidan'][$type] += $data['SALIN_BIDAN'];

          $stack[$counter]['Jumlah Persalinan Oleh Dukun Paraji'][$type] += $data['SALIN_PARAJI'];
          $stack[$counter]['Jumlah Persalinan Oleh Lain-lain'][$type] += $data['SALIN_LAIN_LAIN'];
          $stack[$counter]['Jumlah Persalinan Di Fasilitas Kesehatan'][$type] += $data['SALIN_DI_FAS_KES'];
          $stack[$counter]['Jumlah Persalinan Di Nonfasilitas Kesehatan'][$type] += $data['SALIN_DI_NON_FAS_KES'];
          $stack[$counter]['Jumlah Persalinan Di Rumah'][$type] += $data['SALIN_DI_RUMAH'];
          $stack[$counter]['Jumlah Persalinan Di Tempat Lain-lain'][$type] += $data['SALIN_DI_LAIN'];
        break;

        case 2:
          $stack[$counter]['Ibu Nifas Baru'][$type] += $data['NIFAS_BARU'];
          $stack[$counter]['Ibu Menyusui Baru'][$type] += $data['NIFAS_SUSU_BARU'];
          $stack[$counter]['Jumlah Kunjungan Nifas 1 (KF1) - 6 Jam s.d. 3 Hari'][$type] += $data['NIFAS_KF1'];
          $stack[$counter]['Jumlah Kunjungan Nifas 2 (KF2) - 4 Hari s.d. 28 Hari'][$type] += $data['NIFAS_KF2'];
          $stack[$counter]['Jumlah Kunjungan Nifas 3 (KF3) - 29 Hari s.d. 42 Hari'][$type] += $data['NIFAS_KF3'];
        break;

        case 3:
          $stack[$counter]['Jumlah Komplikasi Kebidanan'][$type] += $data['KOMPLIKASI_JML'];
          $stack[$counter]['Jumlah Komplikasi Kebidanan Tertangani'][$type] += $data['KOMPLIKASI_TERTANGANI'];
          $stack[$counter]['Jumlah Komplikasi Kebidanan Dirujuk'][$type] += $data['KOMPLIKASI_RUJUK'];
          $stack[$counter]['Jumlah Komplikasi Kebidanan Ditangani Tetapi Meninggal'][$type] += $data['KOMPLIKASI_TERTANGANI_MATI'];

          $stack[$counter]['Kasus Komplikasi Kebidanan']['Abortus'][$type] += $data['KOMPLIKASI_ABORTUS'];
          $stack[$counter]['Kasus Komplikasi Kebidanan']['Plasenta Previa'][$type] += $data['KOMPLIKASI_PLASENTA_PREVIA'];
          $stack[$counter]['Kasus Komplikasi Kebidanan']['Solusio Plasenta'][$type] += $data['KOMPLIKASI_SOLUSIO_PLASENTA'];
          $stack[$counter]['Kasus Komplikasi Kebidanan']['Robekan Rahim'][$type] += $data['KOMPLIKASI_ROBEK_RAHIM'];
          $stack[$counter]['Kasus Komplikasi Kebidanan']['Atonia Uteri'][$type] += $data['KOMPLIKASI_ATORIA_UTERI'];
          $stack[$counter]['Kasus Komplikasi Kebidanan']['Retensio Plasenta'][$type] += $data['KOMPLIKASI_RETENSIO_PLASENTA'];
          $stack[$counter]['Kasus Komplikasi Kebidanan']['Plasenta Inkarserata'][$type] += $data['KOMPLIKASI_PLASENTA_INKARSERATA'];
          $stack[$counter]['Kasus Komplikasi Kebidanan']['Sub Involusi Uteri'][$type] += $data['KOMPLIKASI_SUB_INVOLUSI_UTERI'];
          $stack[$counter]['Kasus Komplikasi Kebidanan']['Kelainan Pembekuan Darah'][$type] += $data['KOMPLIKASI_KELAINAN_BEKU_DARAH'];
          $stack[$counter]['Kasus Komplikasi Kebidanan']['Ketuban Pecah Dini (KPD)'][$type] += $data['KOMPLIKASI_KPD'];
          $stack[$counter]['Kasus Komplikasi Kebidanan']['Hipertensi Dalam Kehamilan (HDK)'][$type] += $data['KOMPLIKASI_HDK'];
          $stack[$counter]['Kasus Komplikasi Kebidanan']['Ancaman Persalinan Prematur'][$type] += $data['KOMPLIKASI_ANCAMAN_PREMATUR'];
          $stack[$counter]['Kasus Komplikasi Kebidanan']['Infeksi Berat Dalam Kehamilan'][$type] += $data['KOMPLIKASI_INFEKSI_BERAT'];
          $stack[$counter]['Kasus Komplikasi Kebidanan']['Infeksi Masa Nifas'][$type] += $data['KOMPLIKASI_INFEKSI_NIFAS'];
          $stack[$counter]['Kasus Komplikasi Kebidanan']['Distosia'][$type] += $data['KOMPLIKASI_DISTOSIA'];
          $stack[$counter]['Kasus Komplikasi Kebidanan']['Lain-lain'][$type] += $data['KOMPLIKASI_LAIN_LAIN'];
        break;

        case 4:
          $stack[$counter]['Pencegahan Penularan HIV Dari Ibu ke Bayi (PMTCT)']['Jumlah Ibu hamil yang Dikonseling PMTCT'][$type] += $data['PMTCT_JML'];
          $stack[$counter]['Pencegahan Penularan HIV Dari Ibu ke Bayi (PMTCT)']['Jumlah Ibu Hamil Kasus PMTCT yang Ditangani'][$type] += $data['PMTCT_TERTANGANI'];
          $stack[$counter]['Pencegahan Penularan HIV Dari Ibu ke Bayi (PMTCT)']['Jumlah Ibu Seropositif'][$type] += $data['PMTCT_IBU_SEROPOSITIF'];
          $stack[$counter]['Pencegahan Penularan HIV Dari Ibu ke Bayi (PMTCT)']['Jumlah Bayi Seropositif'][$type] += $data['PMTCT_BAYI_SEROPOSITIF'];

          $stack[$counter]['Pencegahan Malaria Dalam Kehamilan (PMDK)']['Jumlah Ibu Hamil Diperiksa Darah Malaria'][$type] += $data['PMDK_JML'];
          $stack[$counter]['Pencegahan Malaria Dalam Kehamilan (PMDK)']['Jumlah Ibu Seropositif Malaria'][$type] += $data['PMDK_SEROPOSITIF'];

          $stack[$counter]['Pencegahan Kecacingan Dalam Kehamilan']['Jumlah Ibu Hamil Diperiksa Ankylostoma'][$type] += $data['HAMIL_ANKYLOSTOMA'];
          $stack[$counter]['Pencegahan Kecacingan Dalam Kehamilan']['Jumlah Ibu Hamil Seropositif Ankylostoma'][$type] += $data['HAMIL_SEROPOS_ANKYLOSTOMA'];

          $stack[$counter]['Pencegahan IMS Dalam Kehamilan']['Jumlah Ibu Hamil Diperiksa Sifilis'][$type] += $data['HAMIL_SIFILIS'];
          $stack[$counter]['Pencegahan IMS Dalam Kehamilan']['Jumlah Ibu Hamil Diperiksa Hepatitis'][$type] += $data['HAMIL_HEPATITIS'];
        break;

        case 5:
          $stack[$counter]['Perempuan Mengalami Kekerasan Mental'][$type] += $data['KERAS_MENTAL'];
          $stack[$counter]['Perempuan Mengalami Kekerasan Fisik'][$type] += $data['KERAS_FISIK'];
          $stack[$counter]['Perempuan Mengalami Kekerasan Emosional'][$type] += $data['KERAS_EMOSIONAL'];
          $stack[$counter]['Perempuan Mengalami Penelantaran'][$type] += $data['KERAS_PENELANTARAN'];
          $stack[$counter]['Jumlah Penanganan KTP'][$type] += $data['JML_KTP'];
        break;

        case 6:
          $stack[$counter]['Jumlah Ibu Hamil yang Diperiksa'][$type] += $data['PARAJI_HAMIL'];
          $stack[$counter]['Jumlah Pertolongan Persalinan'][$type] += $data['PARAJI_SALIN'];
          $stack[$counter]['Jumlah Bayi Lahir Hidup'][$type] += $data['PARAJI_LAHIR_HIDUP'];
          $stack[$counter]['Jumlah Bayi Lahir Mati'][$type] += $data['PARAJI_LAHIR_MATI'];
          $stack[$counter]['Jumlah Bayi BBLR'][$type] += $data['PARAJI_BBLR'];
          $stack[$counter]['Jumlah Kasus Penyulit Kehamilan'][$type] += $data['PARAJI_PENYULIT_KEHAMILAN'];
          $stack[$counter]['Jumlah Kasus Persalinan'][$type] += $data['PARAJI_KASUS_PERSALINAN'];
          $stack[$counter]['Jumlah Rujukan Ibu'][$type] += $data['PARAJI_RUJUKAN_IBU'];
          $stack[$counter]['Jumlah Rujukan Bayi'][$type] += $data['PARAJI_RUJUKAN_BAYI'];
        break;

        case 7:
          $stack[$counter]['Cakupan K1'][$type] += $data['PWS_K1'];
          $stack[$counter]['Cakupan K4'][$type] += $data['PWS_K4'];
          $stack[$counter]['Cakupan Pertolongan Persalinan Oleh Nakes'][$type] += $data['PWS_TOLONG_SALIN_NAKES'];
          $stack[$counter]['Cakupan KF 1'][$type] += $data['PWS_KF1'];
          $stack[$counter]['Cakupan KF Lengkap'][$type] += $data['PWS_KF_LENGKAP'];
          $stack[$counter]['Cakupan KN 1'][$type] += $data['PWS_KN1'];
          $stack[$counter]['Cakupan KN Lengkap'][$type] += $data['PWS_KN_LENGKAP'];
          $stack[$counter]['Jumlah Ibu Hamil Risiko Tinggi'][$type] += $data['PWS_HAMIL_RESK_TINGGI'];
          $stack[$counter]['Jumlah Neonatus Risti'][$type] += $data['PWS_NEONATUS_RISTI'];
          $stack[$counter]['Jumlah Komplikasi Obstetri Tertangani'][$type] += $data['PWS_KIMPL_OBSTETRI_TERTANGANI'];
          $stack[$counter]['Jumlah Komplikasi Neonatus Tertangani'][$type] += $data['PWS_KOMPL_NEOTATUS_TERTANGANI'];
        break;
      }
    }
  }

  private function prepare_table($stack, $category, $reportType) {
    $preMarkup = '';
    //$puskesmas = array_keys($stack);
    $rowNumCounter = 1;
    $totalMainRow = ($reportType == 'kia_anak') ? 3 : 8;

    $headMarkup = '<thead><tr class="table_head"><th class="text-center middle-aligned" rowspan="2">No.</th><th class="text-center middle-aligned" rowspan="2">Rincian Kegiatan</th>';
    /*for($i=0; $i<count($puskesmas); $i++) {
      $headMarkup .= '<th class="text-center middle-aligned" colspan="4">'.$puskesmas[$i].'</th>';
    }*/
    $headMarkup .= '<th class="text-center middle-aligned" colspan="4">Cakupan Program</th>';
    $headMarkup .= '</tr><tr class="table-head">';
    /*for($i=0; $i<count($puskesmas); $i++) {
      $headMarkup .= '<th class="text-center middle-aligned">Nilai dari Periode Terakhir</th><th class="text-center middle-aligned">Nilai Kumulatif</th><th class="text-center middle-aligned">%</th><th class="text-center middle-aligned">Gakin</th>';
    }*/
    $headMarkup .= '<th class="text-center middle-aligned">Total <em>n</em> Periode Lalu</th><th class="text-center middle-aligned">Nilai Kumulatif</th><th class="text-center middle-aligned">%</th><th class="text-center middle-aligned">Gakin</th>';
    $headMarkup .= '</tr></thead>';
    $preMarkup .= '<tbody>';

    if( !empty($stack) ) {
      while($rowNumCounter <= $totalMainRow) {
        $preMarkup .= '<tr>';
        $preMarkup .= '<td>'.$rowNumCounter.'.</td><td>'.$category[$rowNumCounter-1]['main'].'</td>';
        $postMarkup = '';

        $preMarkup .= '<td colspan="4"></td></tr>';
        $listPoint = 'A';

        foreach(array_keys( $category[$rowNumCounter-1]['sub'] ) as $key) {
          /*
          $j = 0;

          foreach($puskesmas as $name) {
            if($j == 0)
              $postMarkup .= '<tr><td></td><td>'.$listPoint.'.&emsp;'.$key.'</td>';

            if( array_key_exists('n', $stack[$rowNumCounter-1][$key]) ) {
              $postMarkup .= '<td>'.$stack[$rowNumCounter-1][$key]['n'].'</td>';
              $postMarkup .= '<td>'.$stack[$rowNumCounter-1][$key]['kum'].'</td>';
              $postMarkup .= '<td>'.$stack[$rowNumCounter-1][$key]['%'].'</td>';
              $postMarkup .= '<td>'.$stack[$rowNumCounter-1][$key]['gakin'].'</td>';
            }
            else {
              foreach($stack[$rowNumCounter-1][$key] as $subKey=>$subValue) {
                $postMarkup .= '</tr>';
                $postMarkup .= '<tr><td></td><td>&emsp;&emsp;•&emsp;'.$subKey.'</td>';
                $postMarkup .= '<td>'.$subValue['n'].'</td>';
                $postMarkup .= '<td>'.$subValue['kum'].'</td>';
                $postMarkup .= '<td>'.$subValue['%'].'</td>';
                $postMarkup .= '<td>'.$subValue['gakin'].'</td>';
              }
            }
            $j++;
          }
          */

          $postMarkup .= '<tr><td></td><td>'.$listPoint.'.&emsp;'.$key.'</td>';

          if( array_key_exists('n', $stack[$rowNumCounter-1][$key]) ) {
            $postMarkup .= '<td>'.$stack[$rowNumCounter-1][$key]['n'].'</td>';
            $postMarkup .= '<td>'.$stack[$rowNumCounter-1][$key]['kum'].'</td>';
            $postMarkup .= '<td>'.$stack[$rowNumCounter-1][$key]['%'].'</td>';
            $postMarkup .= '<td>'.$stack[$rowNumCounter-1][$key]['gakin'].'</td>';
          }
          else {
            foreach($stack[$rowNumCounter-1][$key] as $subKey=>$subValue) {
              $postMarkup .= '</tr>';
              $postMarkup .= '<tr><td></td><td>&emsp;&emsp;•&emsp;'.$subKey.'</td>';
              $postMarkup .= '<td>'.$subValue['n'].'</td>';
              $postMarkup .= '<td>'.$subValue['kum'].'</td>';
              $postMarkup .= '<td>'.$subValue['%'].'</td>';
              $postMarkup .= '<td>'.$subValue['gakin'].'</td>';
            }
          }

          $listPoint++;
        }

        $preMarkup .= $postMarkup.'</tr>';
        $rowNumCounter++;
      }
    }
    else {
      $preMarkup .= '<tr class="text-center middle-aligned" style="height: 100px;"><td colspan="6" style="vertical-align: middle;">Maaf, data yang Anda cari tidak tersedia atau masih kosong.</tr>';
    }

    $preMarkup .= '</tbody>';
    $fullRowMarkup = $headMarkup.$preMarkup;

    return $fullRowMarkup;
  }
}
