<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

class Kematian_ia extends MY_Controller {
  public function __construct() {
    parent::__construct();

    $this->load->model('report_model');
    $this->load->library('report');
    $this->load->helper('form');

    $this->report->set_report_markup_data('page_title', 'Laporan Kesehatan Ibu & Anak');
  }

  public function index() {
    $this->report->set_report_model_data([
      'years' => $this->report_model->fetch_year(),
      'kecamatan' => $this->report_model->fetch_kecamatan()
    ]);

    $this->set_view_data( $this->report->get_report_data() );

    $this->render();
  }

  public function filter() {
    $nulledFound = FALSE;
    $valuePosted = array(
      'period_start'   => array(
        'year' => $this->input->post('f_period_start[0]', TRUE),
        'month' => $this->input->post('f_period_start[1]', TRUE)
      ),
      'period_end'     => array(
        'year' => $this->input->post('f_period_end[0]', TRUE),
        'month' => $this->input->post('f_period_end[1]', TRUE)
      ),
      'kecamatan_code' => $this->input->post('f_kecamatan', TRUE),
      'puskesmas_code' => $this->input->post('f_puskesmas', TRUE)
    );

    foreach($valuePosted as $key=>$value) {
      if( is_array($valuePosted[$key]) ) {
        foreach($value as $subkey=>$subvalue) {
          if($key == 'period_end' && is_null($subvalue) && $valuePosted['period_start']['month'] == 'all' )
            continue;

          if( is_null($subvalue) ) {
            $nulledFound = TRUE;
            break;
          }
        }
      }
      else {
        if( is_null($value) ) {
          if( $key == 'puskesmas_code' && $valuePosted['kecamatan_code'] == 'all' )
            continue;

          $nulledFound = TRUE;
          break;
        }
      }

      if( $nulledFound ) break;
    }

    if( $nulledFound ) {
      $this->session->set_flashdata('errNulledFilter', TRUE);
      redirect( base_url('report/kematian_ia') );
    }
    else {
      $this->load->model('report/kematian_ia_model', 'kia2_model');

      if($valuePosted['period_start']['month'] != 'all') {
        $periodId = array(
          // param 1: year, param 2: month number
          'start' => $this->report_model->get_period_id($valuePosted['period_start']['year'], $valuePosted['period_start']['month']),
          'end'   => $this->report_model->get_period_id($valuePosted['period_end']['year'], $valuePosted['period_end']['month'])
        );
      }
      else {
        $periodId = $this->report_model->get_period_id($valuePosted['period_start']['year'], 'all');
        $periodId = array(
          'start' => $periodId[0]['id'],
          'end' => $periodId[count($periodId)-1]['id']
        );
      }

      if( $valuePosted['kecamatan_code'] != 'all' ) {
        $puskesmasId = ($valuePosted['puskesmas_code'] != 'all') ? $this->report_model->get_puskesmas_id( $valuePosted['puskesmas_code'] ) : $this->report_model->fetch_related_puskesmas_on_kecamatan( $valuePosted['kecamatan_code'] );
      }
      else {
        $puskesmasId = $this->report_model->get_all_puskesmas_id();
      }

      $kia2Diagnosis = $this->kia2_model->fetch_kematian_diagnosis($valuePosted, $periodId, $puskesmasId);
      
      // f(x) -> generate_period_string
      $monthString = array(1=>'Januari', 2=>'Februari', 3=>'Maret', 4=>'April', 5=>'Mei', 6=>'Juni', 7=>'Juli', 8=>'Agustus', 9=>'September', 10=>'Oktober', 11=>'November', 12=>'Desember');

      if( $valuePosted['period_start']['month'] !== 'all' ) {
        $valuePosted['period_start'] = $monthString[ $valuePosted['period_start']['month'] ].' '.$valuePosted['period_start']['year'];
        $valuePosted['period_end'] = $monthString[ $valuePosted['period_end']['month'] ].' '.$valuePosted['period_end']['year'];
      }
      else {
        $year = $valuePosted['period_start']['year'];
        $valuePosted['period_start'] = $monthString[1].' '.$year;
        $valuePosted['period_end'] = $monthString[12].' '.$year;
      }
      
      // f(x) -> generate_kecamatan_puskesmas_name
      if( $valuePosted['kecamatan_code'] != 'all' ) {
        $valuePosted['kecamatan_name'] = $this->report_model->get_kecamatan_name( $valuePosted['kecamatan_code'] );
        $valuePosted['puskesmas_name'] = $this->report_model->get_puskesmas_name( $puskesmasId );

        if( is_array($valuePosted['puskesmas_name']) ) {
          $tempPuskesmasName = '';

          for($i=0; $i<count( $valuePosted['puskesmas_name'] ); $i++) {
            $tempPuskesmasName .= $valuePosted['puskesmas_name'][$i]['nama_puskesmas'];

            if($i !== count( $valuePosted['puskesmas_name'] )-1)
              $tempPuskesmasName .= ', ';
          }

          $valuePosted['puskesmas_name'] = $tempPuskesmasName;
        }

        $valuePosted['upt_name'] = ( substr($valuePosted['puskesmas_name'], 0, 3) == 'UPT' ) ? $valuePosted['puskesmas_name'] : $this->report_model->get_upt_from_kecamatan( $valuePosted['kecamatan_code'] );
      }
      else {
        $valuePosted['kecamatan_name'] = 'Semua Kecamatan';
        $valuePosted['puskesmas_name'] = '-';
        $valuePosted['upt_name'] = '-';
      }

      $this->set_view_data( $this->report->get_report_data() );
      $this->set_view_data('kematian_table', $this->make_kematian_stack($kia2Diagnosis, $periodId['end']), 'markup');
      $this->set_view_data('diagnosis_data', json_encode($kia2Diagnosis), 'model');
      $this->set_view_data('report_spec', $valuePosted, 'model');
      $this->render();
    }
  }

  public function export() {
    $this->load->library('export_xls');

    $diagnosisData = json_decode( $this->input->post('xls_diagnosis_json', TRUE), TRUE );
    $specificationData = json_decode( $this->input->post('xls_specification_json', TRUE), TRUE );
    $currentRow = 1;

    // set active sheet
    $this->export_xls->setActiveSheetIndex(0);
    $sheet = $this->export_xls->getActiveSheet();

    // set sheet title
    $sheet->setTitle('Sheet 1');

    // generate the top section of report
    $sheet->setCellValueByColumnAndRow('A', $currentRow, 'LAPORAN BULANAN KIA-KB');
    $sheet->mergeCells('A1:M1');
    $sheet->insertNewRowBefore($currentRow+1);

    $currentRow = $sheet->getHighestRow() + 1;

    for($i=0; $i<4; $i++) {
      $label = '';
      $value = '';

      if($i == 0) {
        $label = 'Tahun';
        $value = substr( $specificationData['period_start'], strlen( $specificationData['period_start'] ) - 4, strlen( $specificationData['period_start'] ) ).' - '.substr( $specificationData['period_end'], strlen( $specificationData['period_end'] ) - 4, strlen( $specificationData['period_end'] ) );
      }
      elseif($i == 1) {
        $label = 'Periode';
        $value = $specificationData['period_start'].' - '.$specificationData['period_end'];
      }
      elseif($i == 2) {
        $label = 'Puskesmas';
        $value = $specificationData['puskesmas_name'];
      }
      elseif($i == 3) {
        $label = 'UPT Puskesmas';
        $value = $specificationData['upt_name'];
      }

      $sheet->setCellValue('A'.$currentRow, $label);
      $sheet->setCellValue('F'.$currentRow, $value);
      $sheet->mergeCells("A{$currentRow}:D{$currentRow}");
      $sheet->mergeCells("F{$currentRow}:I{$currentRow}");

      $currentRow++;
    }

    $sheet->setCellValueByColumnAndRow('A', $currentRow+1, 'D. KEMATIAN');

    $currentRow = $sheet->getHighestRow();
    $sheet->mergeCells( 'A'.$currentRow.':M'.$currentRow );
    $currentRow += 2;

    // generate table head
    $theadLabel = array(
      0 => array(
        'label' => 'NO',
        'col'   => 'A',
        'merge' => 'A'.$currentRow.':A'.($currentRow+3)
      ),
      1 => array(
        'label' => 'RINCIAN KEGIATAN',
        'col'   => 'B',
        'merge' => 'B'.$currentRow.':D'.($currentRow+3)
      ),
      2 => array(
        'label' => 'JUMLAH KEMATIAN',
        'col'   => 'E',
        'merge' => 'E'.$currentRow.':M'.$currentRow
      ),
      3 => array(
        'label' => 'UPT PUSKESMAS PER PUSKESMAS',
        'col'   => 'E',
        'merge' => 'E'.($currentRow+1).':M'.($currentRow+1)
      ),
    );

    //var_dump($diagnosisData); exit();
    $prevName = '';

    for($i=0; $i<count($theadLabel); $i++) {
      if($i !== 3)
        $sheet->setCellValue( $theadLabel[$i]['col'].$currentRow, $theadLabel[$i]['label'] );
      else
        $sheet->setCellValue( $theadLabel[$i]['col'].($currentRow+1), $theadLabel[$i]['label'] );

      $sheet->mergeCells( $theadLabel[$i]['merge'] );

      if($i == 3) {
        $j = 0;

        foreach($diagnosisData as $data) {
          $nameStartCol = ord('E') + ($j*3);

          if( $prevName == '' || $prevName != $data['nama_puskesmas'] ) {
            $sheet->setCellValue( chr($nameStartCol).($currentRow+2), $data['nama_puskesmas'] );
            $prevName = $data['nama_puskesmas'];

            $sheet->setCellValue( chr($nameStartCol).($currentRow+3), 'n' );
            $sheet->setCellValue( chr($nameStartCol+1).($currentRow+3), 'kum' );
            $sheet->setCellValue( chr($nameStartCol+2).($currentRow+3), 'gakin' );
            $sheet->mergeCells( chr($nameStartCol).($currentRow+2).':'.chr($nameStartCol+2).($currentRow+2) );

            $j++;
          }
        }

        $sheet->setCellValue( chr($nameStartCol).($currentRow+2), 'TOTAL UPT PUSK.' );
        $sheet->setCellValue( chr($nameStartCol).($currentRow+3), 'n' );
        $sheet->setCellValue( chr($nameStartCol+1).($currentRow+3), 'kum' );
        $sheet->setCellValue( chr($nameStartCol+2).($currentRow+3), 'gakin' );
        $sheet->mergeCells( chr($nameStartCol).($currentRow+2).':'.chr($nameStartCol+2).($currentRow+2) );
      }
    }

    $currentRow = $sheet->getHighestRow() + 1;

    // generate table content
    $rowLabel = array(
      1 => array(
        'val' => 'Kematian Maternal',
        'sub' => array(
          'a' => 'Jumlah Kematian Ibu',
          'b' => array(
            'val' => 'Penyebab Kematian',
            1 => 'Pendarahan',
            2 => 'Hipertensi Dalam Kehamilan',
            3 => 'Infeksi',
            4 => 'Abortus',
            5 => 'Partus Lama',
            6 => 'Penyakit Penyerta',
            7 => 'Lain-lain'
          ),
          'c' => 'Jumlah Kematian Ibu Dilakukan Otopsi Verbal'
        )
      ),
      2 => array(
        'val' => 'Kematian Neonatus Dini (0-7 Hari)',
        'sub' => array(
          'a' => 'Jumlah Kematian Neonatus Dini',
          'b' => array(
            'val' => 'Penyebab Kematian',
            1 => 'BBLR',
            2 => 'Asfiksia',
            3 => 'Tetanus Neonatorum',
            4 => 'Prematur',
            5 => 'Infeksi',
            6 => 'Trauma Lahir',
            7 => 'Ikterus',
            8 => 'Kelainan Bawaan',
            9 => 'Masalah Laktasi',
            10 => 'Hematologi',
            11 => 'Lain-lain'
          ),
          'c' => 'Jumlah Kematian Neonatus Dini Dilakukan Otopsi Verbal'
        )
      ),
      3 => array(
        'val' => 'Kematian Neonatus Lanjut (8-28 Hari)',
        'sub' => array(
          'a' => 'Jumlah Kematian Neonatus Lanjut',
          'b' => array(
            'val' => 'Penyebab Kematian',
            1 => 'BBLR',
            2 => 'Pneumonia',
            3 => 'Tetanus Neonatorum',
            4 => 'Infeksi',
            5 => 'Masalah Laktasi',
            6 => 'Lain-lain'
          ),
          'c' => 'Jumlah Kematian Neonatus Lanjut Dilakukan Otopsi Verbal'
        )
      ),
      4 => array(
        'val' => 'Kematian Bayi (29 Hari - 11 Bulan)',
        'sub' => array(
          'a' => 'Jumlah Kematian Bayi',
          'b' => array(
            'val' => 'Penyebab Kematian',
            1 => 'Diare',
            2 => 'Pneumonia',
            3 => 'Meningitis / Ensephalitis',
            4 => 'Sepsis',
            5 => 'Kelainan Jantung',
            6 => 'Kelainan Pencernaan',
            7 => 'Ikterus',
            8 => 'Kelainan Bawaan',
            9 => 'DBD',
            10 => 'Tyfoid',
            11 => 'Campak',
            12 => 'Tetanus',
            13 => 'Kelainan Syaraf',
            14 => 'Lain-lain'
          ),
          'c' => 'Jumlah Kematian Bayi Dilakukan Otopsi Verbal'
        )
      ),
      5 => array(
        'val' => 'Kematian Balita (1-5 Tahun)',
        'sub' => array(
          'a' => 'Jumlah Kematian Balita',
          'b' => array(
            'val' => 'Penyebab Kematian',
            1 => 'Infeksi Saluran Nafas (Pneumonia)',
            2 => 'Asfiksia',
            3 => 'Diare',
            4 => 'Malaria',
            5 => 'Campak',
            6 => 'DBD',
            7 => 'Tyfoid',
            8 => 'Meningitis / Ensephalitis',
            9 => 'Sepsis',
            10 => 'Kelainan Bawaan',
            11 => 'Lain-lain'
          ),
          'c' => 'Jumlah Kematian Balita Dilakukan Otopsi Verbal'
        )
      )
    );
    $rowValue = array();
    $highestPeriod = 0;

    foreach($rowLabel as $k1=>$v1) {
      $sheet->setCellValue('A'.$currentRow, $k1);
      $sheet->setCellValue('B'.$currentRow, $v1['val']);
      $sheet->mergeCells('B'.$currentRow.':D'.$currentRow);

      $currentRow++;

      foreach($v1['sub'] as $k2=>$v2) {
        $sheet->setCellValue('B'.$currentRow, $k2);

        if( !is_array($v2) ) {
          $sheet->setCellValue('C'.$currentRow, $v2);
          $sheet->mergeCells('C'.$currentRow.':D'.$currentRow);

          $currentRow++;
        }
        else {
          $sheet->setCellValue('C'.$currentRow, $v2['val']);
          $sheet->mergeCells('C'.$currentRow.':D'.$currentRow);

          $currentRow++;

          for($i=1; $i<=count($v2)-1; $i++) {
            $sheet->setCellValue('C'.$currentRow, $i);
            $sheet->setCellValue('D'.$currentRow, $v2[$i]);

            $currentRow++;
          }
        }
      }

      $currentRow++;
    }

    for($i=0; $i<count($diagnosisData); $i++) {
      if( (int)$diagnosisData[$i]['ID_PERIODE'] > $highestPeriod )
        $highestPeriod = (int)$diagnosisData[$i]['ID_PERIODE'];

      if( $i == count($diagnosisData)-1 ) {
        foreach($diagnosisData as $v1) {
          $currentPuskesmas = $v1['nama_puskesmas'];

          if( !array_key_exists($v1['nama_puskesmas'], $rowValue) ) {
            $rowValue[$currentPuskesmas] = array(
              'n' => array(),
              'kum' => array()
            );
          }

          $valueCol = ($v1['ID_PERIODE'] == $highestPeriod) ? 'kum' : 'n';

          unset( $v1['nama_puskesmas'] );
          unset( $v1['ID_PUSKESMAS'] );
          unset( $v1['ID_PERIODE'] );

          $tempData = array_values($v1);
          $tempCount = count($tempData);

          for($j=0; $j<$tempCount; $j++) {
            if( $j == 0 && (empty($rowValue[$currentPuskesmas]['n']) || empty($rowValue[$currentPuskesmas]['kum'])) ) {
              for($k=0; $k<$tempCount; $k++) {
                $rowValue[$currentPuskesmas]['n'][$k] = 0;
                $rowValue[$currentPuskesmas]['kum'][$k] = 0;
              }
            }

            if( $valueCol == 'n' )
              $rowValue[$currentPuskesmas]['kum'][$j] += $tempData[$j];

            $rowValue[$currentPuskesmas][$valueCol][$j] += $tempData[$j];
          }
        }
      }
    }

    $initRowAdder = 15;
    $stepCol = 'E';
    $rowSkip = array( 16, 25, 28, 41, 44, 52, 55, 71, 74 );
    $rowValue['total'] = array( 'n' => array(), 'kum' => array() );

    foreach($rowValue as $k1=>$v1) {
      if($k1 !== 'total') {
        for($i=0; $i<count($v1['n']); $i++) {
          if( !array_key_exists($i, $rowValue['total']['n']) ) {
            $rowValue['total']['n'][$i] = 0;
            $rowValue['total']['kum'][$i] = 0;
          }

          $rowValue['total']['n'][$i] += $v1['n'][$i];
          $rowValue['total']['kum'][$i] += $v1['kum'][$i];

          if( in_array( $i+$initRowAdder, $rowSkip ) ) {
            if( $i+$initRowAdder == 25 || $i+$initRowAdder == 41 || $i+$initRowAdder == 52 || $i+$initRowAdder == 71 ) {
              $initRowAdder += 2;
            }
            else {
              $initRowAdder += 1;
            }
          }

          $sheet->setCellValue($stepCol . ($i + $initRowAdder), $v1['n'][$i]);
          $sheet->setCellValue(chr( ord($stepCol)+1 ).($i+$initRowAdder), $v1['kum'][$i]);
          $sheet->setCellValue(chr( ord($stepCol)+2 ).($i+$initRowAdder), '-');
        }

        $stepCol = chr( ord($stepCol)+3 );
        $initRowAdder = 15;
      }
    }

    for($j=0; $j<count($rowValue['total']['n']); $j++) {
      if( in_array( $j+$initRowAdder, $rowSkip ) ) {
        if( $j+$initRowAdder == 25 || $j+$initRowAdder == 41 || $j+$initRowAdder == 52 || $j+$initRowAdder == 71 ) {
          $initRowAdder += 2;
        }
        else {
          $initRowAdder += 1;
        }
      }

      $sheet->setCellValue( $stepCol.($j+$initRowAdder), $rowValue['total']['n'][$j] );
      $sheet->setCellValue( chr( ord($stepCol)+1 ).($j+$initRowAdder), $rowValue['total']['kum'][$j] );
      $sheet->setCellValue( chr( ord($stepCol)+2 ).($j+$initRowAdder), '-' );
    }

    // post sheet styling
    $alignment = $sheet->getStyle('A10:'.$sheet->getHighestColumn().'13')->getAlignment();
    $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $alignment->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    $alignment = $sheet->getStyle( 'A14:A'.$sheet->getHighestRow() )->getAlignment();
    $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $alignment->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    $alignment = $sheet->getStyle( 'B14:D'.$sheet->getHighestRow() )->getAlignment();
    $alignment->setWrapText(true);
    $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $alignment->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    $alignment = $sheet->getStyle( 'E14:'.$sheet->getHighestColumn().$sheet->getHighestRow() )->getAlignment();
    $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $alignment->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    $sheet->getColumnDimension('A')->setWidth(4);
    $sheet->getColumnDimension('B')->setWidth(4);
    $sheet->getColumnDimension('C')->setWidth(4);
    $sheet->getColumnDimension('D')->setWidth(30);

    for($char='E'; $char<=chr(ord($stepCol)+2); $char++) {
      $sheet->getColumnDimension($char)->setWidth(10);
    }

    //exit();

    // set what the name of the file to be downloaded
    $fileName = 'test_xls.xls';
    // set related headers and download the generated file
    // tell the browser about the xls mime-type
    header('Content-Type: application/vnd.ms-excel');
    // attach the generated xls file
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    // don't cache the file
    header('Cache-Control: max-age=0');
    // write XLS object and set to PHP output
    $objWriter = PHPExcel_IOFactory::createWriter($this->export_xls, 'Excel5');
    $objWriter->save('php://output');

    // and your download will start :)
  }

  /*private function in_array_loop($needle, $haystack, $strict=false) {
    foreach ($haystack as $item) {
      if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_loop($needle, $item, $strict))) {
        return true;
      }
    }

    return false;
  }*/

  private function make_kematian_stack($kia2Data, $endPeriodId) {
    $options = array(
      0 => array('Pendarahan', 'Hipertensi Dalam Kehamilan', 'Infeksi', 'Abortus', 'Partus Lama', 'Penyakit Penyerta', 'Lain-lain'),
      1 => array('BBLR', 'Asfiksia', 'Tetanus Neonatorum', 'Prematur', 'Infeksi', 'Trauma Lahir', 'Ikterus', 'Kelainan Bawaan', 'Masalah Laktasi', 'Hematologi', 'Lain-lain'),
      2 => array('BBLR', 'Pneumonia', 'Tetanus Neonatorum', 'Infeksi', 'Masalah Laktasi', 'Lain-lain'),
      3 => array('Diare', 'Pneumonia', 'Meningitis / Ensephalitis', 'Sepsis', 'Kelainan Jantung', 'Kelainan Pencernaan', 'Ikterus', 'Kelainan Bawaan', 'DBD', 'Tyfoid', 'Campak', 'Tetanus', 'Kelainan Syaraf', 'Lain-lain'),
      4 => array('Infeksi Saluran Nafas (Pneumonia)', 'Asfiksia', 'Diare', 'Malaria', 'Campak', 'DBD', 'Tyfoid', 'Meningitis / Ensephalitis', 'Sepsis', 'Kelainan Bawaan', 'Lain-lain')
    );
    $category = array(
      0 => array(
        'main'     => 'Kematian Maternal',
        'sub'      => array(
          'Jumlah Kematian Ibu'                         => null,
          'Penyebab Kematian'                           => $options[0],
          'Jumlah Kematian Ibu Dilakukan Otopsi Verbal' => null
        )
      ),
      1 => array(
        'main'     => 'Kematian Neonatus Dini (0-7 Hari)',
        'sub'      => array(
          'Jumlah Kematian Neonatus Dini'                         => null,
          'Penyebab Kematian'                                     => $options[1],
          'Jumlah Kematian Neonatus Dini Dilakukan Otopsi Verbal' => null
        )
      ),
      2 => array(
        'main'     => 'Kematian Neonatus Lanjut (8-28 Hari)',
        'sub'      => array(
          'Jumlah Kematian Neonatus Lanjut'                         => null,
          'Penyebab Kematian'                                       => $options[2],
          'Jumlah Kematian Neonatus Lanjut Dilakukan Otopsi Verbal' => null
        )
      ),
      3 => array(
        'main'     => 'Kematian Bayi (29 Hari - 11 Bulan)',
        'sub'      => array(
          'Jumlah Kematian Bayi'                         => null,
          'Penyebab Kematian'                            => $options[3],
          'Jumlah Kematian Bayi Dilakukan Otopsi Verbal' => null
        )
      ),
      4 => array(
        'main'     => 'Kematian Balita (1-5 Tahun)',
        'sub'      => array(
          'Jumlah Kematian Balita'                         => null,
          'Penyebab Kematian'                              => $options[4],
          'Jumlah Kematian Balita Dilakukan Otopsi Verbal' => null
        )
      )
    );
    $stack = array();
    $highestPeriodId = 0;

    for($k=0; $k<count($kia2Data); $k++) {
      if( (int)$kia2Data[$k]['ID_PERIODE'] > $highestPeriodId )
        $highestPeriodId = (int)$kia2Data[$k]['ID_PERIODE'];
    }

    foreach($kia2Data as $dataValue) {
      $currentPeriodIdCheck = $dataValue['ID_PERIODE'];

      if( !array_key_exists($dataValue['nama_puskesmas'], $stack) ) {
        $stack[ $dataValue['nama_puskesmas'] ] = array();

        for($i=0; $i<count($category); $i++) {
          foreach( $category[$i]['sub'] as $subKey=>$subValue ) {
            $stack[ $dataValue['nama_puskesmas'] ][$i][$subKey] = array();

            if( is_null($subValue) ) {
              $stack[ $dataValue['nama_puskesmas'] ][$i][$subKey] = array(
                'n'     => 0,
                'kum'   => 0,
                'gakin' => '-'
              );
            }
            else {
              for($j=0; $j<count($subValue); $j++) {
                $stack[ $dataValue['nama_puskesmas'] ][$i][$subKey][$subValue[$j]] = array(
                  'n'   => 0,
                  'kum' => 0,
                  'gakin'   => '-'
                );
              }
            }
          }
        }
      }

      for($j=0; $j<count($category); $j++) {
        $this->push_options_value($stack, $j, 'kum', $dataValue);

        if( (int)$currentPeriodIdCheck < $highestPeriodId )
          $this->push_options_value($stack, $j, 'n', $dataValue);
      }
    }

    $table = $this->prepare_table($stack, $category);

    return $table;
  }

  private function push_options_value(&$stack, $counter, $type, $data) {
    switch($counter) {
      case 0:
        foreach($stack[ $data['nama_puskesmas'] ][$counter] as $stackKey=>$stackValue) {
          if($stackKey == 'Jumlah Kematian Ibu') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey][$type] += $data['IBU_MATI_JML'];
          }
          elseif($stackKey == 'Penyebab Kematian') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Pendarahan'][$type] += $data['IBU_MATI_PENDARAHAN'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Hipertensi Dalam Kehamilan'][$type] += $data['IBU_MATI_HIPERTENSI'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Infeksi'][$type] += $data['IBU_MATI_INFEKSI'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Abortus'][$type] += $data['IBU_MATI_ABORTUS'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Partus Lama'][$type] += $data['IBU_MATI_PARTUS_LAMA'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Penyakit Penyerta'][$type] += $data['IBU_MATI_PENYAKIT_PENYERTA'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Lain-lain'][$type] += $data['IBU_MATI_LAIN_LAIN'];
          }
          elseif($stackKey == 'Jumlah Kematian Ibu Dilakukan Otopsi Verbal') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey][$type] += $data['IBU_MATI_OTOPSI_VERBAL'];
          }
        }
      break;

      case 1:
        foreach($stack[ $data['nama_puskesmas'] ][$counter] as $stackKey=>$stackValue) {
          if($stackKey == 'Jumlah Kematian Neonatus Dini') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey][$type] += $data['NEOTATUS_MATI_DINI_JML'];
          }
          elseif($stackKey == 'Penyebab Kematian') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['BBLR'][$type] += $data['NEOTATUS_MATI_DINI_BBLR'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Asfiksia'][$type] += $data['NEOTATUS_MATI_DINI_ASFIKSIA'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Tetanus Neonatorum'][$type] += $data['NEOTATUS_MATI_DINI_TETANUS_NEO'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Prematur'][$type] += $data['NEOTATUS_MATI_DINI_PREMATUR'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Infeksi'][$type] += $data['NEOTATUS_MATI_DINI_INFEKSI'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Trauma Lahir'][$type] += $data['NEOTATUS_MATI_DINI_TRAUMA_LAHIR'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Ikterus'][$type] += $data['NEOTATUS_MATI_DINI_IKTERUS'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Kelainan Bawaan'][$type] += $data['NEOTATUS_MATI_DINI_KELAINAN_BAWAAN'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Masalah Laktasi'][$type] += $data['NEOTATUS_MATI_DINI_LAKTASI'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Hematologi'][$type] += $data['NEOTATUS_MATI_DINI_HEMATOLOGI'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Lain-lain'][$type] += $data['NEOTATUS_MATI_DINI_LAIN'];
          }
          elseif($stackKey == 'Jumlah Kematian Neonatus Dini Dilakukan Otopsi Verbal') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey][$type] += $data['NEOTATUS_MATI_DINI_OTOPSI_VERBAL'];
          }
        }
      break;

      case 2:
        foreach($stack[ $data['nama_puskesmas'] ][$counter] as $stackKey=>$stackValue) {
          if($stackKey == 'Jumlah Kematian Neonatus Lanjut') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey][$type] += $data['NEOTATUS_MATI_LANJUT_JML'];
          }
          elseif($stackKey == 'Penyebab Kematian') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['BBLR'][$type] += $data['NEOTATUS_MATI_LANJUT_BBLR'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Pneumonia'][$type] += $data['NEOTATUS_MATI_LANJUT_PNEUMONIA'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Tetanus Neonatorum'][$type] += $data['NEOTATUS_MATI_LANJUT_TETANUS_NEO'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Infeksi'][$type] += $data['NEOTATUS_MATI_LANJUT_INFEKSI'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Masalah Laktasi'][$type] += $data['NEOTATUS_MATI_LANJUT_LAKTASI'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Lain-lain'][$type] += $data['NEOTATUS_MATI_LANJUT_LAIN'];
          }
          elseif($stackKey == 'Jumlah Kematian Neonatus Lanjut Dilakukan Otopsi Verbal') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey][$type] += $data['NEOTATUS_MATI_LANJUT_OTOPSI_VERBAL'];
          }
        }
      break;

      case 3:
        foreach($stack[ $data['nama_puskesmas'] ][$counter] as $stackKey=>$stackValue) {
          if($stackKey == 'Jumlah Kematian Bayi') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey][$type] += $data['BAYI_MATI_JML'];
          }
          elseif($stackKey == 'Penyebab Kematian') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Diare'][$type] += $data['BAYI_MATI_DIARE'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Pneumonia'][$type] += $data['BAYI_MATI_PNEUMONIA'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Meningitis / Ensephalitis'][$type] += $data['BAYI_MATI_MENINGITIS_ENSE'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Sepsis'][$type] += $data['BAYI_MATI_SEPSIS'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Kelainan Jantung'][$type] += $data['BAYI_MATI_KELAINAN_JANTUNG'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Kelainan Pencernaan'][$type] += $data['BAYI_MATI_KELAINAN_PENCERNAAN'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Ikterus'][$type] += $data['BAYI_MATI_IKTERUS'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Kelainan Bawaan'][$type] += $data['BAYI_MATI_KELAINAN_BAWAAN'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['DBD'][$type] += $data['BAYI_MATI_DBD'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Tyfoid'][$type] += $data['BAYI_MATI_TYFOID'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Campak'][$type] += $data['BAYI_MATI_CAMPAK'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Tetanus'][$type] += $data['BAYI_MATI_TETANUS'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Kelainan Syaraf'][$type] += $data['BAYI_MATI_KELAINAN_SYARAF'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Lain-lain'][$type] += $data['BAYI_MATI_LAIN'];
          }
          elseif($stackKey == 'Jumlah Kematian Bayi Dilakukan Otopsi Verbal') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey][$type] += $data['BAYI_MATI_OTOPSI_VERBAL'];
          }
        }
      break;

      case 4:
        foreach($stack[ $data['nama_puskesmas'] ][$counter] as $stackKey=>$stackValue) {
          if($stackKey == 'Jumlah Kematian Balita') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey][$type] += $data['BALITA_MATI_JML'];
          }
          elseif($stackKey == 'Penyebab Kematian') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Infeksi Saluran Nafas (Pneumonia)'][$type] += $data['BALITA_MATI_PNEUMONIA'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Asfiksia'][$type] += $data['BALITA_MATI_ASFIKSIA'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Diare'][$type] += $data['BALITA_MATI_DIARE'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Malaria'][$type] += $data['BALITA_MATI_MALARIA'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Campak'][$type] += $data['BALITA_MATI_CAMPAK'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['DBD'][$type] += $data['BALITA_MATI_DBD'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Tyfoid'][$type] += $data['BALITA_MATI_TYFOID'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Meningitis / Ensephalitis'][$type] += $data['BALITA_MATI_MENINGITIS_ENSE'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Sepsis'][$type] += $data['BALITA_MATI_SEPSIS'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Kelainan Bawaan'][$type] += $data['BALITA_MATI_KELAINAN_BAWAAN'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Lain-lain'][$type] += $data['BALITA_MATI_KELAINAN_BAWAAN'];
          }
          elseif($stackKey == 'Jumlah Kematian Balita Dilakukan Otopsi Verbal') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey][$type] += $data['BALITA_MATI_OTOPSI_VERBAL'];
          }
        }
      break;
    }
  }

  private function prepare_table($stack, $category) {
    $preMarkup = '';
    $puskesmas = array_keys($stack);
    $rowNumCounter = 1;

    $headMarkup = '<thead><tr class="table_head"><th class="text-center middle-aligned" rowspan="2">No.</th><th class="text-center middle-aligned" rowspan="2">Rincian Kegiatan</th>';
    for($i=0; $i<count($puskesmas); $i++) {
      $headMarkup .= '<th class="text-center middle-aligned" colspan="3">'.$puskesmas[$i].'</th>';

      if($i == count($puskesmas) - 1) {
        $headMarkup .= '<th class="text-center middle-aligned" colspan="3">TOTAL UPT PUSKESMAS</th>';
      }
    }
    $headMarkup .= '</tr><tr class="table-head">';
    for($i=0; $i<=count($puskesmas); $i++) {
      $headMarkup .= '<th class="text-center middle-aligned">Total <em>n</em> Periode Lalu</th><th class="text-center middle-aligned">Nilai Kumulatif</th><th class="text-center middle-aligned">Gakin</th>';
    }
    $headMarkup .= '</tr></thead>';
    $preMarkup .= '<tbody>';

    while($rowNumCounter <= 5) {
      $preMarkup .= '<tr>';
      $preMarkup .= '<td>'.$rowNumCounter.'.</td><td>'.$category[$rowNumCounter-1]['main'].'</td>';
      $preMarkup .= '<td colspan="'.(count($puskesmas)*3 + 3).'"></td></tr>';
      $postMarkup = '';
      $listPoint = 'A';

      foreach($category[$rowNumCounter-1]['sub'] as $key=>$value) {
        $postMarkup .= '<tr><td></td><td>'.$listPoint.'.&emsp;'.$key.'</td>';

        if( is_null($value) ) {
          $tempN = 0;
          $tempKum = 0;

          foreach($puskesmas as $name) {
            $postMarkup .= '<td>'.$stack[$name][$rowNumCounter-1][$key]['n'].'</td>';
            $postMarkup .= '<td>'.$stack[$name][$rowNumCounter-1][$key]['kum'].'</td>';
            $postMarkup .= '<td>'.$stack[$name][$rowNumCounter-1][$key]['gakin'].'</td>';

            $tempN += (int)$stack[$name][$rowNumCounter-1][$key]['n'];
            $tempKum += (int)$stack[$name][$rowNumCounter-1][$key]['kum'];
          }

          $postMarkup .= '<td>'.$tempN.'</td>';
          $postMarkup .= '<td>'.$tempKum.'</td>';
          $postMarkup .= '<td>-</td>';
        }
        else {
          $postMarkup .= '<td colspan="'.(count($puskesmas)*3 + 3).'"></td></tr>';

          foreach ($category[$rowNumCounter-1]['sub'][$key] as $subValue) {
            $tempN = 0;
            $tempKum = 0;
            $postMarkup .= '<tr><td></td><td>&emsp;&emsp;•&emsp;'.$subValue.'</td>';

            foreach($puskesmas as $name) {
              $postMarkup .= '<td>'.$stack[$name][$rowNumCounter-1][$key][$subValue]['n'].'</td>';
              $postMarkup .= '<td>'.$stack[$name][$rowNumCounter-1][$key][$subValue]['kum'].'</td>';
              $postMarkup .= '<td>'.$stack[$name][$rowNumCounter-1][$key][$subValue]['gakin'].'</td>';

              $tempN += (int)$stack[$name][$rowNumCounter-1][$key][$subValue]['n'];
              $tempKum += (int)$stack[$name][$rowNumCounter-1][$key][$subValue]['kum'];
            }

            $postMarkup .= '<td>'.$tempN.'</td>';
            $postMarkup .= '<td>'.$tempKum.'</td>';
            $postMarkup .= '<td>-</td>';
          }
        }

        $listPoint++;
      }

      $preMarkup .= $postMarkup.'</tr>';
      $rowNumCounter++;
    }

    $preMarkup .= '</tbody>';
    $fullRowMarkup = $headMarkup.$preMarkup;

    return $fullRowMarkup;
  }
}
