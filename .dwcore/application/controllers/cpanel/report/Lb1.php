<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

class Lb1 extends MY_Controller {
  public function __construct() {
    parent::__construct();

    $this->load->model('report_model');
    $this->load->library('report');
    $this->load->helper('form');

    $this->report->set_report_markup_data('page_title', 'Laporan Bulanan');
  }

  public function index() {
    $this->report->set_report_model_data([
      'years' => $this->report_model->fetch_year(),
      'kecamatan' => $this->report_model->fetch_kecamatan()
    ]);
    $this->report->lb1_diseases_data( $this->report_model->fetch_diseases() );

    $this->set_view_data( $this->report->get_report_data() );

    $this->render();
  }

  public function filter() {
    $nulledFound = FALSE;
    $valuePosted = array(
      'period_start'   => array(
        'year'  => $this->input->post('f_period_start[0]', TRUE),
        'month' => $this->input->post('f_period_start[1]', TRUE)
      ),
      'period_end'     => array(
        'year'  => $this->input->post('f_period_end[0]', TRUE),
        'month' => $this->input->post('f_period_end[1]', TRUE)
      ),
      'kecamatan_code' => $this->input->post('f_kecamatan', TRUE),
      'puskesmas_code' => $this->input->post('f_puskesmas', TRUE),
      'disease_id'     => $this->input->post('f_disease', TRUE)
    );

    foreach($valuePosted as $key=>$value) {
      if( is_array($valuePosted[$key]) ) {
        foreach($value as $subkey=>$subvalue) {
          if($key == 'period_end' && is_null($subvalue) && $valuePosted['period_start']['month'] == 'all' )
            continue;

          if( is_null($subvalue) ) {
            $nulledFound = TRUE;
            break;
          }
        }
      }
      else {
        if( is_null($value) ) {
          if( $key == 'disease_id' || ( $key == 'puskesmas_code' && $valuePosted['kecamatan_code'] == 'all' ) )
            continue;

          $nulledFound = TRUE;
          break;
        }
      }

      if( $nulledFound ) break;
    }

    if( $nulledFound ) {
      $this->session->set_flashdata('errNulledFilter', TRUE);
      redirect( base_url('report/bulanan') );
    }
    else {
      if($valuePosted['period_start']['month'] != 'all') {
        $periodId = array(
          // param 1: year, param 2: month number
          'start' => $this->report_model->get_period_id($valuePosted['period_start']['year'], $valuePosted['period_start']['month']),
          'end'   => $this->report_model->get_period_id($valuePosted['period_end']['year'], $valuePosted['period_end']['month'])
        );
      }
      else {
        $periodId = $this->report_model->get_period_id($valuePosted['period_start']['year'], 'all');
        $periodId = array(
          'start' => $periodId[0]['id'],
          'end' => $periodId[count($periodId)-1]['id']
        );
      }

      if( $valuePosted['kecamatan_code'] != 'all' ) {
        if( $valuePosted['puskesmas_code'] != 'all' ) {
          $puskesmasId = $this->report_model->get_puskesmas_id( $valuePosted['puskesmas_code'] );
        }
        else {
          $puskesmasId = $this->report_model->fetch_related_puskesmas_on_kecamatan( $valuePosted['kecamatan_code'] );
        }
      }
      else {
        $puskesmasId = $this->report_model->get_all_puskesmas_id();
        //var_dump($puskesmasId); exit();
      }

      if( !is_null($valuePosted['disease_id']) && !empty($valuePosted['disease_id']) ) {
        $newDiseaseId = array();

        foreach($valuePosted['disease_id'] as $id) {
          $diseaseCodes = explode('.', $this->report_model->fetch_diseases_code($id, 'id') );

          for($i=0; $i<count($diseaseCodes)-1; $i++) {
            if($i == 0)
              $newIdCandidate = $this->report_model->fetch_diseases($diseaseCodes[$i]);
            elseif($i == 1)
              $newIdCandidate = $this->report_model->fetch_diseases($diseaseCodes[0].'.'.$diseaseCodes[$i]);

            $newIdCandidate = $newIdCandidate[0]['id'];

            if( !$this->report->recurse_array_search($newIdCandidate, $newDiseaseId) )
              $newDiseaseId[] = $newIdCandidate;
          }

          $newDiseaseId[] = $id;
        }

        $valuePosted['disease_id'] = $newDiseaseId;
      }

      $diagnosis = $this->report_model->fetch_diagnosis($valuePosted, $periodId, $puskesmasId);
      $monthString = array(1=>'Januari', 2=>'Februari', 3=>'Maret', 4=>'April', 5=>'Mei', 6=>'Juni', 7=>'Juli', 8=>'Agustus', 9=>'September', 10=>'Oktober', 11=>'November', 12=>'Desember');

      if( $valuePosted['period_start']['month'] !== 'all' ) {
        $valuePosted['period_start'] = $monthString[ $valuePosted['period_start']['month'] ].' '.$valuePosted['period_start']['year'];
        $valuePosted['period_end'] = $monthString[ $valuePosted['period_end']['month'] ].' '.$valuePosted['period_end']['year'];
      }
      else {
        $year = $valuePosted['period_start']['year'];
        $valuePosted['period_start'] = $monthString[1].' '.$year;
        $valuePosted['period_end'] = $monthString[12].' '.$year;
      }

      if( $valuePosted['kecamatan_code'] != 'all' ) {
        $valuePosted['kecamatan_name'] = $this->report_model->get_kecamatan_name( $valuePosted['kecamatan_code'] );
        $valuePosted['puskesmas_name'] = $this->report_model->get_puskesmas_name( $puskesmasId );

        if( is_array($valuePosted['puskesmas_name']) ) {
          $tempPuskesmasName = '';

          for($i=0; $i<count( $valuePosted['puskesmas_name'] ); $i++) {
            $tempPuskesmasName .= $valuePosted['puskesmas_name'][$i]['nama_puskesmas'];

            if($i !== count( $valuePosted['puskesmas_name'] )-1)
              $tempPuskesmasName .= ', ';
          }

          $valuePosted['puskesmas_name'] = $tempPuskesmasName;
        }

        $valuePosted['upt_name'] = ( substr($valuePosted['puskesmas_name'], 0, 3) == 'UPT' ) ? $valuePosted['puskesmas_name'] : $this->report_model->get_upt_from_kecamatan( $valuePosted['kecamatan_code'] );
      }
      else {
        $valuePosted['kecamatan_name'] = 'Semua Kecamatan';
        $valuePosted['puskesmas_name'] = '-';
        $valuePosted['upt_name'] = '-';
      }

      $this->set_view_data( $this->report->get_report_data() );
      $this->set_view_data('diagnosis_table', $this->make_diagnosis_stack($diagnosis), 'markup');
      $this->set_view_data('diagnosis_data', json_encode($diagnosis), 'model');
      $this->set_view_data('report_spec', $valuePosted, 'model');

      $this->render();
    }
  }

  public function export() {
    $this->load->library('export_xls');
    // set active sheet
    $this->export_xls->setActiveSheetIndex(0);
    $sheet = $this->export_xls->getActiveSheet();

    $diagnosisData = json_decode( $this->input->post('xls_diagnosis_json', TRUE), TRUE );
    $specificationData = json_decode( $this->input->post('xls_specification_json', TRUE), TRUE );
    $currentRow = 1;

    // set sheet title
    $sheet->setTitle('Sheet 1');

    // generate the top section of report
    $sheet->setCellValueByColumnAndRow('A', $currentRow, 'LAPORAN BULANAN PENYAKIT (LB1)');
    $sheet->mergeCells('A1:M1');
    $sheet->insertNewRowBefore($currentRow+1);

    $currentRow = $sheet->getHighestRow() + 1;

    for($i=0; $i<4; $i++) {
      $label = '';
      $value = '';

      if($i == 0) {
        $label = 'Tahun';
        $value = substr( $specificationData['period_start'], strlen( $specificationData['period_start'] ) - 4, strlen( $specificationData['period_start'] ) ).' - '.substr( $specificationData['period_end'], strlen( $specificationData['period_end'] ) - 4, strlen( $specificationData['period_end'] ) );
      }
      elseif($i == 1) {
        $label = 'Periode';
        $value = $specificationData['period_start'].' - '.$specificationData['period_end'];
      }
      elseif($i == 2) {
        $label = 'Puskesmas';
        $value = $specificationData['puskesmas_name'];
      }
      elseif($i == 3) {
        $label = 'UPT Puskesmas';
        $value = $specificationData['upt_name'];
      }

      $sheet->setCellValue('A'.$currentRow, $label);
      $sheet->setCellValue('D'.$currentRow, $value);
      $sheet->mergeCells("A{$currentRow}:B{$currentRow}");
      $sheet->mergeCells("D{$currentRow}:G{$currentRow}");

      $currentRow++;
    }

    // generate table head
    $sheet->setCellValueByColumnAndRow('A', $currentRow, 'LAPORAN BULANAN PENYAKIT (LB1)');
    $sheet->mergeCells( 'A'.$currentRow.':AI'.($currentRow+1) );

    $currentRow = $sheet->getHighestRow() + 1;

    $sheet->setCellValueByColumnAndRow('A', $currentRow, 'LAPORAN BULANAN PENYAKIT (LB1)');
    $currentRow++;

    $theadLabel = array(
      0 => array(
        'label' => 'NO URUT',
        'col'   => 'A',
        'merge' => 'A'.$currentRow.':A'.($currentRow+2)
      ),
      1 => array(
        'label' => 'KODE ICD-10',
        'col'   => 'B',
        'merge' => 'B'.$currentRow.':B'.($currentRow+2)
      ),
      2 => array(
        'label' => 'JENIS PENYAKIT',
        'col'   => 'C',
        'merge' => 'C'.$currentRow.':C'.($currentRow+2)
      ),
      3 => array(
        'label' => 'JUMLAH KASUS BARU MENURUT GOLONGAN UMUR',
        'col'   => 'D',
        'merge' => 'D'.$currentRow.':AA'.$currentRow
      ),
      4 => array(
        'label' => 'KASUS BARU',
        'col'   => 'AB',
        'merge' => 'AB'.$currentRow.':AD'.($currentRow+1)
      ),
      5 => array(
        'label' => 'KASUS LAMA',
        'col'   => 'AE',
        'merge' => 'AE'.$currentRow.':AG'.($currentRow+1)
      ),
      6 => array(
        'label' => 'TOTAL KASUS',
        'col'   => 'AH',
        'merge' => 'AH'.$currentRow.':AH'.($currentRow+2)
      ),
      7 => array(
        'label' => 'PESERTA GAKIN',
        'col'   => 'AI',
        'merge' => 'AI'.$currentRow.':AI'.($currentRow+2)
      )
    );
    $theadSubLabel = array('0-7hr', '8-28hr', '29hr-1th', '1-4th', '5-9th', '10-14th', '15-19th', '20-44th', '45-54th', '55-59th', '60-69th', '>=70th');

    for($i=0; $i<count($theadLabel); $i++) {
      $sheet->setCellValue( $theadLabel[$i]['col'].$currentRow, $theadLabel[$i]['label'] );
      $sheet->mergeCells( $theadLabel[$i]['merge'] );

      if($i == 3) {
        $newOrdinalCol = ord( $theadLabel[$i]['col'] );
        $newRow = $currentRow + 1;

        for($j=0; $j<count($theadSubLabel); $j++) {
          if($newOrdinalCol > 65 && $newOrdinalCol < 90) {
            $newColStart = chr($newOrdinalCol);
            $newColEnd = chr($newOrdinalCol+1);
          }
          else {
            $newColStart = chr($newOrdinalCol);
            $newOrdinalCol = 65;
            $newColEnd = 'A'.chr($newOrdinalCol);
          }

          $colMergeRange = $newColStart.$newRow.':'.$newColEnd.$newRow;
          $colSubMale = $newColStart.($newRow+1);
          $colSubFemale = $newColEnd.($newRow+1);

          $sheet->setCellValue( $newColStart.$newRow, $theadSubLabel[$j] );
          $sheet->mergeCells($colMergeRange);
          $sheet->setCellValue($colSubMale, 'L');
          $sheet->setCellValue($colSubFemale, 'P');

          $newOrdinalCol += 2;
        }
      }
      elseif($i == 4 || $i == 5) {
        $newCol = $theadLabel[$i]['col'];
        $newRow = $currentRow + 2;

        for($k=0; $k<3; $k++) {
          $subSubLabel = ($k == 0) ? 'L' : ( ($k == 1) ? 'P' : 'JML' );
          $sheet->setCellValue( $newCol.$newRow, $subSubLabel );
          $newCol++;
        }
      }
    }

    $currentRow = $sheet->getHighestRow() + 1;

    // generate table content
    $counter = 1;

    foreach($diagnosisData['inti'] as $k1=>$v1) {
      $icdMain = $this->report_model->get_icd_code($k1, 'main');

      $sheet->setCellValue( 'B'.$currentRow, $icdMain );
      $sheet->setCellValue( 'C'.$currentRow, $v1 );

      $currentRow++;

      foreach($diagnosisData['sub'] as $k2=>$v2) {
        if($k2 == $k1) {
          $subKeys = array_keys($v2);
          $icdSub = array();

          for($i=0; $i<count($subKeys); $i++) {
            $subDetails = $this->report_model->get_icd_code($k2.'.'.$subKeys[$i], 'sub');
            $icdSub[$i] = $subDetails[0];

            if( $icdSub[$i]['kategori2'] != '-' ) {
              $sheet->setCellValue( 'B'.$currentRow, $icdSub[$i]['kode_icd9'] );
              $sheet->setCellValue( 'C'.$currentRow, $v2[ $subKeys[$i] ] );

              $currentRow++;
            }

            foreach($diagnosisData['kumulasi'][$k1] as $k3=>$v3) {
              if( $k3 == $subKeys[$i] ) {
                $icdDisease = array_keys($v3);

                for($j=0; $j<count($icdDisease); $j++) {
                  $diagnosisDetail = array_values( $v3[ $icdDisease[$j] ] );

                  $sheet->setCellValue( 'A'.$currentRow, $counter );
                  $sheet->setCellValue( 'B'.$currentRow, $icdDisease[$j] );

                  $colPointer = 'C';
                  for($k=0; $k<count($diagnosisDetail); $k++) {
                    $sheet->setCellValue( $colPointer.$currentRow, $diagnosisDetail[$k] );
                    $colPointer++;
                  }

                  $counter++;
                  $currentRow++;
                }
              }
            }
          }
        }
      }
    }

    // post sheet styling
    $alignment = $sheet->getStyle( 'A7:'.$sheet->getHighestColumn().$sheet->getHighestRow() )
      ->getAlignment();

    $alignment->setWrapText(true);
    $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $alignment->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    $alignment = $sheet->getStyle( 'C12:C'.$sheet->getHighestRow() )->getAlignment();

    $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

    $sheet->getColumnDimension('B')->setWidth(8);
    $sheet->getColumnDimension('C')->setWidth(30);

    // set what the name of the file to be downloaded
    $fileName = 'test_xls.xls';
    // set related headers and download the generated file
    // tell the browser about the xls mime-type
    header('Content-Type: application/vnd.ms-excel');
    // attach the generated xls file
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    // don't cache the file
    header('Cache-Control: max-age=0');
    // write XLS object and set to PHP output
    $objWriter = PHPExcel_IOFactory::createWriter($this->export_xls, 'Excel5');
    $objWriter->save('php://output');

    // and your download will start :)
  }

  /*private function in_array_loop($needle, $haystack, $strict=false) {
    foreach ($haystack as $item) {
      if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_loop($needle, $item, $strict))) {
        return true;
      }
    }

    return false;
  }*/

  private function prepare_table($rawData) {
    $tempMarkup = '';
    $fullRowMarkup = '';
    $rowNumCounter = 1;

    foreach( array_keys($rawData['inti']) as $mainKeys ) {
      if( (int)$mainKeys === 21 ) { continue; }

      $spanning = array( 'main' => 0, 'sub' => 0, 'disease' => 0 );
      $fullRowMarkup .= '<tr><td style="border-right: 0; background-color: #fff;"></td><td colspan="36" style="padding: 17px 8px; background-color: #fff; border-left: 0">'.$rawData['inti'][$mainKeys].'</td></tr>';

      foreach( $rawData['sub'][$mainKeys] as $subKeys=>$subValue ) {
        $iterationCounter = 0;

        foreach($rawData['kumulasi'][$mainKeys][$subKeys] as $diseaseCode=>$cumulativeValue) {
          if($iterationCounter > 0) $tempMarkup .= '<tr>';

          $tempMarkup .= '<td class="middle-aligned">'.$rowNumCounter.'. </td><td class="middle-aligned">'.$diseaseCode.'</td>';

          foreach($cumulativeValue as $data) {
            $colTag = ( is_numeric($data) ) ? '<td class="text-right middle-aligned">' : '<td class="middle-aligned">';
            $tempMarkup .= ( is_numeric($data) ) ? $colTag.number_format($data, 0, ",", ".").'</td>' : $colTag.$data.'</td>';
          }

          $tempMarkup .= '</tr>';
          $spanning['sub']++;
          $iterationCounter++;
          $rowNumCounter++;
        }

        $fullRowMarkup .= '<tr><td class="middle-aligned" rowspan="'.$spanning['sub'].'" style="background-color: #fff;">'.$subValue.'</td>'.$tempMarkup;
        $tempMarkup = '';
        $spanning['sub'] = 0;
      }
    }

    return $fullRowMarkup;
  }

  private function make_diagnosis_stack(&$diagnosisData) {
    //$table = '';
    $tableMeta = array(
      'inti'      => array(),
      'sub'       => array(),
      'kumulasi'  => array()
    );
    $tempPuskesmas = array();
    $tempStack = array();

    foreach($diagnosisData as $value) {
      $puskesmas = $value['nama_puskesmas'];
      if( $foundKey = array_key_exists($puskesmas, $tempPuskesmas) ) {
        $tempPuskesmas[$puskesmas] += 1;
      }
      else{
        $tempPuskesmas[$puskesmas] = 1;
      }
    }

    $puskesmasIterator = 0;
    foreach( array_keys($tempPuskesmas) as $key ) {
      $tempStack[]['puskesmas'] = $key;

      foreach($diagnosisData as $value) {
        if( in_array($key, $value) && $key == $value['nama_puskesmas'] ) {
          if( !array_key_exists( 'penyakit', $tempStack[$puskesmasIterator] ) ) {
            $tempStack[$puskesmasIterator]['penyakit'] = array();
          }

          if( !$this->report->recurse_array_search( $value['nama_bulan'].' '.$value['tahun'], $tempStack[$puskesmasIterator] ) ) {
            $tempStack[$puskesmasIterator]['penyakit'][] = array('periode' => $value['nama_bulan'].' '.$value['tahun']);
          }

          $codeChunks = explode('.', $value['kode_penyakit']);
          $chunksFrags = count($codeChunks);

          for($i=0; $i<count($tempStack[$puskesmasIterator]['penyakit']); $i++) {
            $returnIteration = true;

            if( $value['nama_bulan'].' '.$value['tahun'] == $tempStack[$puskesmasIterator]['penyakit'][$i]['periode'] ) {
              foreach( $tempStack[$puskesmasIterator]['penyakit'] as $main ) {
                if( $chunksFrags == 1 ) {
                  if( !$this->report->recurse_array_search( $value['nama_penyakit'], $main ) ) {
                    $tempStack[$puskesmasIterator]['penyakit'][$i][ $codeChunks[0] ] = array( 'inti' => ucwords( strtolower( $value['nama_penyakit'] ) ) );

                    if( !in_array( ucwords( strtolower( $value['nama_penyakit'] ) ), $tableMeta['inti'] ) ) {
                      $tableMeta['inti'][ $codeChunks[0] ] = ucwords( strtolower( $value['nama_penyakit'] ) );
                    }

                    $returnIteration = false;
                    break;
                  }
                }
                elseif( $chunksFrags == 2 ) {
                  if( !$this->report->recurse_array_search( $value['nama_penyakit'], $main ) ) {
                    $tempStack[$puskesmasIterator]['penyakit'][$i][ $codeChunks[0] ][ $codeChunks[1] ] = array('sub' => ucwords( strtolower( $value['nama_penyakit'] ) ) );

                    if( !in_array( ucwords( strtolower( $value['nama_penyakit'] ) ), $tableMeta['inti'] ) ) {
                      $tableMeta['sub'][ $codeChunks[0] ][ $codeChunks[1] ] = ucwords( strtolower( $value['nama_penyakit'] ) );
                    }

                    if( $value['kategori2'] == '-' ) {
                      $tempStack[$puskesmasIterator]['penyakit'][$i][ $codeChunks[0] ][ $codeChunks[1] ][] = $this->make_disease_properties($value);

                      if( !$this->report->recurse_array_search( $value['kode_icd9'], $tableMeta['kumulasi'] ) ) {
                        $tableMeta['kumulasi'][ $codeChunks[0] ][ $codeChunks[1] ][ $value['kode_icd9'] ] = array();
                      }

                      $this->calc_cumulative_cases( $value, $tableMeta['kumulasi'][ $codeChunks[0] ][ $codeChunks[1] ][ $value['kode_icd9'] ] );
                    }

                    $returnIteration = false;
                    break;
                  }
                }
                elseif( $chunksFrags == 3 ) {
                  if( !$this->report->recurse_array_search( $value['nama_penyakit'], $main ) ) {
                    $tempStack[$puskesmasIterator]['penyakit'][$i][ $codeChunks[0] ][ $codeChunks[1] ][] = $this->make_disease_properties($value);

                    if( !$this->report->recurse_array_search( $value['kode_icd9'], $tableMeta['kumulasi'] ) ) {
                      $tableMeta['kumulasi'][ $codeChunks[0] ][ $codeChunks[1] ][ $value['kode_icd9'] ] = array();
                    }

                    $this->calc_cumulative_cases( $value, $tableMeta['kumulasi'][ $codeChunks[0] ][ $codeChunks[1] ][ $value['kode_icd9'] ] );
                    $returnIteration = false;
                    break;
                  }
                }
              }
            }

            if(!$returnIteration) { break; }
          }
        }
      }

      $puskesmasIterator++;
    }
    //var_dump( $tableMeta ); exit();
    $diagnosisData = $tableMeta;
    $table = $this->prepare_table($diagnosisData);

    return $table;
  }

  private function make_disease_properties($data) {
    $properties = array(
      'kode' => $data['kode_icd9'],
      'nama' => ucwords( $data['nama_penyakit'] ),
      'l' => $this->push_gender_cases('l', $data),
      'p' => $this->push_gender_cases('p', $data),
      'jumlah_kasus' => array(
        'baru' => $data['kasus_baru_jml'],
        'lama' => $data['kasus_lama_jml'],
        'total' => $data['total_kasus'],
      ),
      'peserta_gakin' => $data['peserta_gakin']
    );

    return $properties;
  }

  private function push_gender_cases($genderAbbr, $data) {
    $genderKeysPrefix = array('0-7hr', '8-28hr', '29hr-1th', '1-4th', '5-9th', '10-14th', '15-19th', '20-44th', '45-54th', '55-59th', '60-69th', '>-70th', 'kasus_baru', 'kasus_lama');
    $cases = array();

    for($j=0; $j<count($genderKeysPrefix); $j++) {
      $cases[ $genderKeysPrefix[$j] ] = $data[ $genderKeysPrefix[$j].'_'.$genderAbbr ];
    }

    return $cases;
  }

  private function calc_cumulative_cases($data, &$target) {
    $genderKeysPrefix = array('0-7hr', '8-28hr', '29hr-1th', '1-4th', '5-9th', '10-14th', '15-19th', '20-44th', '45-54th', '55-59th', '60-69th', '>-70th', 'kasus_baru', 'kasus_baru_jml', 'kasus_lama', 'kasus_lama_jml');

    if( empty($target) ) {
      $target['nama'] = ucwords( $data['nama_penyakit'] );

      for($i=0; $i<count($genderKeysPrefix); $i++) {
        if( $genderKeysPrefix[$i] !== 'kasus_baru_jml' && $genderKeysPrefix[$i] !== 'kasus_lama_jml' ) {
          $target[ $genderKeysPrefix[$i].'_l' ] = (int)$data[ $genderKeysPrefix[$i].'_l' ];
          $target[ $genderKeysPrefix[$i].'_p' ] = (int)$data[ $genderKeysPrefix[$i].'_p' ];
        }
        else {
          if( $genderKeysPrefix[$i] === 'kasus_baru_jml' )
            $target[ $genderKeysPrefix[$i] ] = (int)$data['kasus_baru_jml'];
          elseif( $genderKeysPrefix[$i] === 'kasus_lama_jml' )
            $target[ $genderKeysPrefix[$i] ] = (int)$data['kasus_lama_jml'];
        }
      }

      $target['total'] = (int)$data['total_kasus'];
      $target['peserta_gakin'] = (int)$data['peserta_gakin'];
    }
    else {
      for($i=0; $i<count($genderKeysPrefix); $i++) {
        if( $genderKeysPrefix[$i] !== 'kasus_baru_jml' && $genderKeysPrefix[$i] !== 'kasus_lama_jml' ) {
          $target[ $genderKeysPrefix[$i].'_l' ] += (int)$data[ $genderKeysPrefix[$i].'_l' ];
          $target[ $genderKeysPrefix[$i].'_p' ] += (int)$data[ $genderKeysPrefix[$i].'_p' ];
        }
        else {
          if( $genderKeysPrefix[$i] === 'kasus_baru_jml' )
            $target[ $genderKeysPrefix[$i] ] += (int)$data['kasus_baru_jml'];
          elseif( $genderKeysPrefix[$i] === 'kasus_lama_jml' )
            $target[ $genderKeysPrefix[$i] ] += (int)$data['kasus_lama_jml'];
        }
      }

      $target['total'] += (int)$data['total_kasus'];
      $target['peserta_gakin'] += (int)$data['peserta_gakin'];
    }
  }
}
