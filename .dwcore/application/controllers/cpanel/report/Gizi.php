<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

class Gizi extends MY_Controller {
  public function __construct() {
    parent::__construct();

    $this->load->model('report_model');
    $this->load->library('report');
    $this->load->helper('form');

    $this->report->set_report_markup_data('page_title', 'Laporan Gizi');
  }
  
  public function index() {
    $this->report->set_report_model_data([
      'years' => $this->report_model->fetch_year(),
      'kecamatan' => $this->report_model->fetch_kecamatan()
    ]);

    $this->set_view_data( $this->report->get_report_data() );

    $this->render();
  }
  
  public function filter() {
    $nulledFound = FALSE;
    $valuePosted = array(
      'period_start'   => array(
        'year' => $this->input->post('f_period_start[0]', TRUE),
        'month' => $this->input->post('f_period_start[1]', TRUE)
      ),
      'period_end'     => array(
        'year' => $this->input->post('f_period_end[0]', TRUE),
        'month' => $this->input->post('f_period_end[1]', TRUE)
      ),
      'kecamatan_code' => $this->input->post('f_kecamatan', TRUE),
      'puskesmas_code' => $this->input->post('f_puskesmas', TRUE)
    );

    foreach($valuePosted as $key=>$value) {
      if( is_array($valuePosted[$key]) ) {
        foreach($value as $subkey=>$subvalue) {
          if($key == 'period_end' && is_null($subvalue) && $valuePosted['period_start']['month'] == 'all' )
            continue;

          if( is_null($subvalue) ) {
            $nulledFound = TRUE;
            break;
          }
        }
      }
      else {
        if( is_null($value) ) {
          if( $key == 'puskesmas_code' && $valuePosted['kecamatan_code'] == 'all' )
            continue;

          $nulledFound = TRUE;
          break;
        }
      }

      if( $nulledFound ) break;
    }

    if( $nulledFound ) {
      $this->session->set_flashdata('errNulledFilter', TRUE);
      redirect( base_url('report/gizi') );
    }
    else {
      $this->load->model('report/gizi_model');

      if($valuePosted['period_start']['month'] != 'all') {
        $periodId = array(
          // param 1: year, param 2: month number
          'start' => $this->report_model->get_period_id($valuePosted['period_start']['year'], $valuePosted['period_start']['month']),
          'end'   => $this->report_model->get_period_id($valuePosted['period_end']['year'], $valuePosted['period_end']['month'])
        );
      }
      else {
        $periodId = $this->report_model->get_period_id($valuePosted['period_start']['year'], 'all');
        $periodId = array(
          'start' => $periodId[0]['id'],
          'end' => $periodId[count($periodId)-1]['id']
        );
      }

      if( $valuePosted['kecamatan_code'] != 'all' ) {
        if( $valuePosted['puskesmas_code'] != 'all' ) {
          $puskesmasId = $this->report_model->get_puskesmas_id( $valuePosted['puskesmas_code'] );
        }
        else {
          $puskesmasId = $this->report_model->fetch_related_puskesmas_on_kecamatan( $valuePosted['kecamatan_code'] );
        }
      }
      else {
        $puskesmasId = $this->report_model->get_all_puskesmas_id();
      }

      $giziDiagnosis = $this->gizi_model->fetch_gizi_diagnosis($periodId, $puskesmasId);
      $monthString = array(1=>'Januari', 2=>'Februari', 3=>'Maret', 4=>'April', 5=>'Mei', 6=>'Juni', 7=>'Juli', 8=>'Agustus', 9=>'September', 10=>'Oktober', 11=>'November', 12=>'Desember');

      if( $valuePosted['period_start']['month'] !== 'all' ) {
        $valuePosted['period_start'] = $monthString[ $valuePosted['period_start']['month'] ].' '.$valuePosted['period_start']['year'];
        $valuePosted['period_end'] = $monthString[ $valuePosted['period_end']['month'] ].' '.$valuePosted['period_end']['year'];
      }
      else {
        $year = $valuePosted['period_start']['year'];
        $valuePosted['period_start'] = $monthString[1].' '.$year;
        $valuePosted['period_end'] = $monthString[12].' '.$year;
      }

      if( $valuePosted['kecamatan_code'] != 'all' ) {
        $valuePosted['kecamatan_name'] = $this->report_model->get_kecamatan_name( $valuePosted['kecamatan_code'] );
        $valuePosted['puskesmas_name'] = $this->report_model->get_puskesmas_name( $puskesmasId );

        if( is_array($valuePosted['puskesmas_name']) ) {
          $tempPuskesmasName = '';

          for($i=0; $i<count( $valuePosted['puskesmas_name'] ); $i++) {
            $tempPuskesmasName .= $valuePosted['puskesmas_name'][$i]['nama_puskesmas'];

            if($i !== count( $valuePosted['puskesmas_name'] )-1)
              $tempPuskesmasName .= ', ';
          }

          $valuePosted['puskesmas_name'] = $tempPuskesmasName;
        }

        $valuePosted['upt_name'] = ( substr($valuePosted['puskesmas_name'], 0, 3) == 'UPT' ) ? $valuePosted['puskesmas_name'] : $this->report_model->get_upt_from_kecamatan( $valuePosted['kecamatan_code'] );
      }
      else {
        $valuePosted['kecamatan_name'] = 'Semua Kecamatan';
        $valuePosted['puskesmas_name'] = '-';
        $valuePosted['upt_name'] = '-';
      }

      $this->set_view_data( $this->report->get_report_data() );
      $this->set_view_data('gizi_table', $this->make_gizi_stack($giziDiagnosis, $periodId['end']), 'markup');
      $this->set_view_data('diagnosis_data', json_encode($giziDiagnosis), 'model');
      $this->set_view_data('report_spec', $valuePosted, 'model');
      $this->render();
    }
  }

  public function export() {
    $this->load->library('export_xls');

    $diagnosisData = json_decode( $this->input->post('xls_diagnosis_json', TRUE), TRUE );
    $specificationData = json_decode( $this->input->post('xls_specification_json', TRUE), TRUE );
    $currentRow = 1;

    // set active sheet
    $this->export_xls->setActiveSheetIndex(0);
    $sheet = $this->export_xls->getActiveSheet();

    // set sheet title
    $sheet->setTitle('Sheet 1');

    // generate the top section of report
    $sheet->setCellValue('B'.$currentRow, 'LAPORAN BULANAN GIZI');
    $sheet->insertNewRowBefore($currentRow+1);

    $currentRow = $sheet->getHighestRow() + 1;

    for($i=0; $i<4; $i++) {
      $label = '';
      $value = '';

      if($i == 0) {
        $label = 'Tahun';
        $value = substr( $specificationData['period_start'], strlen( $specificationData['period_start'] ) - 4, strlen( $specificationData['period_start'] ) ).' - '.substr( $specificationData['period_end'], strlen( $specificationData['period_end'] ) - 4, strlen( $specificationData['period_end'] ) );
      }
      elseif($i == 1) {
        $label = 'Periode';
        $value = $specificationData['period_start'].' - '.$specificationData['period_end'];
      }
      elseif($i == 2) {
        $label = 'Puskesmas';
        $value = $specificationData['puskesmas_name'];
      }
      elseif($i == 3) {
        $label = 'UPT Puskesmas';
        $value = $specificationData['upt_name'];
      }

      $sheet->setCellValue('B'.$currentRow, $label);
      $sheet->setCellValue('C'.$currentRow, $value);
      $sheet->mergeCells("C{$currentRow}:E{$currentRow}");

      $currentRow++;
    }

    $currentRow = $sheet->getHighestRow() + 2;

    // generate table head
    $theadLabel = array(
      0 => array(
        'label' => 'NO',
        'col'   => 'A',
        'merge' => 'A'.$currentRow.':A'.($currentRow+1)
      ),
      1 => array(
        'label' => 'RINCIAN KEGIATAN',
        'col'   => 'B',
        'merge' => 'B'.$currentRow.':B'.($currentRow+1)
      ),
      2 => array(
        'label' => 'JUMLAH',
        'col'   => 'C',
        'merge' => 'C'.$currentRow.':E'.$currentRow
      ),
      3 => array(
        'label' => 'L',
        'col'   => 'C'
      ),
      4 => array(
        'label' => 'P',
        'col'   => 'D'
      ),
      5 => array(
        'label' => 'T',
        'col'   => 'E'
      )
    );

    for($i=0; $i<count($theadLabel); $i++) {
      if($i == 0 || $i == 1 || $i == 2) {
        $sheet->setCellValue( $theadLabel[$i]['col'].$currentRow, $theadLabel[$i]['label'] );
        $sheet->mergeCells( $theadLabel[$i]['merge'] );
      }
      else
        $sheet->setCellValue( $theadLabel[$i]['col'].($currentRow+1), $theadLabel[$i]['label'] );
    }

    $currentRow = $sheet->getHighestRow() + 1;

    // generate table content
    $rowLabel = array(
      'A' => array(
        'val' => 'Posyandu',
        'sub' => array(
          1 => 'Jumlah Posyandu yang Ada',
          2 => 'Jumlah Posyandu yang Melapor pada Bulan Ini'
        )
      ),
      'B' => array(
        'val' => 'Bayi',
        'sub' => array(
          1 => 'Jumlah Bayi (0-6 bl) atau (0-5 bl / 5 bl 29 hr) Seluruhnya(S)',
          2 => 'Bayi (0-6 bl) atau (0-5 bl / 5 bl 29 hr) dengan ASI Eksklusif',
          3 => 'Bayi (6 bl) Seluruhnya (S)',
          4 => 'Bayi (6 bl) yang Mendapatkan ASI Saja → LULUS ASI EKSKLUSIF',
          5 => 'Jumlah Bayi Kasus Gizi Kurang (KURUS: -3 BB/PB sampai < -2 BB/PB) Umur 0-6 bl Pada Bulan Ini',
          6 => 'Bayi (0-11 bl) Seluruhnya (S)',
          7 => 'Bayi (0-11 bl) dengan KMS (K)',
          8 => 'Bayi (0-11 bl) yang Ditimbang (D)',
          9 => 'Bayi (0-11 bl) Naik Berat Badan (N)',
          10 => 'Bayi (0-11 bl) Tidak Naik Berat Badan (T)',
          11 => 'Bayi (0-11 bl) 2T',
          12 => 'Bayi (0-11 bl) yang Tidak Ditimbang Bulan Lalu (O)',
          13 => 'Bayi (0-11 bl) Baru Pertama Lali Ditimbang (B)',
          14 => 'Bayi (0-11 bl) B G M',
          15 => 'Bayi (0-11 bl) B G M Baru',
          16 => 'Bayi (6-11 bl) yang Mendapatkan Vitamin A Dosis Tinggi',
          17 => 'Bayi (6-11 bl) yang Mendapat MP-ASI / PMT-P',
          18 => 'Jumlah Bayi Umur 6-11 bl Kasus Gizi Buruk (< -3 BB/U)',
          19 => 'Jumlah Bayi Umur 6-11 bl Kasus Gizi Kurang (KURUS: -3 BB/PB sampai < -2 BB/PB) Pada Bulan Ini',
          20 => 'Jumlah Bayi Umur 6-11 bl Kasus Gizi Kurang (KURUS: -3 BB/PB sampai < -2 BB/PB) Mendapat PMT-P Pada Bulan Ini',
          21 => 'Jumlah Anak Balita 6-23 bl yang Mendapat Taburia'
        )
      ),
      'C' => array(
        'val' => 'Balita',
        'sub' => array(
          1 => 'Jumlah Anak Umur 12-23 Seluruhnya (S)',
          2 => 'Jumlah Anak Umur 12-23 bl dengan KMS (K)',
          3 => 'Jumlah Anak Umur 12-23 bl yang Ditimbang (D)',
          4 => 'Jumlah Anak Umur 12-23 bl yang Naik Berat Badan (N)',
          5 => 'Jumlah Anak Umur 12-23 bl Tidak Naik berat badan (T)',
          6 => 'Jumlah Anak Umur 12-23 bl 2T',
          7 => 'Jumlah Anak Umur 12-23 bl yang Tidak Ditimbang Bulan Lalu (O)',
          8 => 'Jumlah Anak Umur 12-23 bl Baru Pertama Kali Ditimbang (B)',
          9 => 'Jumlah Anak Umur 12-23 bl yang BGM atau Kasus Gizi Buruk (< -3 BB/U) → Kasus Baru & Lama',
          10 => 'Jumlah Anak Umur 12-23 bl dengan Status BGM Baru atau Kasus Gizi Buruk (< -3 BB/U) → Kasus Baru',
          11 => 'Jumlah Anak Umur 12-23 bl kasus Gizi Kurang (KURUS: -3 BB/PB sampai < -2 BB/PB) pada Bulan Ini',
          12 => 'Jumlah Anak Umur 12-23 bl Kasus Gizi Kurang (KURUS: -3 BB/PB sampai < -2 BB/PB) Mendapat PMT-P pada Bulan Ini',
          13 => 'Jumlah Anak Umur 24-59 Seluruhnya (S)',
          14 => 'Jumlah Anak Umur 24-59 bl dengan KMS (K)',
          15 => 'Jumlah Anak Umur 24-59 bl yang Ditimbang (D)',
          16 => 'Jumlah Anak Umur 24-59 bl yang Naik Berat Badan (N)',
          17 => 'Jumlah Anak Umur 24-59 bl Tidak Naik Berat Badan (T)',
          18 => 'Jumlah Anak Umur 24-59 bl 2T',
          19 => 'Jumlah Anak Umur 24-59 bl yang Tidak Ditimbang Bulan Lalu (O)',
          20 => 'Jumlah Anak Umur 24-59 bl Baru Pertama Kali Ditimbang (B)',
          21 => 'Jumlah Anak Umur 24-59 bl Kasus Gizi Kurang (KURUS: -3 BB/TB sampai < -2 BB/TB) pada Bulan Ini',
          22 => 'Jumlah Anak Umur 24-59 bl Kasus Gizi Kurang (KURUS: -3 BB/TB sampai < -2 BB/TB) Mendapat PMT pada Bulan Ini',
          23 => 'Anak Umur 24-59 bl yang BGM atau Kasus Gizi Buruk (< -3 BB/U) → Kasus Baru & Lama',
          24 => 'Anak Umur 24-59 bl dengan Status BGM Baru atau Kasus Gizi Buruk (< -3 BB/U) → Kasus Baru',
          25 => 'Anak Umur 12-59 bl yang Mendapatkan Kapsul Vitamin A Dosis Tinggi',
          26 => 'Balita (12-23 bl) Gakin yang Mendapat MP-ASI / PMT-P',
          27 => 'Balita (24-59 bl) Gakin yang Mendapat MP-ASI / PMT-P'
        )
      ),
      'D' => array(
        'val' => 'Ibu Hamil',
        'sub' => array(
          1 => 'Ibu Hamil yang Mendapatkan Tablet Tambah Darah (Fe)',
          2 => 'Pertama Kali (Fe1)',
          3 => 'Ketiga Kali (Fe3)',
          4 => 'Fe1 Luar Wilayah',
          5 => 'Fe3 Luar Wilayah',
          6 => 'Lila < 23,5cm atau Penambahan BB < 9kg Selama Kehamilan',
          7 => 'Ibu Hamil Anemia dengan HB < 11gr %'
        )
      ),
      'E' => array(
        'val' => 'Ibu Nifas',
        'sub' => array(
          1 => 'Ibu Nifas yang Mendapatkan 2 Kapsul Vitamin A Dosis Tinggi',
          2 => 'Ibu Nifas yang Mendapatkan Tablet Fe',
          3 => 'Jumlah Ibu Hamil Risiko Kurang Energi Kronis (KEK) dengan Lingkar Lengan Atas (LiLA) < 23,5cm yang Mendapat PMT pada Bulan Ini'
        )
      ),
      'F' => array(
        'val' => 'Gizi Buruk',
        'sub' => array(
          1 => 'Bayi (0-11 bl) yang Bergizi Buruk → Kasus Baru',
          2 => 'Bayi (0-11 bl) yang Bergizi Buruk → Kasus Baru & Lama',
          3 => 'Anak Umur 12-23 bl yang Bergizi Buruk → Kasus Baru',
          4 => 'Anak Umur 12-23 bl yang Bergizi Buruk → Kasus Baru & Lama',
          5 => 'Anak Umur 24-59 bl yang Bergizi Buruk → Kasus Baru',
          6 => 'Anak Umur 24-59 bl yang Bergizi Buruk → Kasus Baru & Lama',
          7 => array(
            'val' => 'Kasus Gizi Buruk Baru pada Bulan Ini',
            'a' => 'Tanpa Gejala Klinis',
            'b' => 'Marasmus',
            'c' => 'Kwashiorkor',
            'd' => 'Marasmic-Kwashiorkor',
            'e' => 'Lain-lain'
          ),
          8 => 'Kasus Gizi Buruk Baru yang Masih dirawat Pada Bulan Ini',
          9 => 'Kasus Gizi Buruk Lama yang Masih dirawat Sampai Pada Bulan Ini',
          10 => 'Kasus Gizi Buruk Baru yang Meninggal Pada Bulan Ini',
          11 => 'Kasus Gizi Buruk Lama yang Meninggal Pada Bulan Ini'
        )
      ),
      'G' => array(
        'val' => 'Masalah Gizi Lain',
        'sub' => array(
          1 => 'Desa / Kelurahan Disurvei Garam Beryodium',
          2 => 'Desa / Kelurahan dengan Garam Beryodium Baik',
          3 => 'Jumlah KK yang Disurvei Garam Beryodium',
          4 => 'Desa / Kelurahan Endemis GAKI',
          5 => 'Penduduk yang Menderita GAKI',
          6 => 'WUS yang Diberi Kapsul Iodium'
        )
      )
    );
    $rowValue = array();

    foreach($rowLabel as $k1=>$v1) {
      if($k1 !== 'A') {
        $sheet->setCellValue('A'.$currentRow, chr(ord($k1)-1));
        $sheet->setCellValue('B'.$currentRow, $v1['val']);

        $currentRow++;
      }

      foreach($v1['sub'] as $k2=>$v2) {
        $sheet->setCellValue('A'.$currentRow, $k2);

        if( !is_array($v2) ) {
          $sheet->setCellValue('B'.$currentRow, $v2);

          $currentRow++;
        }
        else {
          $sheet->setCellValue('B'.$currentRow, $v2['val']);

          $currentRow++;

          for($i=0; $i<count($v2)-1; $i++) {
            $sheet->setCellValue('B'.$currentRow, chr( ord('a')+$i ).') '.$v2[ chr( ord('a')+$i ) ]);

            $currentRow++;
          }
        }
      }
    }

    foreach($diagnosisData as $v1) {
      unset( $v1['nama_puskesmas'] );
      unset( $v1['ID_PUSKESMAS'] );
      unset( $v1['ID_PERIODE'] );

      $tempData = array_values($v1);
      $tempCount = count($tempData);

      for($j=0; $j<$tempCount; $j++) {
        if( !array_key_exists($j, $rowValue) ) {
          $rowValue[$j] = $tempData[$j];
        }
        else {
          $rowValue[$j] += $tempData[$j];
        }
      }
    }

    $initRowAdder = 10;
    $rowSkip = array( 12, 34, 62, 70, 74, 81, 91 );
    $i = 0;

    foreach($rowValue as $k1=>$v1) {
      $formula = '=SUM(C'.($i + $initRowAdder).':D'.($i + $initRowAdder).')';

      if( in_array( $i+$initRowAdder, $rowSkip ) ) { $initRowAdder += 1; }

      if($k1 <= 1) { //Posyandu Data
        $sheet->setCellValue('C'.($i + $initRowAdder), '-');
        $sheet->setCellValue('D'.($i + $initRowAdder), '-');
        $sheet->setCellValue('E'.($i + $initRowAdder), $v1);
      }
      elseif($k1 >= 2 && $k1 < 44) { //Bayi Data
        if($k1-2 < 21 ) { //Bayi Laki-laki
          $sheet->setCellValue('C'.($i + $initRowAdder), $v1);

          if($i == 44/2)
            $i = 1;
        }
        else { //Bayi Perempuan & total
          $sheet->setCellValue('D'.($i + $initRowAdder), $v1);
          $sheet->setCellValue('E'.($i + $initRowAdder), $formula);
        }
      }
      elseif($k1 >= 44 && $k1 < 98) { //Balita Data
        if($k1-44 < 27) { //Balita Laki-laki
          $sheet->setCellValue('C'.($i + $initRowAdder), $v1);

          if($i == 98/2)
            $i = 44/2;
        }
        else { //Balita Perempuan & total
          $sheet->setCellValue('D'.($i + $initRowAdder), $v1);
          $sheet->setCellValue('E'.($i + $initRowAdder), $formula);
        }
      }
      elseif( ($k1 >= 98 && $k1 < 105) || ($k1 >= 105 && $k1 < 108) ) {
        //Ibu Hamil & Ibu Nifas Data
        $sheet->setCellValue('C'.($i + $initRowAdder), '-');
        $sheet->setCellValue('D'.($i + $initRowAdder), $v1);
        $sheet->setCellValue('E'.($i + $initRowAdder), $v1);
      }
      elseif($k1 >= 108 && $k1 < 138) { //Gizi Buruk Data
        if($k1 >= 108 && $k1 < 112) { //Bayi Gizi Buruk
          if($k1-108 < 2) { //BGB Laki-laki
            $sheet->setCellValue('C'.($i + $initRowAdder), $v1);

            if($i == 122/2) {
              $i = 118/2;
            }
          }
          else { //BGB Perempuan & total
            $sheet->setCellValue('D'.($i + $initRowAdder), $v1);
            $sheet->setCellValue('E'.($i + $initRowAdder), $formula);
          }
        }
        elseif($k1 >= 112 && $k1 < 120) { //Anak Gizi Buruk
          if($k1-112 < 4) { //AGB Laki-laki
            $sheet->setCellValue('C'.($i + $initRowAdder), $v1);

            if($i == 130/2) {
              $i = 122/2;
            }
          }
          else { //AGB Perempuan & total
            $sheet->setCellValue('D'.($i + $initRowAdder), $v1);
            $sheet->setCellValue('E'.($i + $initRowAdder), $formula);
          }
        }
        elseif($k1 >= 120 && $k1 < 138) { //Kasus Gizi Buruk
          if($k1-120 < 9) { //KGB Laki-laki
            $sheet->setCellValue('C'.($i + $initRowAdder), $v1);

            if($i == 148/2) {
              $i = 130/2;
            }
          }
          else { //KGB Perempuan & total
            $sheet->setCellValue('D'.($i + $initRowAdder), $v1);
            $sheet->setCellValue('E'.($i + $initRowAdder), $formula);
          }
        }
      }
      elseif($k1 >= 138) { //Other Data
        if($k1-138 >= 0 && $k1-138 < 4) { //Desa
          $sheet->setCellValue('C'.($i + $initRowAdder), '-');
          $sheet->setCellValue('D'.($i + $initRowAdder), '-');
          $sheet->setCellValue('E'.($i + $initRowAdder), $v1);
        }
        else { //Penduduk GAKI & WUS Iodium
          $sheet->setCellValue('C'.($i + $initRowAdder), $v1);
          $sheet->setCellValue('D'.($i + $initRowAdder), $rowValue[$k1+1]);
          $sheet->setCellValue('E'.($i + $initRowAdder), $formula);

          $i += 1;
          $formula = '=SUM(C'.($i + $initRowAdder).':D'.($i + $initRowAdder).')';

          $sheet->setCellValue('C'.($i + $initRowAdder), $rowValue[$k1+2]);
          $sheet->setCellValue('D'.($i + $initRowAdder), $rowValue[$k1+3]);
          $sheet->setCellValue('E'.($i + $initRowAdder), $formula);

          break;
        }
      }

      $i++;
    }

    // set what the name of the file to be downloaded
    $fileName = 'test_xls.xls';
    // set related headers and download the generated file
    // tell the browser about the xls mime-type
    header('Content-Type: application/vnd.ms-excel');
    // attach the generated xls file
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    // don't cache the file
    header('Cache-Control: max-age=0');
    // write XLS object and set to PHP output
    $objWriter = PHPExcel_IOFactory::createWriter($this->export_xls, 'Excel5');
    $objWriter->save('php://output');

    // and your download will start :)
  }

  /*private function in_array_loop($needle, $haystack, $strict=false) {
    foreach ($haystack as $item) {
      if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_loop($needle, $item, $strict))) {
        return true;
      }
    }

    return false;
  }*/

  private function make_gizi_stack($giziData, $endPeriodId) {
    $options = array(
      0 => array('Tanpa gejala klinis', 'Marasmus', 'Kwashiorkor', 'Marasmic-kwashiorkor', 'Dan lain-lain')
    );
    $category = array(
      0 => array(
        'main' => 'Posyandu',
        'sub'  => array(
          'Jumlah Posyandu yang Ada' => 0,
          'Jumlah Posyandu yang Melapor pada Bulan Ini' => 0
        )
      ),
      1 => array(
        'main'     => 'Bayi',
        'sub'      => array(
          'Jumlah Bayi (0-6 bl) atau (0-5 bl / 5 bl 29 hr) Seluruhnya(S)' => null,
          'Bayi (0-6 bl) atau (0-5 bl / 5 bl 29 hr) dengan ASI Eksklusif' => null,
          'Bayi (6 bl) Seluruhnya (S)' => null,
          'Bayi (6 bl) yang Mendapatkan ASI Saja &rarr; LULUS ASI EKSKLUSIF' => null,
          'Jumlah Bayi Kasus Gizi Kurang (KURUS: -3 BB/PB sampai &lt; -2 BB/PB) Umur 0-6 bl Pada Bulan Ini' => null,
          'Bayi (0-11 bl) Seluruhnya (S)' => null,
          'Bayi (0-11 bl) dengan KMS (K)' => null,
          'Bayi (0-11 bl) yang Ditimbang (D)' => null,
          'Bayi (0-11 bl) Naik Berat Badan (N)' => null,
          'Bayi (0-11 bl) Tidak Naik Berat Badan (T)' => null,
          'Bayi (0-11 bl) 2T' => null,
          'Bayi (0-11 bl) yang Tidak Ditimbang Bulan Lalu (O)' => null,
          'Bayi (0-11 bl) Baru Pertama Lali Ditimbang (B)' => null,
          'Bayi (0-11 bl) B G M' => null,
          'Bayi (0-11 bl) B G M Baru' => null,
          'Bayi (6-11 bl) yang Mendapatkan Vitamin A Dosis Tinggi' => null,
          'Bayi (6-11 bl) yang Mendapat MP-ASI / PMT-P' => null,
          'Jumlah Bayi Umur 6-11 bl Kasus Gizi Buruk (&lt; -3 BB/U)' => null,
          'Jumlah Bayi Umur 6-11 bl Kasus Gizi Kurang (KURUS: -3 BB/PB sampai &lt; -2 BB/PB) Pada Bulan Ini' => null,
          'Jumlah Bayi Umur 6-11 bl Kasus Gizi Kurang (KURUS: -3 BB/PB sampai &lt; -2 BB/PB) Mendapat PMT-P Pada Bulan Ini' => null,
          'Jumlah Anak Balita 6-23 bl yang Mendapat Taburia' => null
        )
      ),
      2 => array(
        'main' => 'Balita',
        'sub'  => array(
          'Jumlah Anak Umur 12-23 Seluruhnya (S)' => null,
          'Jumlah Anak Umur 12-23 bl dengan KMS (K)' => null,
          'Jumlah Anak Umur 12-23 bl yang Ditimbang (D)' => null,
          'Jumlah Anak Umur 12-23 bl yang Naik Berat Badan (N)' => null,
          'Jumlah Anak Umur 12-23 bl Tidak Naik berat badan (T)' => null,
          'Jumlah Anak Umur 12-23 bl 2T' => null,
          'Jumlah Anak Umur 12-23 bl yang Tidak Ditimbang Bulan Lalu (O)' => null,
          'Jumlah Anak Umur 12-23 bl Baru Pertama Kali Ditimbang (B)' => null,
          'Jumlah Anak Umur 12-23 bl yang BGM atau Kasus Gizi Buruk (&lt; -3 BB/U) &rarr; Kasus Baru & Lama' => null,
          'Jumlah Anak Umur 12-23 bl dengan Status BGM Baru atau Kasus Gizi Buruk (&lt; -3 BB/U) &rarr; Kasus Baru' => null,
          'Jumlah Anak Umur 12-23 bl kasus Gizi Kurang (KURUS: -3 BB/PB sampai &lt; -2 BB/PB) pada Bulan Ini' => null,
          'Jumlah Anak Umur 12-23 bl Kasus Gizi Kurang (KURUS: -3 BB/PB sampai &lt; -2 BB/PB) Mendapat PMT-P pada Bulan Ini' => null,
          'Jumlah Anak Umur 24-59 Seluruhnya (S)' => null,
          'Jumlah Anak Umur 24-59 bl dengan KMS (K)' => null,
          'Jumlah Anak Umur 24-59 bl yang Ditimbang (D)' => null,
          'Jumlah Anak Umur 24-59 bl yang Naik Berat Badan (N)' => null,
          'Jumlah Anak Umur 24-59 bl Tidak Naik Berat Badan (T)' => null,
          'Jumlah Anak Umur 24-59 bl 2T' => null,
          'Jumlah Anak Umur 24-59 bl yang Tidak Ditimbang Bulan Lalu (O)' => null,
          'Jumlah Anak Umur 24-59 bl Baru Pertama Kali Ditimbang (B)' => null,
          'Jumlah Anak Umur 24-59 bl Kasus Gizi Kurang (KURUS: -3 BB/TB sampai &lt; -2 BB/TB) pada Bulan Ini' => null,
          'Jumlah Anak Umur 24-59 bl Kasus Gizi Kurang (KURUS: -3 BB/TB sampai &lt; -2 BB/TB) Mendapat PMT pada Bulan Ini' => null,
          'Anak Umur 24-59 bl yang BGM atau Kasus Gizi Buruk (&lt; -3 BB/U) &rarr; Kasus Baru &amp; Lama' => null,
          'Anak Umur 24-59 bl dengan Status BGM Baru atau Kasus Gizi Buruk (&lt; -3 BB/U) &rarr; Kasus Baru' => null,
          'Anak Umur 12-59 bl yang Mendapatkan Kapsul Vitamin A Dosis Tinggi ' => null,
          'Balita (12-23 bl) Gakin yang Mendapat MP-ASI / PMT-P' => null,
          'Balita (24-59 bl) Gakin yang Mendapat MP-ASI / PMT-P' => null
        )
      ),
      3 => array(
        'main' => 'Ibu Hamil',
        'sub'  => array(
          'Ibu Hamil yang Mendapatkan Tablet Tambah Darah (Fe)'           => 0,
          'Pertama Kali (Fe1)'                                            => 0,
          'Ketiga Kali (Fe3)'                                             => 0,
          'Fe1 Luar Wilayah'                                              => 0,
          'Fe3 Luar Wilayah'                                              => 0,
          'Lila &lt; 23,5cm atau Penambahan BB &lt; 9kg Selama Kehamilan' => 0,
          'Ibu Hamil Anemia dengan HB &lt; 11gr %'                        => 0,
        )
      ),
      4 => array(
        'main' => 'Ibu Nifas',
        'sub'  => array(
          'Ibu Nifas yang Mendapatkan 2 Kapsul Vitamin A Dosis Tinggi'  => 0,
          'Ibu Nifas yang Mendapatkan Tablet Fe'                        => 0,
          'Jumlah Ibu Hamil Risiko Kurang Energi Kronis (KEK) dengan Lingkar Lengan Atas (LiLA) &lt; 23,5cm yang Mendapat PMT pada Bulan Ini' => 0
        )
      ),
      5 => array(
        'main' => 'Gizi Buruk',
        'sub'  => array(
          'Bayi (0-11 bl) yang Bergizi Buruk &rarr; Kasus Baru'                => null,
          'Bayi (0-11 bl) yang Bergizi Buruk &rarr; Kasus Baru &amp; Lama'     => null,
          'Anak Umur 12-23 bl yang Bergizi Buruk &rarr; Kasus Baru'            => null,
          'Anak Umur 12-23 bl yang Bergizi Buruk &rarr; Kasus Baru &amp; Lama' => null,
          'Anak Umur 24-59 bl yang Bergizi Buruk &rarr; Kasus Baru'            => null,
          'Anak Umur 24-59 bl yang Bergizi Buruk &rarr; Kasus Baru &amp; Lama' => null,
          'Kasus Gizi Buruk Baru pada Bulan Ini'                               => $options[0],
          'Kasus Gizi Buruk Baru yang Masih dirawat Pada Bulan Ini'            => null,
          'Kasus Gizi Buruk Lama yang Masih dirawat Sampai Pada Bulan Ini'     => null,
          'Kasus Gizi Buruk Baru yang Meninggal Pada Bulan Ini'                => null,
          'Kasus Gizi Buruk Lama yang Meninggal Pada Bulan Ini'                => null
        )
      ),
      6 => array(
        'main' => 'Masalah Gizi Lain',
        'sub'  => array(
          'Desa / Kelurahan Disurvei Garam Beryodium'    => 0,
          'Desa / Kelurahan dengan Garam Beryodium Baik' => 0,
          'Jumlah KK yang Disurvei Garam Beryodium'      => 0,
          'Desa / Kelurahan Endemis GAKI'                => 0,
          'Penduduk yang Menderita GAKI'                 => null,
          'WUS yang Diberi Kapsul Iodium'                => null
        )
      )
    );
    $stack = array();
    $highestPeriodId = 0;

    for($k=0; $k<count($giziData); $k++) {
      if( (int)$giziData[$k]['ID_PERIODE'] > $highestPeriodId )
        $highestPeriodId = (int)$giziData[$k]['ID_PERIODE'];
    }

    foreach($giziData as $dataValue) {
      $currentPeriodIdCheck = $dataValue['ID_PERIODE'];

      if( !array_key_exists($dataValue['nama_puskesmas'], $stack) ) {
        $stack[ $dataValue['nama_puskesmas'] ] = array();

        for($i=0; $i<count($category); $i++) {
          foreach( $category[$i]['sub'] as $subKey=>$subValue ) {
            $stack[ $dataValue['nama_puskesmas'] ][$i][$subKey] = array();

            if( is_null($subValue) ) {
              $stack[ $dataValue['nama_puskesmas'] ][$i][$subKey] = array(
                'l' => 0,
                'p' => 0,
                't' => 0
              );
            }
            elseif( $subValue == 0 ) {
              if( $i == 3 || $i == 4 ) {
                $stack[ $dataValue['nama_puskesmas'] ][$i][$subKey] = array(
                  'l' => '-',
                  'p' => 0,
                  't' => 0
                );
              }
              else {
                $stack[ $dataValue['nama_puskesmas'] ][$i][$subKey] = array(
                  'l' => '-',
                  'p' => '-',
                  't' => 0
                );
              }
            }
            else {
              for($j=0; $j<count($subValue); $j++) {
                $stack[ $dataValue['nama_puskesmas'] ][$i][$subKey][$subValue[$j]] = array(
                  'l' => 0,
                  'p' => 0,
                  't' => 0
                );
                /*if($subValue[$j] == 'Jumlah KK yang Disurvei Garam Beryodium') {
                  $stack[ $dataValue['nama_puskesmas'] ][$i][$subKey][$subValue[$j]] = array(
                    'l' => '-',
                    'p' => '-',
                    't' => 0
                  );
                }
                else {
                  $stack[ $dataValue['nama_puskesmas'] ][$i][$subKey][$subValue[$j]] = array(
                    'l' => 0,
                    'p' => 0,
                    't' => 0
                  );
                }*/
              }
            }
          }

          /*if($currentPeriodIdCheck == $endPeriodId)
            $this->push_options_value($stack, $i, 'n', $dataValue);

          $this->push_options_value($stack, $i, 'kum', $dataValue);*/
        }
      }

      for($i=0; $i<count($category); $i++) {
        if($i == 0) {
          $this->push_options_value($stack, $i, 't', $dataValue);
        }
        elseif($i == 1 || $i == 2 || $i == 5) {
          $this->push_options_value($stack, $i, 'l', $dataValue);
          $this->push_options_value($stack, $i, 'p', $dataValue);
        }
        elseif($i == 3 || $i == 4) {
          $this->push_options_value($stack, $i, 'p', $dataValue);
        }
        elseif($i == 6) {
          $this->push_options_value($stack, $i, 'l', $dataValue);
          $this->push_options_value($stack, $i, 'p', $dataValue);
          $this->push_options_value($stack, $i, 't', $dataValue);
        }
      }
    }

    $table = $this->prepare_table($stack, $category);

    return $table;
  }

  private function push_options_value(&$stack, $counter, $type, $data) {
    switch($counter) {
      case 0:
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Posyandu yang Ada'][$type] += $data['JML_POSYANDU'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Posyandu yang Melapor pada Bulan Ini'][$type] += $data['JML_POSYANDU_LAPOR'];
      break;

      case 1:
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Bayi (0-6 bl) atau (0-5 bl / 5 bl 29 hr) Seluruhnya(S)'][$type] += $data['BAYI_'.strtoupper($type).'_0_6_S'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Bayi (0-6 bl) atau (0-5 bl / 5 bl 29 hr) dengan ASI Eksklusif'][$type] += $data['BAYI_'.strtoupper($type).'_0_6_ASIEx'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Bayi (6 bl) Seluruhnya (S)'][$type] += $data['BAYI_'.strtoupper($type).'_6_S'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Bayi (6 bl) yang Mendapatkan ASI Saja &rarr; LULUS ASI EKSKLUSIF'][$type] += $data['BAYI_'.strtoupper($type).'_6_ASIEx'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Bayi Kasus Gizi Kurang (KURUS: -3 BB/PB sampai &lt; -2 BB/PB) Umur 0-6 bl Pada Bulan Ini'][$type] += $data['BAYI_'.strtoupper($type).'_0_6_GK'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Bayi (0-11 bl) Seluruhnya (S)'][$type] += $data['BAYI_'.strtoupper($type).'_0_11_S'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Bayi (0-11 bl) dengan KMS (K)'][$type] += $data['BAYI_'.strtoupper($type).'_0_11_K'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Bayi (0-11 bl) yang Ditimbang (D)'][$type] += $data['BAYI_'.strtoupper($type).'_0_11_D'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Bayi (0-11 bl) Naik Berat Badan (N)'][$type] += $data['BAYI_'.strtoupper($type).'_0_11_N'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Bayi (0-11 bl) Tidak Naik Berat Badan (T)'][$type] += $data['BAYI_'.strtoupper($type).'_0_11_T'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Bayi (0-11 bl) 2T'][$type] += $data['BAYI_'.strtoupper($type).'_0_11_2T'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Bayi (0-11 bl) yang Tidak Ditimbang Bulan Lalu (O)'][$type] += $data['BAYI_'.strtoupper($type).'_0_11_O'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Bayi (0-11 bl) Baru Pertama Lali Ditimbang (B)'][$type] += $data['BAYI_'.strtoupper($type).'_0_11_B'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Bayi (0-11 bl) B G M'][$type] += $data['BAYI_'.strtoupper($type).'_0_11_BGM'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Bayi (0-11 bl) B G M Baru'][$type] += $data['BAYI_'.strtoupper($type).'_0_11_BGM_BARU'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Bayi (6-11 bl) yang Mendapatkan Vitamin A Dosis Tinggi'][$type] += $data['BAYI_'.strtoupper($type).'_6_11_A_DT'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Bayi (6-11 bl) yang Mendapat MP-ASI / PMT-P'][$type] += $data['BAYI_'.strtoupper($type).'_6_11_MP_ASI'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Bayi Umur 6-11 bl Kasus Gizi Buruk (&lt; -3 BB/U)'][$type] += $data['BAYI_'.strtoupper($type).'_6_11_GB'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Bayi Umur 6-11 bl Kasus Gizi Kurang (KURUS: -3 BB/PB sampai &lt; -2 BB/PB) Pada Bulan Ini'][$type] += $data['BAYI_'.strtoupper($type).'_6_11_GK'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Bayi Umur 6-11 bl Kasus Gizi Kurang (KURUS: -3 BB/PB sampai &lt; -2 BB/PB) Mendapat PMT-P Pada Bulan Ini'][$type] += $data['BAYI_'.strtoupper($type).'_6_11_GK_PMT'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Balita 6-23 bl yang Mendapat Taburia'][$type] += $data['BAYI_'.strtoupper($type).'_6_11_TAB'];
      break;

      case 2:
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 12-23 Seluruhnya (S)'][$type] += $data['ANAK_'.strtoupper($type).'_12_23_S'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 12-23 bl dengan KMS (K)'][$type] += $data['ANAK_'.strtoupper($type).'_12_23_K'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 12-23 bl yang Ditimbang (D)'][$type] += $data['ANAK_'.strtoupper($type).'_12_23_D'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 12-23 bl yang Naik Berat Badan (N)'][$type] += $data['ANAK_'.strtoupper($type).'_12_23_N'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 12-23 bl Tidak Naik berat badan (T)'][$type] += $data['ANAK_'.strtoupper($type).'_12_23_T'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 12-23 bl 2T'][$type] += $data['ANAK_'.strtoupper($type).'_12_23_2T'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 12-23 bl yang Tidak Ditimbang Bulan Lalu (O)'][$type] += $data['ANAK_'.strtoupper($type).'_12_23_O'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 12-23 bl Baru Pertama Kali Ditimbang (B)'][$type] += $data['ANAK_'.strtoupper($type).'_12_23_B'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 12-23 bl yang BGM atau Kasus Gizi Buruk (&lt; -3 BB/U) &rarr; Kasus Baru & Lama'][$type] += $data['ANAK_'.strtoupper($type).'_12_23_BGM'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 12-23 bl dengan Status BGM Baru atau Kasus Gizi Buruk (&lt; -3 BB/U) &rarr; Kasus Baru'][$type] += $data['ANAK_'.strtoupper($type).'_12_23_BGM_BARU'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 12-23 bl kasus Gizi Kurang (KURUS: -3 BB/PB sampai &lt; -2 BB/PB) pada Bulan Ini'][$type] += $data['ANAK_'.strtoupper($type).'_12_23_GK'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 12-23 bl Kasus Gizi Kurang (KURUS: -3 BB/PB sampai &lt; -2 BB/PB) Mendapat PMT-P pada Bulan Ini'][$type] += $data['ANAK_'.strtoupper($type).'_12_23_GK_PMT'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 24-59 Seluruhnya (S)'][$type] += $data['ANAK_'.strtoupper($type).'_24_59_S'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 24-59 bl dengan KMS (K)'][$type] += $data['ANAK_'.strtoupper($type).'_24_59_K'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 24-59 bl yang Ditimbang (D)'][$type] += $data['ANAK_'.strtoupper($type).'_24_59_D'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 24-59 bl yang Naik Berat Badan (N)'][$type] += $data['ANAK_'.strtoupper($type).'_24_59_N'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 24-59 bl Tidak Naik Berat Badan (T)'][$type] += $data['ANAK_'.strtoupper($type).'_24_59_T'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 24-59 bl 2T'][$type] += $data['ANAK_'.strtoupper($type).'_24_59_2T'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 24-59 bl yang Tidak Ditimbang Bulan Lalu (O)'][$type] += $data['ANAK_'.strtoupper($type).'_24_59_O'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 24-59 bl Baru Pertama Kali Ditimbang (B)'][$type] += $data['ANAK_'.strtoupper($type).'_24_59_B'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 24-59 bl Kasus Gizi Kurang (KURUS: -3 BB/TB sampai &lt; -2 BB/TB) pada Bulan Ini'][$type] += $data['ANAK_'.strtoupper($type).'_24_59_GK'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Anak Umur 24-59 bl Kasus Gizi Kurang (KURUS: -3 BB/TB sampai &lt; -2 BB/TB) Mendapat PMT pada Bulan Ini'][$type] += $data['ANAK_'.strtoupper($type).'_24_59_GK_PMT'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Anak Umur 24-59 bl yang BGM atau Kasus Gizi Buruk (&lt; -3 BB/U) &rarr; Kasus Baru &amp; Lama'][$type] += $data['ANAK_'.strtoupper($type).'_24_59_BGM'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Anak Umur 24-59 bl dengan Status BGM Baru atau Kasus Gizi Buruk (&lt; -3 BB/U) &rarr; Kasus Baru'][$type] += $data['ANAK_'.strtoupper($type).'_24_59_BGM_BARU'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Anak Umur 12-59 bl yang Mendapatkan Kapsul Vitamin A Dosis Tinggi '][$type] += $data['ANAK_'.strtoupper($type).'_12_59_A_DT'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Balita (12-23 bl) Gakin yang Mendapat MP-ASI / PMT-P'][$type] += $data['ANAK_'.strtoupper($type).'_12_23_MP_ASI_PMT'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Balita (24-59 bl) Gakin yang Mendapat MP-ASI / PMT-P'][$type] += $data['ANAK_'.strtoupper($type).'_24_59_MP_ASI_PMT'];
      break;

      case 3:
        $stack[ $data['nama_puskesmas'] ][$counter]['Ibu Hamil yang Mendapatkan Tablet Tambah Darah (Fe)'][$type] += $data['IBU_FE'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Pertama Kali (Fe1)'][$type] += $data['IBU_FE1'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Ketiga Kali (Fe3)'][$type] += $data['IBU_FE3'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Fe1 Luar Wilayah'][$type] += $data['IBU_FE1_LUAR'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Fe3 Luar Wilayah'][$type] += $data['IBU_FE3_LUAR'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Lila &lt; 23,5cm atau Penambahan BB &lt; 9kg Selama Kehamilan'][$type] += $data['IBU_BB_9'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Ibu Hamil Anemia dengan HB &lt; 11gr %'][$type] += $data['IBU_ANEM_HB_11'];
      break;

      case 4:
        $stack[ $data['nama_puskesmas'] ][$counter]['Ibu Nifas yang Mendapatkan 2 Kapsul Vitamin A Dosis Tinggi'][$type] += $data['IBU_NIFAS_A_DT'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Ibu Nifas yang Mendapatkan Tablet Fe'][$type] += $data['IBU_NIFAS_FE'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah Ibu Hamil Risiko Kurang Energi Kronis (KEK) dengan Lingkar Lengan Atas (LiLA) &lt; 23,5cm yang Mendapat PMT pada Bulan Ini'][$type] += $data['IBU_HAMIL_KEK'];
      break;

      case 5:
        $stack[ $data['nama_puskesmas'] ][$counter]['Bayi (0-11 bl) yang Bergizi Buruk &rarr; Kasus Baru'][$type] += $data['BAYI_'.strtoupper($type).'_0_11_GB_BARU'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Bayi (0-11 bl) yang Bergizi Buruk &rarr; Kasus Baru &amp; Lama'][$type] += $data['BAYI_'.strtoupper($type).'_0_11_GB_BARU_LAMA'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Anak Umur 12-23 bl yang Bergizi Buruk &rarr; Kasus Baru'][$type] += $data['ANAK_'.strtoupper($type).'_12_23_GB_BARU'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Anak Umur 12-23 bl yang Bergizi Buruk &rarr; Kasus Baru &amp; Lama'][$type] += $data['ANAK_'.strtoupper($type).'_12_23_GB_BARU_LAMA'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Anak Umur 24-59 bl yang Bergizi Buruk &rarr; Kasus Baru'][$type] += $data['ANAK_'.strtoupper($type).'_24_59_GB_BARU'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Anak Umur 24-59 bl yang Bergizi Buruk &rarr; Kasus Baru &amp; Lama'][$type] += $data['ANAK_'.strtoupper($type).'_24_59_GB_BARU_LAMA'];

        $stack[ $data['nama_puskesmas'] ][$counter]['Kasus Gizi Buruk Baru pada Bulan Ini']['Tanpa gejala klinis'][$type] += $data['GB_'.strtoupper($type).'_TANPA_GEJALA_KLINIS'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Kasus Gizi Buruk Baru pada Bulan Ini']['Marasmus'][$type] += $data['GB_'.strtoupper($type).'_MARASMUS'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Kasus Gizi Buruk Baru pada Bulan Ini']['Kwashiorkor'][$type] += $data['GB_'.strtoupper($type).'_KWASHIORKOR'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Kasus Gizi Buruk Baru pada Bulan Ini']['Marasmic-kwashiorkor'][$type] += $data['GB_'.strtoupper($type).'_MARASMIS_KWASHIORKOR'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Kasus Gizi Buruk Baru pada Bulan Ini']['Dan lain-lain'][$type] += $data['GB_'.strtoupper($type).'_LAIN_LAIN'];

        $stack[ $data['nama_puskesmas'] ][$counter]['Kasus Gizi Buruk Baru yang Masih dirawat Pada Bulan Ini'][$type] += $data['GB_'.strtoupper($type).'_BARU_DIRAWAT'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Kasus Gizi Buruk Lama yang Masih dirawat Sampai Pada Bulan Ini'][$type] += $data['GB_'.strtoupper($type).'_LAMA_DIRAWAT'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Kasus Gizi Buruk Baru yang Meninggal Pada Bulan Ini'][$type] += $data['GB_'.strtoupper($type).'_BARU_MENINGGAL'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Kasus Gizi Buruk Lama yang Meninggal Pada Bulan Ini'][$type] += $data['GB_'.strtoupper($type).'_LAMA_MENINGGAL'];
      break;

      case 6:
        if($type == 'l' || $type == 'p') {
          $stack[ $data['nama_puskesmas'] ][$counter]['Penduduk yang Menderita GAKI'][$type] += $data['DESA_PENDUDUK_GAKI_'.strtoupper($type)];
          $stack[ $data['nama_puskesmas'] ][$counter]['WUS yang Diberi Kapsul Iodium'][$type] += $data['DESA_WUS_KAPSUL_YOD_'.strtoupper($type)];
        }
        elseif($type == 't') {
          $stack[ $data['nama_puskesmas'] ][$counter]['Desa / Kelurahan Disurvei Garam Beryodium'][$type] += $data['DESA_GARAM_YOD_SURVEY'];
          $stack[ $data['nama_puskesmas'] ][$counter]['Desa / Kelurahan dengan Garam Beryodium Baik'][$type] += $data['DESA_GARAM_YOD_BAIK'];
          $stack[ $data['nama_puskesmas'] ][$counter]['Jumlah KK yang Disurvei Garam Beryodium'][$type] += $data['DESA_KK_GARAM_YUD_SURVEY'];
          $stack[ $data['nama_puskesmas'] ][$counter]['Desa / Kelurahan Endemis GAKI'][$type] += $data['DESA_ENDEMIS_GAKI'];
        }
      break;
    }
  }

  private function prepare_table($stack, $category) {
    $preMarkup = '';
    $puskesmas = array_keys($stack);
    $rowNumCounter = 1;
    $rowAlphaCounter = 'A';

    $headMarkup = '<thead><tr class="table_head"><th class="text-center middle-aligned" rowspan="2">No.</th><th class="text-center middle-aligned" rowspan="2">Rincian Kegiatan</th><th class="text-center middle-aligned" colspan="3" rowspan="1">Jumlah</th></tr><tr><th class="text-center middle-aligned" rowspan="1">L</th><th class="text-center middle-aligned" rowspan="1">P</th><th class="text-center middle-aligned" rowspan="1">T</th></tr></thead>';
    /*for($i=0; $i<count($puskesmas); $i++) {
      $headMarkup .= '<th class="text-center middle-aligned" colspan="2">'.$puskesmas[$i].'</th>';
    }*/
    /*$headMarkup .= '</tr><tr class="table-head">';
    for($i=0; $i<count($puskesmas); $i++) {
      $headMarkup .= '<th class="text-center middle-aligned">Nilai dari Periode Terakhir</th><th class="text-center middle-aligned">Nilai Kumulatif</th>';
    }
    $headMarkup .= '</tr></thead>';*/

    $preMarkup .= '<tbody>';
    while($rowNumCounter <= 7) {
      $postMarkup = '';

      if($rowNumCounter > 1) {
        $preMarkup .= '<tr>';
        $preMarkup .= '<td>'.$rowAlphaCounter.'.</td><td>'.$category[$rowNumCounter-1]['main'].'</td>';
        $preMarkup .= '<td colspan="3"></td></tr>';
        $listPoint = 1;
      }

      foreach($category[$rowNumCounter-1]['sub'] as $key=>$value) {
        $j = 0;
        $tempDataValue = array('l' => 0, 'p' => 0, 't' => 0);
        $isArray = FALSE;

        foreach($puskesmas as $name) {
          if($j == 0) {
            if($rowNumCounter == 1) {
              $postMarkup .= '<tr><td></td><td>'.$key.'</td>';
            }
            else {
              $postMarkup .= '<tr><td></td><td>'.$listPoint.'.&emsp;'.$key.'</td>';
            }
          }

          if( $value == 0 || is_null($value) ) {
            if($stack[$name][$rowNumCounter-1][$key]['l'] === '-' && $stack[$name][$rowNumCounter-1][$key]['p'] === '-') {
              $tempDataValue['l'] = '-';
              $tempDataValue['p'] = '-';
              $tempDataValue['t'] += $stack[$name][$rowNumCounter-1][$key]['t'];
            }
            elseif($stack[$name][$rowNumCounter-1][$key]['l'] === '-' && is_numeric($stack[$name][$rowNumCounter-1][$key]['p'])) {
              $tempDataValue['l'] = '-';
              $tempDataValue['p'] += $stack[$name][$rowNumCounter-1][$key]['p'];
              $tempDataValue['t'] += $stack[$name][$rowNumCounter-1][$key]['p'];
            }
            elseif(is_numeric($stack[$name][$rowNumCounter-1][$key]['l']) && is_numeric($stack[$name][$rowNumCounter-1][$key]['p'])) {
              $tempDataValue['l'] += $stack[$name][$rowNumCounter-1][$key]['l'];
              $tempDataValue['p'] += $stack[$name][$rowNumCounter-1][$key]['p'];
              $tempDataValue['t'] += $stack[$name][$rowNumCounter-1][$key]['l'] + $stack[$name][$rowNumCounter-1][$key]['p'];
            }

            $isArray = FALSE;
          }
          elseif( is_array($value) ) {
            if($j == 0) {
              $postMarkup .= '<td colspan="3"></td></tr>';
            }

            foreach ($value as $subValue) {
              foreach($puskesmas as $name) {
                $tempDataValue['l'] += $stack[$name][$rowNumCounter - 1][$key][$subValue]['l'];
                $tempDataValue['p'] += $stack[$name][$rowNumCounter - 1][$key][$subValue]['p'];
                $tempDataValue['t'] += $stack[$name][$rowNumCounter - 1][$key][$subValue]['l'] + $stack[$name][$rowNumCounter - 1][$key][$subValue]['p'];
              }

              if($j == 0) {
                $postMarkup .= '<tr><td></td><td>&emsp;&emsp;•&emsp;'.$subValue.'</td>';
                $postMarkup .= '<td>'.$tempDataValue['l'].'</td>';
                $postMarkup .= '<td>'.$tempDataValue['p'].'</td>';
                $postMarkup .= '<td>'.$tempDataValue['t'].'</td>';
                $tempDataValue['l'] = 0;
                $tempDataValue['p'] = 0;
                $tempDataValue['t'] = 0;
              }

              $isArray = TRUE;
            }
          }

          $j++;
        }

        if(!$isArray) {
          $postMarkup .= '<td>'.$tempDataValue['l'].'</td>';
          $postMarkup .= '<td>'.$tempDataValue['p'].'</td>';
          $postMarkup .= '<td>'.$tempDataValue['t'].'</td>';
        }

        /*foreach($puskesmas as $name) {
          if($j == 0) {
            if($rowNumCounter == 1) {
              $postMarkup .= '<tr><td></td><td>'.$key.'</td>';
            }
            else {
              $postMarkup .= '<tr><td></td><td>'.$listPoint.'.&emsp;'.$key.'</td>';
            }
          }

          if( is_array($value) ) {
            $postMarkup .= '<td colspan="2"></td></tr>';

            foreach ($value as $subValue) {
              $postMarkup .= '<tr><td></td><td>&emsp;&emsp;•&emsp;'.$subValue.'</td>';

              foreach($puskesmas as $name) {
                $postMarkup .= '<td>'.$stack[$name][$rowNumCounter-1][$key][$subValue]['l'].'</td>';
                $postMarkup .= '<td>'.$stack[$name][$rowNumCounter-1][$key][$subValue]['p'].'</td>';
                $postMarkup .= '<td>'.$stack[$name][$rowNumCounter-1][$key][$subValue]['t'].'</td>';
              }
            }
          }
          else {
            $postMarkup .= '<td>'.$stack[$name][$rowNumCounter-1][$key]['l'].'</td>';
            $postMarkup .= '<td>'.$stack[$name][$rowNumCounter-1][$key]['p'].'</td>';
            $postMarkup .= '<td>'.$stack[$name][$rowNumCounter-1][$key]['t'].'</td>';
          }

          $j++;
        }*/

        if($rowNumCounter > 1) { $listPoint++; }
      }

      $preMarkup .= $postMarkup.'</tr>';
      if($rowNumCounter > 1) { $rowAlphaCounter++; }
      $rowNumCounter++;
    }

    $preMarkup .= '</tbody>';
    $fullRowMarkup = $headMarkup.$preMarkup;

    return $fullRowMarkup;
  }
}