<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

class Kb extends MY_Controller {
  public function __construct() {
    parent::__construct();

    $this->load->model('report_model');
    $this->load->library('report');
    $this->load->helper('form');

    $this->report->set_report_markup_data('page_title', 'Laporan KB');
  }

  public function index() {
    $this->report->set_report_model_data([
      'years' => $this->report_model->fetch_year(),
      'kecamatan' => $this->report_model->fetch_kecamatan()
    ]);

    $this->set_view_data( $this->report->get_report_data() );

    $this->render();
  }

  public function filter() {
    $nulledFound = FALSE;
    $valuePosted = array(
      'period_start'   => array(
        'year' => $this->input->post('f_period_start[0]', TRUE),
        'month' => $this->input->post('f_period_start[1]', TRUE)
      ),
      'period_end'     => array(
        'year' => $this->input->post('f_period_end[0]', TRUE),
        'month' => $this->input->post('f_period_end[1]', TRUE)
      ),
      'kecamatan_code' => $this->input->post('f_kecamatan', TRUE),
      'puskesmas_code' => $this->input->post('f_puskesmas', TRUE)
    );

    foreach($valuePosted as $key=>$value) {
      if( is_array($valuePosted[$key]) ) {
        foreach($value as $subkey=>$subvalue) {
          if($key == 'period_end' && is_null($subvalue) && $valuePosted['period_start']['month'] == 'all' )
            continue;

          if( is_null($subvalue) ) {
            $nulledFound = TRUE;
            break;
          }
        }
      }
      else {
        if( is_null($value) ) {
          if( $key == 'puskesmas_code' && $valuePosted['kecamatan_code'] == 'all' )
            continue;

          $nulledFound = TRUE;
          break;
        }
      }

      if( $nulledFound ) break;
    }

    if( $nulledFound ) {
      $this->session->set_flashdata('errNulledFilter', TRUE);
      redirect( base_url('report/kb') );
    }
    else {
      $this->load->model('report/kb_model');

      if($valuePosted['period_start']['month'] != 'all') {
        $periodId = array(
          // param 1: year, param 2: month number
          'start' => $this->report_model->get_period_id($valuePosted['period_start']['year'], $valuePosted['period_start']['month']),
          'end'   => $this->report_model->get_period_id($valuePosted['period_end']['year'], $valuePosted['period_end']['month'])
        );
      }
      else {
        $periodId = $this->report_model->get_period_id($valuePosted['period_start']['year'], 'all');
        $periodId = array(
          'start' => $periodId[0]['id'],
          'end' => $periodId[count($periodId)-1]['id']
        );
      }

      if( $valuePosted['kecamatan_code'] != 'all' ) {
        if( $valuePosted['puskesmas_code'] != 'all' ) {
          $puskesmasId = $this->report_model->get_puskesmas_id( $valuePosted['puskesmas_code'] );
        }
        else {
          $puskesmasId = $this->report_model->fetch_related_puskesmas_on_kecamatan( $valuePosted['kecamatan_code'] );
        }
      }
      else {
        $puskesmasId = $this->report_model->get_all_puskesmas_id();
      }

      $kbDiagnosis = $this->kb_model->fetch_kb_diagnosis($valuePosted, $periodId, $puskesmasId);
      $monthString = array(1=>'Januari', 2=>'Februari', 3=>'Maret', 4=>'April', 5=>'Mei', 6=>'Juni', 7=>'Juli', 8=>'Agustus', 9=>'September', 10=>'Oktober', 11=>'November', 12=>'Desember');

      if( $valuePosted['period_start']['month'] !== 'all' ) {
        $valuePosted['period_start'] = $monthString[ $valuePosted['period_start']['month'] ].' '.$valuePosted['period_start']['year'];
        $valuePosted['period_end'] = $monthString[ $valuePosted['period_end']['month'] ].' '.$valuePosted['period_end']['year'];
      }
      else {
        $year = $valuePosted['period_start']['year'];
        $valuePosted['period_start'] = $monthString[1].' '.$year;
        $valuePosted['period_end'] = $monthString[12].' '.$year;
      }

      if( $valuePosted['kecamatan_code'] != 'all' ) {
        $valuePosted['kecamatan_name'] = $this->report_model->get_kecamatan_name( $valuePosted['kecamatan_code'] );
        $valuePosted['puskesmas_name'] = $this->report_model->get_puskesmas_name( $puskesmasId );

        if( is_array($valuePosted['puskesmas_name']) ) {
          $tempPuskesmasName = '';

          for($i=0; $i<count( $valuePosted['puskesmas_name'] ); $i++) {
            $tempPuskesmasName .= $valuePosted['puskesmas_name'][$i]['nama_puskesmas'];

            if($i !== count( $valuePosted['puskesmas_name'] )-1)
              $tempPuskesmasName .= ', ';
          }

          $valuePosted['puskesmas_name'] = $tempPuskesmasName;
        }

        $valuePosted['upt_name'] = ( substr($valuePosted['puskesmas_name'], 0, 3) == 'UPT' ) ? $valuePosted['puskesmas_name'] : $this->report_model->get_upt_from_kecamatan( $valuePosted['kecamatan_code'] );
      }
      else {
        $valuePosted['kecamatan_name'] = 'Semua Kecamatan';
        $valuePosted['puskesmas_name'] = '-';
        $valuePosted['upt_name'] = '-';
      }

      $this->set_view_data( $this->report->get_report_data() );
      $this->set_view_data('kb_table', $this->make_kb_stack($kbDiagnosis, $periodId['end']), 'markup');
      $this->set_view_data('diagnosis_data', json_encode($kbDiagnosis), 'model');
      $this->set_view_data('report_spec', $valuePosted, 'model');
      $this->render();
    }
  }

  public function export() {
    $this->load->library('export_xls');

    $diagnosisData = json_decode( $this->input->post('xls_diagnosis_json', TRUE), TRUE );
    $specificationData = json_decode( $this->input->post('xls_specification_json', TRUE), TRUE );
    $currentRow = 1;

    // set active sheet
    $this->export_xls->setActiveSheetIndex(0);
    $sheet = $this->export_xls->getActiveSheet();

    // set sheet title
    $sheet->setTitle('Sheet 1');

    // generate the top section of report
    $sheet->setCellValueByColumnAndRow('A', $currentRow, 'LAPORAN BULANAN KIA-KB');
    $sheet->mergeCells('A1:M1');
    $sheet->insertNewRowBefore($currentRow+1);

    $currentRow = $sheet->getHighestRow() + 1;

    for($i=0; $i<4; $i++) {
      $label = '';
      $value = '';

      if($i == 0) {
        $label = 'Tahun';
        $value = substr( $specificationData['period_start'], strlen( $specificationData['period_start'] ) - 4, strlen( $specificationData['period_start'] ) ).' - '.substr( $specificationData['period_end'], strlen( $specificationData['period_end'] ) - 4, strlen( $specificationData['period_end'] ) );
      }
      elseif($i == 1) {
        $label = 'Periode';
        $value = $specificationData['period_start'].' - '.$specificationData['period_end'];
      }
      elseif($i == 2) {
        $label = 'Puskesmas';
        $value = $specificationData['puskesmas_name'];
      }
      elseif($i == 3) {
        $label = 'UPT Puskesmas';
        $value = $specificationData['upt_name'];
      }

      $sheet->setCellValue('A'.$currentRow, $label);
      $sheet->setCellValue('F'.$currentRow, $value);
      $sheet->mergeCells("A{$currentRow}:D{$currentRow}");
      $sheet->mergeCells("F{$currentRow}:I{$currentRow}");

      $currentRow++;
    }

    $sheet->setCellValueByColumnAndRow('A', $currentRow+1, 'C. KELUARGA BERENCANA');

    $currentRow = $sheet->getHighestRow();
    $sheet->mergeCells( 'A'.$currentRow.':M'.$currentRow );
    $currentRow += 2;

    // generate table head
    $theadLabel = array(
      0 => array(
        'label' => 'NO',
        'col'   => 'A',
        'merge' => 'A'.$currentRow.':A'.($currentRow+3)
      ),
      1 => array(
        'label' => 'RINCIAN KEGIATAN',
        'col'   => 'B',
        'merge' => 'B'.$currentRow.':D'.($currentRow+3)
      ),
      2 => array(
        'label' => 'CAKUPAN PROGRAM',
        'col'   => 'E',
        'merge' => 'E'.$currentRow.':M'.$currentRow
      ),
      3 => array(
        'label' => 'UPT PUSKESMAS PER PUSKESMAS',
        'col'   => 'E',
        'merge' => 'E'.($currentRow+1).':M'.($currentRow+1)
      ),
    );

    /*for($i=0; $i<count($theadLabel); $i++) {
      if($i !== 3)
        $sheet->setCellValue( $theadLabel[$i]['col'].$currentRow, $theadLabel[$i]['label'] );
      else
        $sheet->setCellValue( $theadLabel[$i]['col'].($currentRow+1), $theadLabel[$i]['label'] );

      $sheet->mergeCells( $theadLabel[$i]['merge'] );

      if($i == 3) {
        for($j=0; $j<=count($diagnosisData); $j++) {
          $nameStartCol = ord('E') + ($j*3);
          var_dump(chr($nameStartCol));

          if( $j < count($diagnosisData) )
            $sheet->setCellValue( chr($nameStartCol).($currentRow+2), $diagnosisData[$j]['nama_puskesmas'] );
          else
            $sheet->setCellValue( chr($nameStartCol).($currentRow+2), 'TOTAL UPT PUSK.' );

          $sheet->setCellValue( chr($nameStartCol).($currentRow+3), 'n' );
          $sheet->setCellValue( chr($nameStartCol+1).($currentRow+3), 'kum' );
          $sheet->setCellValue( chr($nameStartCol+2).($currentRow+3), 'gakin' );
          $sheet->mergeCells( chr($nameStartCol).($currentRow+2).':'.chr($nameStartCol+2).($currentRow+2) );
        }
      }
    }*/

    $prevName = '';

    for($i=0; $i<count($theadLabel); $i++) {
      if($i !== 3)
        $sheet->setCellValue( $theadLabel[$i]['col'].$currentRow, $theadLabel[$i]['label'] );
      else
        $sheet->setCellValue( $theadLabel[$i]['col'].($currentRow+1), $theadLabel[$i]['label'] );

      $sheet->mergeCells( $theadLabel[$i]['merge'] );

      if($i == 3) {
        $j = 0;

        foreach($diagnosisData as $data) {
          $nameStartCol = ord('E') + ($j*3);

          if( $prevName == '' || $prevName != $data['nama_puskesmas'] ) {
            $sheet->setCellValue( chr($nameStartCol).($currentRow+2), $data['nama_puskesmas'] );
            $prevName = $data['nama_puskesmas'];

            $sheet->setCellValue( chr($nameStartCol).($currentRow+3), 'n' );
            $sheet->setCellValue( chr($nameStartCol+1).($currentRow+3), 'kum' );
            $sheet->setCellValue( chr($nameStartCol+2).($currentRow+3), 'gakin' );
            $sheet->mergeCells( chr($nameStartCol).($currentRow+2).':'.chr($nameStartCol+2).($currentRow+2) );

            $j++;
          }
        }

        $sheet->setCellValue( chr($nameStartCol).($currentRow+2), 'TOTAL UPT PUSK.' );
        $sheet->setCellValue( chr($nameStartCol).($currentRow+3), 'n' );
        $sheet->setCellValue( chr($nameStartCol+1).($currentRow+3), 'kum' );
        $sheet->setCellValue( chr($nameStartCol+2).($currentRow+3), 'gakin' );
        $sheet->mergeCells( chr($nameStartCol).($currentRow+2).':'.chr($nameStartCol+2).($currentRow+2) );
      }
    }

    $currentRow = $sheet->getHighestRow() + 1;

    // generate table content
    $rowLabel = array(
      1 => array( 'val' => 'Jumlah KB Postpartum s.d. 42 Hari' ),
      2 => array(
        'val' => 'Jumlah Pelayanan KB Baru',
        'sub' => array(
          'a' => 'Pil',
          'b' => 'Suntik',
          'c' => 'AKDR',
          'd' => 'Implant / Norplant',
          'e' => 'Kondom / Vaginal',
          'f' => 'MOW',
          'g' => 'MOP',
          'h' => 'Lain-lain'
        )
      ),
      3 => array( 'val' => 'Jumlah Pelayanan KB Lama' ),
      4 => array(
        'val' => 'Pelayanan KB Aktif',
        'sub' => array(
          'a' => array(
            'val' => 'KB Aktif',
            1 => 'Pil',
            2 => 'Suntik',
            3 => 'AKDR',
            4 => 'Implant / Norplant',
            5 => 'Kondom / Vaginal',
            6 => 'MOW',
            7 => 'MOP',
            8 => 'Lain-lain'
          ),
          'b' => array(
            'val' => 'Efek Samping',
            1 => 'Pil',
            2 => 'Suntik',
            3 => 'AKDR',
            4 => 'Implant / Norplant',
            5 => 'Kondom / Vaginal',
            6 => 'MOW',
            7 => 'MOP',
            8 => 'Lain-lain'
          ),
          'c' => array(
            'val' => 'Komplikasi',
            1 => 'Pil',
            2 => 'Suntik',
            3 => 'AKDR',
            4 => 'Implant / Norplant',
            5 => 'Kondom / Vaginal',
            6 => 'MOW',
            7 => 'MOP',
            8 => 'Lain-lain'
          ),
          'd' => array(
            'val' => 'Kegagalan',
            1 => 'Pil',
            2 => 'Suntik',
            3 => 'AKDR',
            4 => 'Implant / Norplant',
            5 => 'Kondom / Vaginal',
            6 => 'MOW',
            7 => 'MOP',
            8 => 'Lain-lain'
          ),
          'e' => array(
            'val' => 'Drop Out',
            1 => 'Pil',
            2 => 'Suntik',
            3 => 'AKDR',
            4 => 'Implant / Norplant',
            5 => 'Kondom / Vaginal',
            6 => 'MOW',
            7 => 'MOP',
            8 => 'Lain-lain'
          ),
          'f' => array(
            'val' => 'PUS Miskin ber-KB',
            1 => 'Pil',
            2 => 'Suntik',
            3 => 'AKDR',
            4 => 'Implant / Norplant',
            5 => 'Kondom / Vaginal',
            6 => 'MOW / MOP'
          ),
          'g' => array(
            'val' => 'PUS 4T ber-KB',
            1 => 'Pil',
            2 => 'Suntik',
            3 => 'AKDR',
            4 => 'Implant / Norplant',
            5 => 'Kondom / Vaginal',
            6 => 'MOW / MOP'
          )
        )
      ),
      5 => array(
        'val' => 'Pemantauan Persediaan Alat Kontrasepsi',
        'sub' => array(
          'a' => array(
            'val' => 'Stok Tahun Lalu',
            1 => 'Pil',
            2 => 'Suntik',
            3 => 'AKDR',
            4 => 'Implant / Norplant',
            5 => 'Kondom / Vaginal'
          ),
          'b' => array(
            'val' => 'Penerimaan Tahun Ini',
            1 => 'Pil',
            2 => 'Suntik',
            3 => 'AKDR',
            4 => 'Implant / Norplant',
            5 => 'Kondom / Vaginal'
          ),
          'c' => array(
            'val' => 'Penggunaan',
            1 => 'Pil',
            2 => 'Suntik',
            3 => 'AKDR',
            4 => 'Implant / Norplant',
            5 => 'Kondom / Vaginal'
          ),
          'd' => array(
            'val' => 'Sisa',
            1 => 'Pil',
            2 => 'Suntik',
            3 => 'AKDR',
            4 => 'Implant / Norplant',
            5 => 'Kondom / Vaginal'
          )
        )
      )
    );
    $rowValue = array();
    $highestPeriod = 0;

    foreach($rowLabel as $k1=>$v1) {
      $sheet->setCellValue('A'.$currentRow, $k1);
      $sheet->setCellValue('B'.$currentRow, $v1['val']);
      $sheet->mergeCells('B'.$currentRow.':D'.$currentRow);

      $currentRow++;

      if(array_key_exists('sub', $v1)) {
        foreach($v1['sub'] as $k2=>$v2) {

          $sheet->setCellValue('B'.$currentRow, $k2);

          if( !is_array($v2) ) {
            $sheet->setCellValue('C'.$currentRow, $v2);
            $sheet->mergeCells('C'.$currentRow.':D'.$currentRow);

            $currentRow++;
          }
          else {
            $sheet->setCellValue('C'.$currentRow, $v2['val']);
            $sheet->mergeCells('C'.$currentRow.':D'.$currentRow);

            $currentRow++;

            for($i=1; $i<=count($v2)-1; $i++) {
              $sheet->setCellValue('C'.$currentRow, $i);
              $sheet->setCellValue('D'.$currentRow, $v2[$i]);

              $currentRow++;
            }
          }
        }
      }

      $currentRow++;
    }

    for($i=0; $i<count($diagnosisData); $i++) {
      if( (int)$diagnosisData[$i]['ID_PERIODE'] > $highestPeriod )
        $highestPeriod = (int)$diagnosisData[$i]['ID_PERIODE'];

      if( $i == count($diagnosisData)-1 ) {
        foreach($diagnosisData as $v1) {
          $currentPuskesmas = $v1['nama_puskesmas'];

          if( !array_key_exists($v1['nama_puskesmas'], $rowValue) ) {
            $rowValue[$currentPuskesmas] = array(
              'n' => array(),
              'kum' => array()
            );
          }

          $valueCol = ($v1['ID_PERIODE'] == $highestPeriod) ? 'kum' : 'n';

          unset( $v1['nama_puskesmas'] );
          unset( $v1['ID_PUSKESMAS'] );
          unset( $v1['ID_PERIODE'] );

          $tempData = array_values($v1);
          $tempCount = count($tempData);

          for($j=0; $j<$tempCount; $j++) {
            if( $j == 0 && (empty($rowValue[$currentPuskesmas]['n']) || empty($rowValue[$currentPuskesmas]['kum'])) ) {
              for($k=0; $k<$tempCount; $k++) {
                $rowValue[$currentPuskesmas]['n'][$k] = 0;
                $rowValue[$currentPuskesmas]['kum'][$k] = 0;
              }
            }

            if( $valueCol == 'n' )
              $rowValue[$currentPuskesmas]['kum'][$j] += $tempData[$j];

            $rowValue[$currentPuskesmas][$valueCol][$j] += $tempData[$j];
          }
        }
      }
    }

    $initRowAdder = 14;
    $stepCol = 'E';
    $rowSkip = array( 15, 25, 27, 38, 47, 56, 65, 74, 81, 88, 96, 102, 108 );
    $rowValue['total'] = array( 'n' => array(), 'kum' => array() );

    foreach($rowValue as $k1=>$v1) {
      if($k1 !== 'total') {
        for($i=0; $i<count($v1['n']); $i++) {
          if( !array_key_exists($i, $rowValue['total']['n']) ) {
            $rowValue['total']['n'][$i] = 0;
            $rowValue['total']['kum'][$i] = 0;
          }

          $rowValue['total']['n'][$i] += $v1['n'][$i];
          $rowValue['total']['kum'][$i] += $v1['kum'][$i];

          if( in_array( $i+$initRowAdder, $rowSkip ) ) {
            if( $i+$initRowAdder == 27 || $i+$initRowAdder == 88 ) {
              $initRowAdder += 3;
            }
            elseif( $i+$initRowAdder == 15 ) {
              $initRowAdder += 2;
            }
            else {
              $initRowAdder += 1;
            }
          }

          $sheet->setCellValue($stepCol . ($i + $initRowAdder), $v1['n'][$i]);
          $sheet->setCellValue(chr( ord($stepCol)+1 ).($i+$initRowAdder), $v1['kum'][$i]);
          $sheet->setCellValue(chr( ord($stepCol)+2 ).($i+$initRowAdder), '-');
        }

        $stepCol = chr( ord($stepCol)+3 );
        $initRowAdder = 14;
      }
    }

    for($j=0; $j<count($rowValue['total']['n']); $j++) {
      if( in_array( $j+$initRowAdder, $rowSkip ) ) {
        if( $j+$initRowAdder == 27 || $j+$initRowAdder == 88 ) {
          $initRowAdder += 3;
        }
        elseif( $j+$initRowAdder == 15 ) {
          $initRowAdder += 2;
        }
        else {
          $initRowAdder += 1;
        }
      }

      $sheet->setCellValue( $stepCol.($j+$initRowAdder), $rowValue['total']['n'][$j] );
      $sheet->setCellValue( chr( ord($stepCol)+1 ).($j+$initRowAdder), $rowValue['total']['kum'][$j] );
      $sheet->setCellValue( chr( ord($stepCol)+2 ).($j+$initRowAdder), '-' );
    }

    // post sheet styling
    $alignment = $sheet->getStyle('A10:'.$sheet->getHighestColumn().'13')->getAlignment();
    $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $alignment->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    $alignment = $sheet->getStyle( 'A14:A'.$sheet->getHighestRow() )->getAlignment();
    $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $alignment->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    $alignment = $sheet->getStyle( 'B14:D'.$sheet->getHighestRow() )->getAlignment();
    $alignment->setWrapText(true);
    $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $alignment->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    $alignment = $sheet->getStyle( 'E14:'.$sheet->getHighestColumn().$sheet->getHighestRow() )->getAlignment();
    $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $alignment->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    $sheet->getColumnDimension('A')->setWidth(4);
    $sheet->getColumnDimension('B')->setWidth(4);
    $sheet->getColumnDimension('C')->setWidth(4);
    $sheet->getColumnDimension('D')->setWidth(30);

    for($char='E'; $char<=chr(ord($stepCol)+2); $char++) {
      $sheet->getColumnDimension($char)->setWidth(10);
    }

    //exit();

    // set what the name of the file to be downloaded
    $fileName = 'test_xls.xls';
    // set related headers and download the generated file
    // tell the browser about the xls mime-type
    header('Content-Type: application/vnd.ms-excel');
    // attach the generated xls file
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    // don't cache the file
    header('Cache-Control: max-age=0');
    // write XLS object and set to PHP output
    $objWriter = PHPExcel_IOFactory::createWriter($this->export_xls, 'Excel5');
    $objWriter->save('php://output');

    // and your download will start :)
  }

  /*private function in_array_loop($needle, $haystack, $strict=false) {
    foreach ($haystack as $item) {
      if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_loop($needle, $item, $strict))) {
        return true;
      }
    }

    return false;
  }*/

  private function make_kb_stack($kbData, $endPeriodId) {
    $options = array(
      0 => array('Pil', 'Suntik', 'AKDR', 'Implant / Norplant', 'Kondom / Vaginal', 'MOW', 'MOP', 'Lain-lain'),
      1 => array('Pil', 'Suntik', 'AKDR', 'Implant / Norplant', 'Kondom / Vaginal', 'MOW / MOP'),
      2 => array('Pil', 'Suntik', 'AKDR', 'Implant / Norplant', 'Kondom / Vaginal')
    );
    $category = array(
      0 => array(
        'main'     => 'Jumlah KB <em>Postpartum</em> s.d. 42 Hari'
      ),
      1 => array(
        'main'     => 'Jumlah Pelayanan KB Baru',
        'sub'      => $options[0]
      ),
      2 => array(
        'main'     => 'Jumlah Pelayanan KB Lama',
      ),
      3 => array(
        'main'     => 'Pelayanan KB Aktif',
        'sub'      => array(
          'KB Aktif'          => $options[0],
          'Efek Samping'      => $options[0],
          'Komplikasi'        => $options[0],
          'Kegagalan'         => $options[0],
          'Drop Out'          => $options[0],
          'PUS Miskin ber-KB' => $options[1],
          'PUS 4T ber-KB'     => $options[1]
        )
      ),
      4 => array(
        'main'     => 'Pemantauan Persediaan Alat Kontrasepsi',
        'sub'      => array(
          'Stok Tahun Lalu'       => $options[2],
          'Penerimaan Tahun Ini'  => $options[2],
          'Penggunaan'            => $options[2],
          'Sisa'                  => $options[2]
        )
      )
    );
    $stack = array();
    $highestPeriodId = 0;

    for($k=0; $k<count($kbData); $k++) {
      if( (int)$kbData[$k]['ID_PERIODE'] > $highestPeriodId )
        $highestPeriodId = (int)$kbData[$k]['ID_PERIODE'];
    }

    foreach($kbData as $dataValue) {
      $currentPeriodIdCheck = $dataValue['ID_PERIODE'];

      if( !array_key_exists($dataValue['nama_puskesmas'], $stack) ) {
        $stack[ $dataValue['nama_puskesmas'] ] = array();

        for($i=0; $i<count($category); $i++) {
          if( !array_key_exists('sub', $category[$i]) ) {
            $stack[ $dataValue['nama_puskesmas'] ][$i]['row_as_value'] = array(
              'n'   => 0,
              'kum' => 0,
              '%'   => '-'
            );
          }
          else {
            foreach( $category[$i]['sub'] as $subKey=>$subValue ) {
              if( !is_numeric($subKey) )
                $stack[ $dataValue['nama_puskesmas'] ][$i][$subKey] = array();

              for($j=0; $j<count($subValue); $j++) {
                if($i == 1) {
                  $stack[ $dataValue['nama_puskesmas'] ][$i][$subValue] = array(
                    'n'   => 0,
                    'kum' => 0,
                    '%'   => '-'
                  );
                }
                else {
                  $stack[ $dataValue['nama_puskesmas'] ][$i][$subKey][$subValue[$j]] = array(
                    'n'   => 0,
                    'kum' => 0,
                    '%'   => '-'
                  );
                }
              }
            }
          }
        }
      }

      for($j=0; $j<count($category); $j++) {
        $this->push_options_value($stack, $j, 'kum', $dataValue);

        if( (int)$currentPeriodIdCheck < $highestPeriodId )
          $this->push_options_value($stack, $j, 'n', $dataValue);
      }
    }

    $table = $this->prepare_table($stack, $category);

    return $table;
  }

  private function push_options_value(&$stack, $counter, $type, $data) {
    switch($counter) {
      case 0:
        $stack[ $data['nama_puskesmas'] ][$counter]['row_as_value'][$type] += $data['AKS_POST_PARTUM_42_H'];
      break;

      case 1:
        $stack[ $data['nama_puskesmas'] ][$counter]['Pil'][$type] += $data['AKS_BARU_PIL'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Suntik'][$type] += $data['AKS_BARU_SUNTIK'];
        $stack[ $data['nama_puskesmas'] ][$counter]['AKDR'][$type] += $data['AKS_BARU_AKDR'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Implant / Norplant'][$type] += $data['AKS_BARU_IMPLANT'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Kondom / Vaginal'][$type] += $data['AKS_BARU_KONDOM'];
        $stack[ $data['nama_puskesmas'] ][$counter]['MOW'][$type] += $data['AKS_BARU_MOW'];
        $stack[ $data['nama_puskesmas'] ][$counter]['MOP'][$type] += $data['AKS_BARU_MOP'];
        $stack[ $data['nama_puskesmas'] ][$counter]['Lain-lain'][$type] += $data['AKS_BARU_LAIN'];
      break;

      case 2:
        $stack[ $data['nama_puskesmas'] ][$counter]['row_as_value'][$type] += $data['AKS_LAMA'];
      break;

      case 3:
        foreach($stack[ $data['nama_puskesmas'] ][$counter] as $stackKey=>$stackValue) {
          if($stackKey == 'KB Aktif') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Pil'][$type] += $data['AKS_AKTIF_PIL'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Suntik'][$type] += $data['AKS_AKTIF_SUNTIK'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['AKDR'][$type] += $data['AKS_AKTIF_AKDR'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Implant / Norplant'][$type] += $data['AKS_AKTIF_IMPLANT'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Kondom / Vaginal'][$type] += $data['AKS_AKTIF_KONDOM'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['MOW'][$type] += $data['AKS_AKTIF_MOW'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['MOP'][$type] += $data['AKS_AKTIF_MOP'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Lain-lain'][$type] += $data['AKS_AKTIF_LAIN'];
          }
          elseif($stackKey == 'Efek Samping') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Pil'][$type] += $data['EFEK_SAMPING_PIL'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Suntik'][$type] += $data['EFEK_SAMPING_SUNTIK'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['AKDR'][$type] += $data['EFEK_SAMPING_AKDR'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Implant / Norplant'][$type] += $data['EFEK_SAMPING_IMPLANT'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Kondom / Vaginal'][$type] += $data['EFEK_SAMPING_KONDOM'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['MOW'][$type] += $data['EFEK_SAMPING_MOW'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['MOP'][$type] += $data['EFEK_SAMPING_MOP'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Lain-lain'][$type] += $data['EFEK_SAMPING_LAIN'];
          }
          elseif($stackKey == 'Komplikasi') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Pil'][$type] += $data['KOMPLIKASI_PIL'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Suntik'][$type] += $data['KOMPLIKASI_SUNTIK'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['AKDR'][$type] += $data['KOMPLIKASI_AKDR'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Implant / Norplant'][$type] += $data['KOMPLIKASI_IMPLANT'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Kondom / Vaginal'][$type] += $data['KOMPLIKASI_KONDOM'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['MOW'][$type] += $data['KOMPLIKASI_MOW'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['MOP'][$type] += $data['KOMPLIKASI_MOP'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Lain-lain'][$type] += $data['KOMPLIKASI_LAIN'];
          }
          elseif($stackKey == 'Kegagalan') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Pil'][$type] += $data['GAGAL_PIL'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Suntik'][$type] += $data['GAGAL_SUNTIK'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['AKDR'][$type] += $data['GAGAL_AKDR'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Implant / Norplant'][$type] += $data['GAGAL_IMPLANT'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Kondom / Vaginal'][$type] += $data['GAGAL_KONDOM'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['MOW'][$type] += $data['GAGAL_MOW'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['MOP'][$type] += $data['GAGAL_MOP'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Lain-lain'][$type] += $data['GAGAL_LAIN'];
          }
          elseif($stackKey == 'Drop Out') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Pil'][$type] += $data['DO_PIL'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Suntik'][$type] += $data['DO_SUNTIK'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['AKDR'][$type] += $data['DO_AKDR'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Implant / Norplant'][$type] += $data['DO_IMPLANT'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Kondom / Vaginal'][$type] += $data['DO_KONDOM'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['MOW'][$type] += $data['DO_MOW'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['MOP'][$type] += $data['DO_MOP'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Lain-lain'][$type] += $data['DO_LAIN'];
          }
          elseif($stackKey == 'PUS Miskin ber-KB') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Pil'][$type] += $data['PUS_KIN_PIL'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Suntik'][$type] += $data['PUS_KIN_SUNTIK'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['AKDR'][$type] += $data['PUS_KIN_AKDR'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Implant / Norplant'][$type] += $data['PUS_KIN_IMPLANT'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Kondom / Vaginal'][$type] += $data['PUS_KIN_KONDOM'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['MOW / MOP'][$type] += $data['PUS_KIN_MOW_MOP'];
          }
          elseif($stackKey == 'PUS 4T ber-KB') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Pil'][$type] += $data['PUS_4T_PIL'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Suntik'][$type] += $data['PUS_4T_SUNTIK'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['AKDR'][$type] += $data['PUS_4T_AKDR'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Implant / Norplant'][$type] += $data['PUS_4T_IMPLANT'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Kondom / Vaginal'][$type] += $data['PUS_4T_KONDOM'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['MOW / MOP'][$type] += $data['PUS_4T_MOW_MOP'];
          }
        }
      break;

      case 4:
        foreach($stack[ $data['nama_puskesmas'] ][$counter] as $stackKey=>$stackValue) {
          if($stackKey == 'Stok Tahun Lalu') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Pil'][$type] += $data['STOK_PIL'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Suntik'][$type] += $data['STOK_SUNTIK'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['AKDR'][$type] += $data['STOK_AKDR'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Implant / Norplant'][$type] += $data['STOK_IMPLANT'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Kondom / Vaginal'][$type] += $data['STOK_KONDOM'];
          }
          elseif($stackKey == 'Penerimaan Tahun Ini') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Pil'][$type] += $data['TERIMA_PIL'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Suntik'][$type] += $data['TERIMA_SUNTIK'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['AKDR'][$type] += $data['TERIMA_AKDR'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Implant / Norplant'][$type] += $data['TERIMA_IMPLANT'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Kondom / Vaginal'][$type] += $data['TERIMA_KONDOM'];
          }
          elseif($stackKey == 'Penggunaan') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Pil'][$type] += $data['PEMAKAIAN_PIL'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Suntik'][$type] += $data['PEMAKAIAN_SUNTIK'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['AKDR'][$type] += $data['PEMAKAIAN_AKDR'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Implant / Norplant'][$type] += $data['PEMAKAIAN_IMPLANT'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Kondom / Vaginal'][$type] += $data['PEMAKAIAN_KONDOM'];
          }
          elseif($stackKey == 'Sisa') {
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Pil'][$type] += $data['SISA_PIL'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Suntik'][$type] += $data['SISA_SUNTIK'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['AKDR'][$type] += $data['SISA_AKDR'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Implant / Norplant'][$type] += $data['SISA_IMPLANT'];
            $stack[ $data['nama_puskesmas'] ][$counter][$stackKey]['Kondom / Vaginal'][$type] += $data['SISA_KONDOM'];
          }
        }
      break;
    }
  }

  private function prepare_table($stack, $category) {
    $preMarkup = '';
    $puskesmas = array_keys($stack);
    $rowNumCounter = 1;

    $headMarkup = '<thead><tr class="table_head"><th class="text-center middle-aligned" rowspan="2">No.</th><th class="text-center middle-aligned" rowspan="2">Rincian Kegiatan</th>';
    for($i=0; $i<count($puskesmas); $i++) {
      $headMarkup .= '<th class="text-center middle-aligned" colspan="3">'.$puskesmas[$i].'</th>';

      if($i == count($puskesmas) - 1) {
        $headMarkup .= '<th class="text-center middle-aligned" colspan="3">TOTAL UPT PUSKESMAS</th>';
      }
    }
    $headMarkup .= '</tr><tr class="table-head">';
    for($i=0; $i<=count($puskesmas); $i++) {
      $headMarkup .= '<th class="text-center middle-aligned">Total <em>n</em> Periode Lalu</th><th class="text-center middle-aligned">Nilai Kumulatif</th><th class="text-center middle-aligned">%</th>';
    }
    $headMarkup .= '</tr></thead>';
    $preMarkup .= '<tbody>';
    while($rowNumCounter <= 5) {
      $preMarkup .= '<tr>';
      $preMarkup .= '<td>'.$rowNumCounter.'.</td><td>'.$category[$rowNumCounter-1]['main'].'</td>';
      $postMarkup = '';
      $tempN = 0;
      $tempKum = 0;

      if($rowNumCounter == 1 || $rowNumCounter == 3) {
        foreach($puskesmas as $name) {
          foreach( $stack[$name][$rowNumCounter-1]['row_as_value'] as $key=>$value) {
            $postMarkup .= '<td>'.$value.'</td>';

            if($key == 'n')
              $tempN += $value;
            elseif($key == 'kum')
              $tempKum += $value;
            else
              continue;
          }
        }

        $postMarkup .= '<td>'.$tempN.'</td><td>'.$tempKum.'</td><td>-</td>';
      }
      elseif($rowNumCounter == 2) {
        $preMarkup .= '<td colspan="'.(count($puskesmas)*3 + 3).'"></td></tr>';
        $listPoint = 'A';

        foreach($category[$rowNumCounter-1]['sub'] as $value) {
          $j = 0;

          foreach($puskesmas as $name) {
            if($j == 0)
              $postMarkup .= '<tr><td></td><td>'.$listPoint.'.&emsp;'.$value.'</td>';

            $postMarkup .= '<td>'.$stack[$name][$rowNumCounter-1][$value]['n'].'</td>';
            $postMarkup .= '<td>'.$stack[$name][$rowNumCounter-1][$value]['kum'].'</td>';
            $postMarkup .= '<td>'.$stack[$name][$rowNumCounter-1][$value]['%'].'</td>';

            $tempN += $stack[$name][$rowNumCounter-1][$value]['n'];
            $tempKum += $stack[$name][$rowNumCounter-1][$value]['kum'];

            $j++;
          }

          $postMarkup .= '<td>'.$tempN.'</td><td>'.$tempKum.'</td><td>-</td>';
          $tempN = 0;
          $tempKum = 0;

          $listPoint++;
        }
      }
      elseif($rowNumCounter == 4 || $rowNumCounter == 5) {
        $preMarkup .= '<td colspan="'.(count($puskesmas)*3 + 3).'"></td></tr>';
        $listPoint = 'A';

        foreach($category[$rowNumCounter-1]['sub'] as $key=>$value) {
          $postMarkup .= '<tr><td></td><td>'.$listPoint.'.&emsp;'.$key.'</td><td colspan="'.(count($puskesmas)*3 + 3).'"></td></tr>';

          foreach ($category[$rowNumCounter-1]['sub'][$key] as $subValue) {
            $postMarkup .= '<tr><td></td><td>&emsp;&emsp;•&emsp;'.$subValue.'</td>';

            foreach($puskesmas as $name) {
              $postMarkup .= '<td>'.$stack[$name][$rowNumCounter-1][$key][$subValue]['n'].'</td>';
              $postMarkup .= '<td>'.$stack[$name][$rowNumCounter-1][$key][$subValue]['kum'].'</td>';
              $postMarkup .= '<td>'.$stack[$name][$rowNumCounter-1][$key][$subValue]['%'].'</td>';

              $tempN += $stack[$name][$rowNumCounter-1][$key][$subValue]['n'];
              $tempKum += $stack[$name][$rowNumCounter-1][$key][$subValue]['kum'];
            }

            $postMarkup .= '<td>'.$tempN.'</td><td>'.$tempKum.'</td><td>-</td>';
            $tempN = 0;
            $tempKum = 0;
          }

          $listPoint++;
        }
      }

      $preMarkup .= $postMarkup.'</tr>';
      $rowNumCounter++;
    }

    $preMarkup .= '</tbody>';
    $fullRowMarkup = $headMarkup.$preMarkup;

    return $fullRowMarkup;
  }
}
