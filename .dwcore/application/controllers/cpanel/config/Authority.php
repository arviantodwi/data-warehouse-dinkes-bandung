<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

class Authority extends MY_Controller {
  private $data = array(
    'markup' => array(),
    'model' => array()
  );
  
  public function __construct() {
    parent::__construct();
    
    $this->data['markup'] = array(
      'active_view' => 'cpanel_view',
      'body_class'  => 'hold-transition skin-green-light sidebar-mini',
      'page_title'  => 'Otoritas Pengguna'
    );
    
    $this->set_view_data($this->data, 'markup');
    $this->load->model('users_model');
  }
  public function index() {
    $accountType = $this->users_model->fetch_global_account_type();
    $typeCount = count($accountType);
    
    for($i=0; $i<$typeCount; $i++) {
      $min = $i;
      
      for($j=$i+1; $j<$typeCount; $j++) {
        if( strcasecmp($accountType[$j]['type_name'], $accountType[$min]['type_name']) < 0 ) {
          $min = $j;
        }
      }
      
      if($min != $i) {
        $pointer = $accountType[$i]; 
        $accountType[$i] = $accountType[$min];
        $accountType[$min] = $pointer;
      }
    }
    
    $this->set_view_data('account_type', $accountType, 'model');
    $this->set_view_data('privileges', $this->users_model->fetch_authority(), 'model');
    $this->load->helper('form');
    $this->render();
  }
  
  public function verify() {
    $accountId = $this->input->post('priv_account_type', TRUE);
    $privileges = array(
      'dashboard'    => $this->input->post('priv_opt_dashboard', TRUE),
      'import'       => $this->input->post('priv_opt_import', TRUE),
      'report'       => $this->input->post('priv_opt_report', TRUE),
      'config'       => $this->input->post('priv_opt_config', TRUE),
      'import_sikda' => $this->input->post('priv_opt_sikda', TRUE),
      'import_excel' => $this->input->post('priv_opt_excel', TRUE)
    );
    
    if( !empty($privileges['import']) && ( empty($privileges['import_sikda']) && empty($privileges['import_excel']) ) ) {
      $privileges['import'] = null;
    }
    
    if( $accountId === 'none' || empty($accountId) ) {
      $this->session->set_flashdata('authConfigError', TRUE);
    }
    else {
      if( count( array_filter($privileges, function($elem) { return !empty($elem); }) ) < 1 ) {
        $this->session->set_flashdata('authConfigNeedOpts', TRUE);
      }
      else {
        $this->load->model('users_model');
        $this->users_model->update_authority($accountId, $privileges);
        $this->session->set_flashdata('authConfigSucceed', TRUE);
      }
    }
    
    redirect( site_url('config/authority') );
  }
}