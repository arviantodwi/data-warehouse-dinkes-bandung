<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

class Registration extends MY_Controller {
  private $data = array(
    'markup' => array(),
    'model' => array()
  );
  
  public function __construct() {
    parent::__construct();
    
    $this->data['markup'] = array(
      'active_view' => 'cpanel_view',
      'body_class'  => 'hold-transition skin-green-light sidebar-mini',
      'page_title'  => 'Registrasi Pengguna'
    );
    
    $this->set_view_data($this->data, 'markup');
  }
  
  
  public function index() {
    $this->load->helper('form');
    $this->load->model('users_model');
    $this->load->model('report/spreadsheet_model', 'ss_model');
    
    $puskesmasList = $this->ss_model->fetch_puskesmas(0);
    $accountType = $this->users_model->fetch_global_account_type();
    $typeCount = count($accountType);
    
    for($i=0; $i<$typeCount; $i++) {
      $min = $i;
      
      for($j=$i+1; $j<$typeCount; $j++) {
        if( strcasecmp($accountType[$j]['type_name'], $accountType[$min]['type_name']) < 0 ) {
          $min = $j;
        }
      }
      
      if($min != $i) {
        $pointer = $accountType[$i]; 
        $accountType[$i] = $accountType[$min];
        $accountType[$min] = $pointer;
      }
    }
    
    $this->set_view_data('account_type', $accountType, 'model');
    $this->set_view_data('puskesmas', $puskesmasList, 'model');
    $this->render();
  }
  
  public function verify() {
    $postedData = array(  
      'real_name'    => $this->input->post('reg_real_name', TRUE),
      'nip'          => $this->input->post('reg_nip', TRUE),
      'username'     => $this->input->post('reg_username', TRUE),
      'password'     => $this->input->post('reg_userpass[0]', TRUE),
      'repass'       => $this->input->post('reg_userpass[1]', TRUE),
      'account_type' => $this->input->post('reg_account_type', TRUE),
      'puskesmas_id' => ( (int)$this->input->post('reg_account_type', TRUE) !== 4 ) ? 0 : $this->input->post('reg_puskesmas'),
      'gender'       => $this->input->post('reg_gender', TRUE)
    );
    $nulledPostFound = FALSE;
    
    foreach($postedData as $key => $value) {
      if( is_null($value) || $value === 'none') {
        $nulledPostFound = TRUE;
        break;
      }
    }
    
    if($nulledPostFound || $postedData['password'] !== $postedData['repass']) {
      $this->session->set_flashdata('registerError', TRUE);
    }
    else {
      $this->load->model('users_model');
      $this->users_model->insert_user($postedData);
      $this->session->set_flashdata('registerSuccess', TRUE);
    }
    
    redirect( base_url('config/registration') );
  }
}