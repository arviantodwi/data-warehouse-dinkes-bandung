<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

/**
 * Class Login
 */
class Login extends MY_Controller {
  /**
   * @var array
   */
  private $data = array(
    'markup' => array(),
    'model'  => array()
  );

  /**
   * Login constructor.
   */
  public function __construct() {
    parent::__construct();
    
    $this->data['markup'] = array(
      'active_view' => 'login_view',
      'body_class'  => 'hold-transition login-page',
      'sub_view'    => null,
      'page_title'  => 'Autentikasi'
    );
    
    $this->set_view_data($this->data);
    $this->load->model('report/lb1_model');
  }

  /**
   * 
   */
  public function index() {
    $this->load->helper('form');
		$this->render();
  }

  /**
   *
   */
  public function verify() {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('login_uname', 'username', 'required|trim|regex_match[/^[a-z0-9\_\.]{5,}$/]');
    $this->form_validation->set_rules('login_upass', 'password', 'required|trim');
    
    if( $this->form_validation->run() == FALSE ) {
      $this->session->set_flashdata('validationError', TRUE);
      redirect( base_url('auth/login') );
    }
    else {
      $this->load->model('users_model');
      
      $username = $this->input->post('login_uname', TRUE);
      $password = $this->input->post('login_upass', TRUE);
      $userId = $this->users_model->fetch_user_id($username);
      
      if($userId !== null) {
        $this->users_model->fetch_all_user_data($userId);
        $hashMap = $this->users_model->get_user_data('hash_map');
        
        $this->load->library('crypto');
        $this->crypto->retrieve_salt($hashMap);
        $hash = $this->crypto->make_password_hash($password, 'sha256');
        
        if( $hash == $this->users_model->get_user_data('password') ) {
          if( !isset($_SESSION['credential']) ) {
            $publicData = (object) array(
              'username'     => $this->users_model->get_user_data('username'),
              'user_nip'     => $this->users_model->get_user_data('user_nip'),
              'real_name'    => $this->users_model->get_user_data('real_name')
            );
            $cookieData = array(
              'name'   => 'public_credential',
              'value'  => base64_encode( json_encode($publicData) ),
              'expire' => time() + 2*60*60,
              'domain' => '',
              'path'   => '/'
            );
            $credentialData = array(
              'user_id'      => $userId,
              'account_type' => $this->users_model->get_user_data('account_type'),
              'puskesmas_id' => $this->users_model->get_user_data('puskesmas_id'),
              'last_active'  => time(),
              'is_login'     => TRUE
            );
            
            $_SESSION['credential'] = $credentialData;
            
            set_cookie($cookieData);
          }
          else {
            $_SESSION['credential']['last_active'] = time();
            $_SESSION['credential']['is_login'] = TRUE;
          }
          
          if( (int)$_SESSION['credential']['account_type'] === 4 )
            redirect( base_url('import/spreadsheet') );
          else
            redirect( base_url('dashboard') );
        }
        else {
          $this->session->set_flashdata('validationError', TRUE);
          redirect( base_url('auth/login') );
        }
      }
      else {
        $this->session->set_flashdata('validationError', TRUE);
        redirect( base_url('auth/login') );
      }
    }
    // debug else
    // else {
    //   $this->load->library('crypto');
    //   $password = $this->input->post('login_upass');
    //   $hash = $this->crypto->make_password_hash($password, 'sha256');
      
    //   echo "<textarea readonly style='font-family:monospace; width: 100%; height: 99%; background: #eee; border: solid 1px #989898; padding: 7px 13px;'>";
    //   echo '=============================================================='.PHP_EOL."RAW DATA".PHP_EOL."==============================================================".PHP_EOL;
    //   echo 'Password    : '.$password.PHP_EOL;
    //   echo 'Salt        : '.$this->crypto->get_salt().PHP_EOL;
    //   echo 'Combination : '.$this->crypto->get_combination().PHP_EOL;
    //   echo "==============================================================".PHP_EOL."ENCRYPTED DATA".PHP_EOL."==============================================================".PHP_EOL;
    //   echo 'Hash        : '.$hash.PHP_EOL;
    //   echo 'Length      : '.strlen($hash).PHP_EOL;
    //   echo "==============================================================".PHP_EOL."RETRIEVE MATCH HASH".PHP_EOL."==============================================================".PHP_EOL;
    //   echo 'Old Comb.   : '.$this->crypto->get_combination().PHP_EOL;
    //   echo 'Old Salt    : '.$this->crypto->get_salt().PHP_EOL;
    //   $salt = $this->crypto->retrieve_salt( $this->crypto->get_combination() );
    //   echo 'Solved Salt : '.$salt.PHP_EOL;
    //   echo 'Old Hash    : '.$hash.PHP_EOL;
    //   $hash = $this->crypto->make_password_hash($password, 'sha256');
    //   echo 'Solved Hash : '.$hash.PHP_EOL;
    //   echo "</textarea>";
    // }
  }
}