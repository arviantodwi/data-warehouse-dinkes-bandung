<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

/**
 * Class Logout
 */
class Logout extends CI_Controller {
  /**
   * Logout constructor.
   */
  public function __construct() {
    parent::__construct();
    
    unset( $_SESSION['credential'] );
    unset( $_COOKIE['dw_public_credential'] );
  }

  /**
   *
   */
  public function index() {
    $cookieData = array(
      'name'   => 'public_credential',
      'value'  => '',
      'expire' => time()-3600,
    );
    
    set_cookie($cookieData);
    $this->session->mark_as_flash('credentialExpired');
    redirect( base_url('auth/login') );
  }
}