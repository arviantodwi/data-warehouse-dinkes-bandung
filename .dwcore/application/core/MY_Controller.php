<?php
defined('BASEPATH') OR exit('Akses langsung tidak diperkenankan');

/**
 * Class MY_Controller
 */
class MY_Controller extends CI_Controller {
  /**
   * @var array
   */
  private $initData = array();
  /**
   * @var array
   */
  private $viewData = array();
  /**
   * @var array
   */
  private $sidebarItems = array(
    'dashboard' => array(
      'uri'    => 'dashboard',
      'icon'   => 'fa-home',
      'locale' => 'Beranda',
      'child'  => null
    ),
    'import' => array(
      'uri'    => 'import',
      'icon'   => 'fa-cloud-upload',
      'locale' => 'Unggah &#38; Impor',
      'child'  => array(
        //0 => array('uri' => 'sikda', 'locale' => 'Data SIKDA'),
        1 => array('uri' => 'spreadsheet', 'locale' => 'Berkas Excel')
      )
    ),
    'report' => array(
      'uri'    => 'report',
      'icon'   => 'fa-clipboard',
      'locale' => 'Laporan',
      'child'  => array(
        0 => array('uri' => 'bulanan', 'locale' => 'Bulanan (LB1)'),
        1 => array('uri' => 'kesehatan_ia', 'locale' => 'Kesehatan Ibu &#38; Anak'),
        2 => array('uri' => 'kematian_ia', 'locale' => 'Kematian Ibu &#38; Anak'),
        3 => array('uri' => 'kb', 'locale' => 'Keluarga Berencana'),
        4 => array('uri' => 'gizi', 'locale' => 'Gizi')
      )
    ),
    'config' => array(
      'uri'    => 'config',
      'icon'   => 'fa-sliders',
      'locale' => 'Konfigurasi',
      'child'  => array(
        0 => array('uri' => 'registration', 'locale' => 'Registrasi Pengguna'),
        1 => array('uri' => 'authority', 'locale' => 'Atur Otoritas')
      )
    ),
    'log' => array(
      'uri'    => 'log',
      'icon'   => 'fa-history',
      'locale' => 'Riwayat Perubahan',
      'child'  => null
    )
  );

  /**
   * MY_Controller constructor.
   */
  public function __construct() {
    parent::__construct();

    $uriSegments = explode('/', $this->uri->uri_string() );
    $this->initData['markup']['uri'] = $uriSegments;

    /*
     * IF all credential is active, not empty and readable
     * ELSE no active credential
     */
    if( isset($_SESSION['credential']) && isset($_COOKIE['dw_public_credential']) && !is_null( get_cookie('public_credential') ) ) {
      /*
       * IF authentication time is still valid (under 30 minutes) and login
       * state is true
       * ELSE go to logout page and set the login state to false
       */
      if( time() - $_SESSION['credential']['last_active'] <= 30*60 && $_SESSION['credential']['is_login'] == TRUE ) {
        // renew authentication time
        $_SESSION['credential']['last_active'] = time();

        /*
         * IF active users are trying to accessing the page outside
         * authentication page
         * ELSE active users are trying to accessing the authentication page
         */
        if($uriSegments[0] !== 'auth') {
          $publicCredential = (array) json_decode( base64_decode( get_cookie('public_credential') ) );

          foreach($publicCredential as $key => $value) {
            $this->initData['model']['public'][$key] = $value;
          }

          $this->initData['markup']['sidebar'] = $this->generate_sidebar($_SESSION['credential']['account_type'], $uriSegments);
        }
        else {
          if( (int)$_SESSION['credential']['account_type'] === 4 ) {
            redirect( site_url('import/spreadsheet') );
          }
          else { redirect( site_url('dashboard') ); }
        }
      }
      else {
        $_SESSION['credentialExpired'] = TRUE;
        redirect( site_url('auth/logout') );
      }
    }
    else {
      if($uriSegments[0] !== 'auth') {
        redirect( site_url('auth/login') );
      }
    }
  }

  /**
   * @param      $key
   * @param null $value
   * @param null $parent
   */
  public function set_view_data($key, $value = null, $parent = null) {
    if( !is_array($key) ) {
      (!is_null($parent)) ? $this->viewData[$parent][$key] = $value : $this->viewData[$key] = $value;
    }
    else {
      foreach($key as $newKey => $newValue) {
        $this->viewData[$newKey] = $newValue;
      }
    }
  }

  /**
   * @param string $key
   * @param null   $parent
   *
   * @return array|mixed
   */
  public function get_view_data($key = 'all', $parent = null) {
    if( !is_null($parent) ) {
      if($key !== 'all') { $data = $this->viewData[$parent][$key]; }
      else { $data = $this->viewData[$parent]; }
    }
    else {
      if($key !== 'all') { $data = $this->viewData[$key]; }
      else { $data = $this->viewData; }
    }

    return $data;
  }

  /**
   * @return response
   */
  public function render() {
    $compiledData = array_merge_recursive($this->viewData, $this->initData);

    $this->load->view('layouts/skeleton', $compiledData);
  }

  /**
   * @param $authority
   * @param $requested
   *
   * @return string
   */
  private function generate_sidebar($authority, $requested) {
    $markup = '';
    $this->load->model('users_model');
    $visibleList = $this->users_model->fetch_authority($authority);

    /*if( !(bool)$visibleList->import_sikda ) {
      unset( $this->sidebarItems['import']['child'][0] );
    }*/
    if( !(bool)$visibleList->import_excel ) {
      unset( $this->sidebarItems['import']['child'][1] );
    }

    foreach($visibleList as $key => $value) {
      if( count( explode('_', $key) ) !== 1 ) { break; }
      elseif( !(bool)$value ) { continue; }

      /*$parentClass = '';
      $parentLink = '';*/
      $treeview = '';
      $child = $this->sidebarItems[$key]['child'];

      if($child !== null) {
        $parentClass = ($key == $requested[0]) ? 'treeview active' : 'treeview';
        $parentLink = '#';
        $treeview = '<ul class="treeview-menu">';
        $treeIndicator = '<i class="fa fa-angle-left pull-right"></i>';

        foreach($child as $index => $menu) {
          $childLink = '/'.$key.'/'.$menu['uri'];
          $childIconClass = ( !empty($requested[1]) && $menu['uri'] == $requested[1] ) ? 'fa-dot-circle-o' : 'fa-circle-o';

          if( $menu == null || empty($menu) ) { continue; }

          $treeview .= '<li><a href="'.$childLink.'"><i class="fa '.$childIconClass.'"></i>'.$menu['locale'].'</a></li>';
        }

        $treeview .= '</ul>';
      }
      else {
        $parentClass = ($key == $requested[0]) ? 'active' : '';
        $parentLink = '/'.$key;
        $treeIndicator = '';
      }

      $markup .= '<li class="'.$parentClass.'">';
      $markup .= '<a href="'.$parentLink.'"><i class="fa '.$this->sidebarItems[$key]['icon'].' fa-lg"></i><span style="margin-left: 7px;">'.$this->sidebarItems[$key]['locale'].'</span>'.$treeIndicator.'</a>';
      $markup .= ($treeview !== '') ? $treeview : '';
      $markup .= '</li>';
    }

    return $markup;
  }

  /**
   * @param $debArr
   */
  protected function run_debug($debArr) {
    foreach($debArr as $value) {
      var_dump($value);
    }
    exit();
  }
}
