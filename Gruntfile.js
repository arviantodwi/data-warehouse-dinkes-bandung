/*!
 * Data Warehouse's Gruntfile
 * Authored by Arvianto Dwi, 2015-2016
 */
module.exports = function(grunt) {
  'use strict';

  // Load all packages in development scope with "grunt-" file name pattern.
  // See package.json for packages list and detail.
  require('load-grunt-tasks')(grunt, {scope: 'devDependencies'});

  // Load some postcss plugin and assign each of it to variable
  var autoprefixer = require('autoprefixer');
  var cssnano = require('cssnano');

  /**
   * Generate the file path
   * 
   * @param {string}  fileName    - The actual file name
   * @param {string}  buildType   - 'development' or 'production' 
   * @param {boolean} parentsOnly - If this value is TRUE, the file name won't
   *                                be included after the last directory
   * @returns {string}
   */
  var getFilePath = function(fileName, buildType, parentsOnly) {
    var buildType = (typeof buildType !== 'undefined') ? buildType : 'development';
    var parentsOnly = (typeof parentsOnly !== 'undefined') ? parentsOnly : false;
    var fullPath = '';

    if(typeof fileName !== 'undefined') {
      var mainPath = {
        assets: 'assets/',
        production: 'html/'
      };
      var fileExt = fileName.split('.').pop() + '/';

      fullPath = (buildType == 'development') ? mainPath.assets + fileExt : mainPath.production + fileExt;

      if(parentsOnly == false) {
        fullPath += fileName;
      }
    }
    else {
      fullPath = 'undefined';
      console.log('File name is undefined!');
    }
    
    return fullPath;
  };

  // Grunt project configuration
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // Config: grunt-sass
    sass: {
      compile: {
        options: { outputStyle: 'expanded' },
        src: getFilePath('datakesbdw.scss', 'development'),
        dest: getFilePath('temp.css', 'development')
      }
    },

    // Config: grunt-postcss
    postcss: {
      options: {
        processors: [
          autoprefixer({
            browsers: ['> 1%', 'last 2 versions', 'ie 8', 'ie 9', 'Opera 12.1']
          })
        ]
      },
      vendorPrefix: {
        src: getFilePath('temp.css', 'development'),
        dest: getFilePath('datakesbdw.css', 'development')
      }
      /*,
      distribution: {
        options: {
          processors: [
            cssnano()
          ],
          map: {
            inline: false,
            annotation: 'Content2016/css/'
          }
        },
        files: [{
          expand: true,
          cwd: 'Content2016/css/dev',
          src: 'venuekita.css',
          dest: 'Content2016/css',
          ext: '.min.css'
        }]
      }*/
    },

    // Config: grunt-exec
    exec: {
      npmUpdate: {
        command: 'echo "Updating npm packages..."; npm update;'
      },
      testAssets: {
        command: function(assetsType) {
          var pathToAssets = 'assets/' + assetsType;
          var pathToTest = 'html/' + assetsType;

          return 'cp ' + pathToAssets + '/* ' + pathToTest;
        }
      }
    },

    // Config: grunt-contrib-copy
    copy: {
      cssTest: {
        expand: true,
        flatten: true,
        cwd: 'assets/css/',
        src: '**',
        dest: 'html/css/',
        filter: 'isFile'
      }
    },
    
    // Config: grunt-contrib-clean
    clean: {
      tempCss: [getFilePath('temp.css', 'development')]
    },

    // Config: grunt-contrib-watch
    watch: {
      scssDev: {
        files: [
          getFilePath('*.scss', 'development'),
          getFilePath('**/*.scss', 'development')
        ],
        tasks: ['sass:compile', 'postcss:vendorPrefix', 'clean:tempCss', 'copy:cssTest']
      }
    }
  });

  // Set grunt task
  grunt.registerTask('default', ['exec:npmUpdate']);
  grunt.registerTask('watch-scss', ['watch:scssDev']);
};